from string import Template

protocol = 'refine'

SA_schedule = [
    (10000,   5000),
    (1000,   50000),
]

do_ini_opt = False

traj_frame_period = 100 
print_frame_period = traj_frame_period 

print_log_scores_to_files = True 
print_log_scores_to_files_frame_period = 10

print_total_score_to_files = True
print_total_score_to_files_frame_period = 10

states = 1
struct_resolutions = [1,10]
add_missing = False
missing_resolution = 1

add_rbs_from_pdbs = True

connectivity_restraint_weight = 1.
max_conn_gap = None
conn_first_copy_only = False

rb_max_rot = 2
rb_max_trans = 3

add_symmetry_constraints = False
add_symmetry_restraints = False

add_xlink_restraints = False

add_em_restr = True

ev_restraints = [
    {
        'name': 'ev_restraints_lowres',
        'weight': 10,
        'repr_resolution': 10
    }
]

scoring_functions = {
    'score_func_lowres': {
        'restraints': [
            'conn_restraints',
            'ev_restraints_lowres',
            'FitRestraint_NR_116d37',
            'membrane_116d37C',
            'elastic_network'
        ]
    }
}

score_func = 'score_func_lowres'

# SLURM template, adjust to your cluster environment
ntasks = 1
cluster_submission_command = 'sbatch'
run_script_templ = Template("""#!/bin/bash
#
#SBATCH --ntasks=$ntasks
#SBATCH --mem-per-cpu=2000
#SBATCH --job-name=${prefix}fit
#SBATCH --time=10:00:00
#SBATCH -e $outdir/logs/${prefix}_err.txt
#SBATCH -o $outdir/logs/${prefix}_out.txt

echo "Running on:"
srun hostname

$cmd

wait
""")