from efitter import Map
from string import Template

method='chimera'
dry_run = False
run_crashed_only = True
master_outdir = './result_fits_chimera'

MAPS = [
    Map('em_maps/NR_merged_unerased_tail_relative_clean_v1.3.mrc', threshold=0.0539, resolution=40)
]

models_dir = './PDB'

PDB_FILES = [
    'ScNup133N_56-480_renamed.pdb',
    'ScNup133C_490_881_renamed_relative_3CQC.pdb',
    '4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb',
    '4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb',
    '4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb',
    '4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb',
    'ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb'
]

CA_only = False
backbone_only = False
move_to_center = True

fitmap_args = [
    # We suggest to run a small test run with search 100 first
    # {
    #     'template': Template("""
    #         map $map
    #         map_threshold $threshold
    #         fitmap_args resolution $resolution metric cam envelope true search 100 placement sr clusterAngle 3 clusterShift 3.0 radius 600 inside .30
    #         saveFiles False
    #         """),
    #     'config_prefix': 'test'
    #     },
    # Paramters for main runs (https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/fitmap.html)
    {
        'template': Template("""
            map $map
            map_threshold $threshold
            fitmap_args resolution $resolution metric cam envelope true search 100000 placement sr clusterAngle 3 clusterShift 3.0 radius 600 inside .30
            saveFiles False
            """),
        'config_prefix': 'search100000_metric_cam_rad_600_inside0.3_res_40'
        },
     {
        'template': Template("""
            map $map
            map_threshold $threshold
            fitmap_args resolution $resolution metric cam envelope true search 100000 placement sr maxSteps 10 clusterAngle 0.1 clusterShift 0.1 radius 600 inside .30
            saveFiles False
            """),
        'config_prefix': 'search100000_metric_cam_rad_600_inside0.3_cluster_sets_0.1_maxsteps_10_res_40'
        }
]


# If necessary, edit the below template following specifications of
# cluster_submission_command
# and template for you cluster submission script (using Python string.Template formatting)
cluster_submission_command = 'sbatch'
run_script_templ = Template("""#!/bin/bash
#
#SBATCH --job-name=$job_name
#SBATCH --nodes=1
#SBATCH --mem-per-cpu=m=2000
#SBATCH --time=10:00:00
#SBATCH --error $pdb_outdir/log_err.txt
#SBATCH --output $pdb_outdir/log_out.txt

$cmd
""")