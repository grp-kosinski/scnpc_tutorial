score 33722.064550858195
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-588.3107157369626,-588.3107157369626,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1849.3735871116044,1849.3735871116044,1.0
"conn restraint Residue_711-Residue_715 of Nup120",13013.429874869906,13013.429874869906,1.0
"conn restraint Residue_480-Residue_490 of Nup133",38.78867822568526,38.78867822568526,1.0
"conn restraint Residue_881-Residue_882 of Nup133",6871.8884694548115,6871.8884694548115,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",1840.3563809320776,1840.3563809320776,1.0
"ev_restraint_resol_10",4171.747778093378,4171.747778093378,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",4512.434251507998,4512.434251507998,1000.0
"Nup133_close_to_membrane",4571.291732788086,4571.291732788086,100.0
"Nup120_close_to_membrane",1650.956153869629,1650.956153869629,100.0
