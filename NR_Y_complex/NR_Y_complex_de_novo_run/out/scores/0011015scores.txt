score 13035.193118988007
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1374.173698104934,-1374.173698104934,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1022.4188847438211,1022.4188847438211,1.0
"conn restraint Residue_544-Residue_553 of Nup85",118.08829512497073,118.08829512497073,1.0
"conn restraint Residue_711-Residue_715 of Nup120",4222.692429957679,4222.692429957679,1.0
"conn restraint Residue_480-Residue_490 of Nup133",462.60352999132635,462.60352999132635,1.0
"conn restraint Residue_881-Residue_882 of Nup133",742.6116703124695,742.6116703124695,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",1648.9335148000441,1648.9335148000441,1.0
"ev_restraint_resol_10",5294.320608469412,5294.320608469412,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",2328.4386580210953,2328.4386580210953,1000.0
"Nup133_close_to_membrane",2022.003173828125,2022.003173828125,100.0
"Nup120_close_to_membrane",673.9990234375,673.9990234375,100.0
