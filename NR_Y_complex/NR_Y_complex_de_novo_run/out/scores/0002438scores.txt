score 32328.936810795807
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1219.1636320228845,-1219.1636320228845,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",9956.176509362174,9956.176509362174,1.0
"conn restraint Residue_711-Residue_715 of Nup120",579.345473629335,579.345473629335,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1223.5176621223784,1223.5176621223784,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2493.2589957629716,2493.2589957629716,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",4974.343963604192,4974.343963604192,1.0
"ev_restraint_resol_10",14175.395243671352,14175.395243671352,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",29.691586935572687,29.691586935572687,1000.0
"Nup133_close_to_membrane",2235.404586791992,2235.404586791992,100.0
"Nup120_close_to_membrane",2021.9970703125,2021.9970703125,100.0
