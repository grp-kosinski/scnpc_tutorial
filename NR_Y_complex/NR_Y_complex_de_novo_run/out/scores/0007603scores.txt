score 35912.835388491876
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.2808015829963,-692.2808015829963,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",2963.241670175825,2963.241670175825,1.0
"conn restraint Residue_544-Residue_553 of Nup85",309.89531483947854,309.89531483947854,1.0
"conn restraint Residue_711-Residue_715 of Nup120",3101.1141602191033,3101.1141602191033,1.0
"conn restraint Residue_480-Residue_490 of Nup133",343.7472373820617,343.7472373820617,1.0
"conn restraint Residue_881-Residue_882 of Nup133",8036.997549120182,8036.997549120182,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",4.739071103236815,4.739071103236815,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",15587.795834771752,15587.795834771752,1.0
"ev_restraint_resol_10",2288.5520649662244,2288.5520649662244,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",4571.29020690918,4571.29020690918,100.0
"Nup120_close_to_membrane",3629.6016693115234,3629.6016693115234,100.0
