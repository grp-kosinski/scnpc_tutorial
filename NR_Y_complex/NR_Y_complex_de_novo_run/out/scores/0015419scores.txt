score 25734.141116010276
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2668.2519743574753,-2668.2519743574753,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1046.8972635302398,1046.8972635302398,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1498.9734661573402,1498.9734661573402,1.0
"conn restraint Residue_480-Residue_490 of Nup133",998.2954687580183,998.2954687580183,1.0
"conn restraint Residue_881-Residue_882 of Nup133",622.9393736652598,622.9393736652598,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",5907.176378349419,5907.176378349419,1.0
"ev_restraint_resol_10",14603.017628003965,14603.017628003965,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",609.8705010267081,609.8705010267081,1000.0
"Nup133_close_to_membrane",953.178596496582,953.178596496582,100.0
"Nup120_close_to_membrane",6358.500671386719,6358.500671386719,100.0
