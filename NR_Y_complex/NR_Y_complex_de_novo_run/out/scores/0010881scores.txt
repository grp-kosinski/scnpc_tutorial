score 29155.646349785817
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1721.6785784706417,-1721.6785784706417,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-689.1703684403612,-689.1703684403612,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2949.9337033186284,2949.9337033186284,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1184.4260461665074,1184.4260461665074,1.0
"conn restraint Residue_480-Residue_490 of Nup133",959.5267020309909,959.5267020309909,1.0
"conn restraint Residue_881-Residue_882 of Nup133",446.6676460322036,446.6676460322036,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",6810.75517963986,6810.75517963986,1.0
"ev_restraint_resol_10",14982.133296721298,14982.133296721298,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",673.9990234375,673.9990234375,100.0
"Nup120_close_to_membrane",7068.9727783203125,7068.9727783203125,100.0
