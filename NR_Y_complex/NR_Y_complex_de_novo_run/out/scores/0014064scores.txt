score 28612.65874336696
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-804.1948174497952,-804.1948174497952,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-684.8553346686846,-684.8553346686846,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-669.4341731943005,-669.4341731943005,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",308.6416395950663,308.6416395950663,1.0
"conn restraint Residue_544-Residue_553 of Nup85",31.41362936777184,31.41362936777184,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1256.6186760845535,1256.6186760845535,1.0
"conn restraint Residue_480-Residue_490 of Nup133",787.7806322492984,787.7806322492984,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1213.1700330643212,1213.1700330643212,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",16955.429829171768,16955.429829171768,1.0
"ev_restraint_resol_10",1982.4311280546146,1982.4311280546146,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1017.3559482927363,1017.3559482927363,1000.0
"Nup133_close_to_membrane",0.0,0.0,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
