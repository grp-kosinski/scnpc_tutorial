score 6138.401624571783
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2024.9306729054326,-2024.9306729054326,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-790.8409132207426,-790.8409132207426,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",17.79439482913859,17.79439482913859,1.0
"conn restraint Residue_711-Residue_715 of Nup120",31.421795368356378,31.421795368356378,1.0
"conn restraint Residue_480-Residue_490 of Nup133",268.8581809090834,268.8581809090834,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1438.7884803617374,1438.7884803617374,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",105.32937017645786,105.32937017645786,1.0
"ev_restraint_resol_10",6171.648140442339,6171.648140442339,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1052.4982922927923,1052.4982922927923,1000.0
"Nup133_close_to_membrane",1347.998046875,1347.998046875,100.0
"Nup120_close_to_membrane",2021.9970703125,2021.9970703125,100.0
