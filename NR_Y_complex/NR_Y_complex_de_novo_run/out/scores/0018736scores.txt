score 25895.824826755837
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-8578.903797806503,-8578.903797806503,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1059.1958984690011,1059.1958984690011,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1175.4662848839284,1175.4662848839284,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1328.410953698624,1328.410953698624,1.0
"conn restraint Residue_480-Residue_490 of Nup133",74.56373015226066,74.56373015226066,1.0
"conn restraint Residue_881-Residue_882 of Nup133",155.22682303196942,155.22682303196942,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",1213.732092346493,1213.732092346493,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",5522.153391818814,5522.153391818814,1.0
"ev_restraint_resol_10",16120.432533339623,16120.432533339623,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",2022.003173828125,2022.003173828125,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
