score 28373.178621465217
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1374.173698104934,-1374.173698104934,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",131.5063519594346,131.5063519594346,1.0
"conn restraint Residue_711-Residue_715 of Nup120",3473.4304458761026,3473.4304458761026,1.0
"conn restraint Residue_480-Residue_490 of Nup133",502.38468027422186,502.38468027422186,1.0
"conn restraint Residue_881-Residue_882 of Nup133",648.6209260557578,648.6209260557578,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",9574.265272023893,9574.265272023893,1.0
"ev_restraint_resol_10",7866.54395021923,7866.54395021923,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",5963.758445521284,5963.758445521284,1000.0
"Nup133_close_to_membrane",3629.604721069336,3629.604721069336,100.0
"Nup120_close_to_membrane",2131.374931335449,2131.374931335449,100.0
