score 36078.23838561929
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2663.9530548835914,-2663.9530548835914,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-588.3107157369626,-588.3107157369626,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",370.64295035463374,370.64295035463374,1.0
"conn restraint Residue_544-Residue_553 of Nup85",3443.9206770038386,3443.9206770038386,1.0
"conn restraint Residue_711-Residue_715 of Nup120",8589.275291031183,8589.275291031183,1.0
"conn restraint Residue_480-Residue_490 of Nup133",199.47288052955005,199.47288052955005,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2213.166095359178,2213.166095359178,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",6653.3920011090195,6653.3920011090195,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",12656.134415659013,12656.134415659013,1.0
"ev_restraint_resol_10",618.15256823578,618.15256823578,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1605.0536369962315,1605.0536369962315,1000.0
"Nup133_close_to_membrane",0.0,0.0,100.0
"Nup120_close_to_membrane",6429.548645019531,6429.548645019531,100.0
