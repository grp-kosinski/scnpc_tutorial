score 21523.2915010461
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",2280.8907723784905,2280.8907723784905,1.0
"conn restraint Residue_544-Residue_553 of Nup85",148.52160280712116,148.52160280712116,1.0
"conn restraint Residue_711-Residue_715 of Nup120",289.9802537731566,289.9802537731566,1.0
"conn restraint Residue_480-Residue_490 of Nup133",506.02555951213077,506.02555951213077,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2777.980297576592,2777.980297576592,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2551.1184273440654,2551.1184273440654,1.0
"ev_restraint_resol_10",8293.184135750853,8293.184135750853,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",4765.899276733398,4765.899276733398,100.0
"Nup120_close_to_membrane",4813.321685791016,4813.321685791016,100.0
