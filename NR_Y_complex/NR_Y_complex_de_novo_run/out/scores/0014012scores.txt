score 19656.75759287425
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2668.2519743574753,-2668.2519743574753,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",921.83517746615,921.83517746615,1.0
"conn restraint Residue_544-Residue_553 of Nup85",3540.2461646276206,3540.2461646276206,1.0
"conn restraint Residue_711-Residue_715 of Nup120",845.4829718302052,845.4829718302052,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1002.6425680044833,1002.6425680044833,1.0
"conn restraint Residue_881-Residue_882 of Nup133",5539.083124272565,5539.083124272565,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",94.3635750858744,94.3635750858744,1.0
"ev_restraint_resol_10",7254.406652153828,7254.406652153828,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",5264.109420776367,5264.109420776367,100.0
"Nup120_close_to_membrane",2021.9970703125,2021.9970703125,100.0
