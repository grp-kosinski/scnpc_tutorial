score 24205.970317449664
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-713.7571026696061,-713.7571026696061,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1976.7149018853092,1976.7149018853092,1.0
"conn restraint Residue_544-Residue_553 of Nup85",3605.595481847572,3605.595481847572,1.0
"conn restraint Residue_711-Residue_715 of Nup120",498.6503743213509,498.6503743213509,1.0
"conn restraint Residue_480-Residue_490 of Nup133",0.07033327546178776,0.07033327546178776,1.0
"conn restraint Residue_881-Residue_882 of Nup133",5281.948301720026,5281.948301720026,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",5017.718787566897,5017.718787566897,1.0
"ev_restraint_resol_10",4907.2206200355295,4907.2206200355295,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1576.477004567957,1576.477004567957,1000.0
"Nup133_close_to_membrane",4906.795883178711,4906.795883178711,100.0
"Nup120_close_to_membrane",1347.998046875,1347.998046875,100.0
