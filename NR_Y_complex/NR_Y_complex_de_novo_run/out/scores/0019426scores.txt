score 14773.229206467899
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2336.9020107503266,-2336.9020107503266,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.8973168071794,-679.8973168071794,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1121.2905533645398,1121.2905533645398,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1371.4533140063643,1371.4533140063643,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1183.552964332051,1183.552964332051,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1368.4742657928682,1368.4742657928682,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",769.9340269434181,769.9340269434181,1.0
"ev_restraint_resol_10",1125.399311368813,1125.399311368813,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",110.06988407756789,110.06988407756789,1000.0
"Nup133_close_to_membrane",4368.014907836914,4368.014907836914,100.0
"Nup120_close_to_membrane",9813.587188720703,9813.587188720703,100.0
