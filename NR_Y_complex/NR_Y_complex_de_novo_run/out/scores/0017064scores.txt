score 38199.53679430173
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-810.2755386342355,-810.2755386342355,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-695.1045108627361,-695.1045108627361,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1416.9039765132718,1416.9039765132718,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2294.6846701490726,2294.6846701490726,1.0
"conn restraint Residue_711-Residue_715 of Nup120",739.6134496941623,739.6134496941623,1.0
"conn restraint Residue_480-Residue_490 of Nup133",795.8219655394571,795.8219655394571,1.0
"conn restraint Residue_881-Residue_882 of Nup133",5959.596495369892,5959.596495369892,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",1532.9250173040182,1532.9250173040182,1.0
"ev_restraint_resol_10",16423.251207540256,16423.251207540256,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",6906.446075439453,6906.446075439453,100.0
"Nup120_close_to_membrane",7164.719390869141,7164.719390869141,100.0
