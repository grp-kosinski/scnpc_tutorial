score 12767.771940703507
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",3469.3619830653997,3469.3619830653997,1.0
"conn restraint Residue_544-Residue_553 of Nup85",54.26398663952859,54.26398663952859,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1739.2718224243688,1739.2718224243688,1.0
"conn restraint Residue_480-Residue_490 of Nup133",0.0,0.0,1.0
"conn restraint Residue_881-Residue_882 of Nup133",3677.9980774359756,3677.9980774359756,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",304.3637354760693,304.3637354760693,1.0
"ev_restraint_resol_10",1127.271241596447,1127.271241596447,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",2148.1296858598485,2148.1296858598485,1000.0
"Nup133_close_to_membrane",4470.811080932617,4470.811080932617,100.0
"Nup120_close_to_membrane",673.9990234375,673.9990234375,100.0
