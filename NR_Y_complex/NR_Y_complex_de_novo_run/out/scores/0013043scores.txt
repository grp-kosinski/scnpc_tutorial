score 43957.487172778296
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-648.491685812281,-648.491685812281,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",6374.429761247402,6374.429761247402,1.0
"conn restraint Residue_544-Residue_553 of Nup85",3145.595977690573,3145.595977690573,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1543.6767388512455,1543.6767388512455,1.0
"conn restraint Residue_480-Residue_490 of Nup133",0.0,0.0,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2448.0345117797688,2448.0345117797688,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",13496.205599776436,13496.205599776436,1.0
"ev_restraint_resol_10",3952.3659636947696,3952.3659636947696,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",4167.746819063431,4167.746819063431,1000.0
"Nup133_close_to_membrane",10000.0,10000.0,100.0
"Nup120_close_to_membrane",3691.6534423828125,3691.6534423828125,100.0
