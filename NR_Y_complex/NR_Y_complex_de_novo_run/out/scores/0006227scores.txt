score 19231.151641157783
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",2016.3387544076118,2016.3387544076118,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2619.192497082472,2619.192497082472,1.0
"conn restraint Residue_711-Residue_715 of Nup120",4356.1104987620165,4356.1104987620165,1.0
"conn restraint Residue_480-Residue_490 of Nup133",221.28591647986806,221.28591647986806,1.0
"conn restraint Residue_881-Residue_882 of Nup133",771.386437247043,771.386437247043,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2235.6111164235954,2235.6111164235954,1.0
"ev_restraint_resol_10",6596.022674492679,6596.022674492679,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",818.4825943430089,818.4825943430089,1000.0
"Nup133_close_to_membrane",0.0,0.0,100.0
"Nup120_close_to_membrane",4470.811080932617,4470.811080932617,100.0
