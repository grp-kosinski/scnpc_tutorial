score 20428.368757470864
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-810.2755386342355,-810.2755386342355,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1470.6264339427562,1470.6264339427562,1.0
"conn restraint Residue_544-Residue_553 of Nup85",64.65765618673895,64.65765618673895,1.0
"conn restraint Residue_711-Residue_715 of Nup120",2402.2581582913144,2402.2581582913144,1.0
"conn restraint Residue_480-Residue_490 of Nup133",225.75995419980364,225.75995419980364,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1918.4981738510098,1918.4981738510098,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",0.0,0.0,1.0
"ev_restraint_resol_10",9091.544882447193,9091.544882447193,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",2891.207075329079,2891.207075329079,1000.0
"Nup133_close_to_membrane",3436.7366790771484,3436.7366790771484,100.0
"Nup120_close_to_membrane",3987.4401092529297,3987.4401092529297,100.0
