score 13458.1181646021
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2356.774025740865,2356.774025740865,1.0
"conn restraint Residue_711-Residue_715 of Nup120",3926.233172346214,3926.233172346214,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1.2074580891978204,1.2074580891978204,1.0
"conn restraint Residue_881-Residue_882 of Nup133",903.3183934965441,903.3183934965441,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2091.417638540444,2091.417638540444,1.0
"ev_restraint_resol_10",546.9040651510757,546.9040651510757,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",4224.1320000871165,4224.1320000871165,1000.0
"Nup133_close_to_membrane",2778.972053527832,2778.972053527832,100.0
"Nup120_close_to_membrane",1507.1076393127441,1507.1076393127441,100.0
