score 32184.75283403448
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3969.4510865147563,-3969.4510865147563,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-810.2755386342355,-810.2755386342355,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1512.4237281632122,1512.4237281632122,1.0
"conn restraint Residue_544-Residue_553 of Nup85",0.0,0.0,1.0
"conn restraint Residue_711-Residue_715 of Nup120",3608.571723626926,3608.571723626926,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1637.8818804461741,1637.8818804461741,1.0
"conn restraint Residue_881-Residue_882 of Nup133",10621.966964654956,10621.966964654956,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",2089.585221216631,2089.585221216631,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",12972.000861330085,12972.000861330085,1.0
"ev_restraint_resol_10",948.8616142522113,948.8616142522113,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",6394.12727355957,6394.12727355957,100.0
"Nup120_close_to_membrane",673.9990234375,673.9990234375,100.0
