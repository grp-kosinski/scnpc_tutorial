score 28059.854884433367
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-886.2557041605461,-886.2557041605461,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0083388380074381,0.0083388380074381,1.0
"conn restraint Residue_544-Residue_553 of Nup85",384.20887658251456,384.20887658251456,1.0
"conn restraint Residue_711-Residue_715 of Nup120",395.06802103793035,395.06802103793035,1.0
"conn restraint Residue_480-Residue_490 of Nup133",137.05102689075125,137.05102689075125,1.0
"conn restraint Residue_881-Residue_882 of Nup133",9914.719798053207,9914.719798053207,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",870.5050804202415,870.5050804202415,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",6884.505859268362,6884.505859268362,1.0
"ev_restraint_resol_10",10291.343962884257,10291.343962884257,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",3616.209626722966,3616.209626722966,1000.0
"Nup133_close_to_membrane",673.9990234375,673.9990234375,100.0
"Nup120_close_to_membrane",0.0,0.0,100.0
