score 22130.42968763433
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",160.39191186972698,160.39191186972698,1.0
"conn restraint Residue_544-Residue_553 of Nup85",64.27201340772277,64.27201340772277,1.0
"conn restraint Residue_711-Residue_715 of Nup120",2364.6778173796206,2364.6778173796206,1.0
"conn restraint Residue_480-Residue_490 of Nup133",0.0,0.0,1.0
"conn restraint Residue_881-Residue_882 of Nup133",3058.4226810067644,3058.4226810067644,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",1097.2711206605154,1097.2711206605154,1.0
"ev_restraint_resol_10",4225.952206212867,4225.952206212867,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1553.9572984636618,1553.9572984636618,1000.0
"Nup133_close_to_membrane",10000.0,10000.0,100.0
"Nup120_close_to_membrane",4521.329879760742,4521.329879760742,100.0
