score 24041.471998849313
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-739.8903790710319,-739.8903790710319,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-717.4361808126814,-717.4361808126814,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1124.350544667979,1124.350544667979,1.0
"conn restraint Residue_711-Residue_715 of Nup120",227.24812743213226,227.24812743213226,1.0
"conn restraint Residue_480-Residue_490 of Nup133",409.2363925681803,409.2363925681803,1.0
"conn restraint Residue_881-Residue_882 of Nup133",891.2162562904542,891.2162562904542,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",217.94737808879975,217.94737808879975,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",7131.492745577156,7131.492745577156,1.0
"ev_restraint_resol_10",7416.666677597368,7416.666677597368,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",35.84091673863617,35.84091673863617,1000.0
"Nup133_close_to_membrane",3987.437057495117,3987.437057495117,100.0
"Nup120_close_to_membrane",7565.631103515625,7565.631103515625,100.0
