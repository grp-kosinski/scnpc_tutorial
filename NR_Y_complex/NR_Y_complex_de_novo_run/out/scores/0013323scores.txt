score 18554.68058239801
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-644.2709948483198,-644.2709948483198,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",823.3019161058838,823.3019161058838,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2345.0663743400155,2345.0663743400155,1.0
"conn restraint Residue_711-Residue_715 of Nup120",408.1825707425918,408.1825707425918,1.0
"conn restraint Residue_480-Residue_490 of Nup133",38.78867822568526,38.78867822568526,1.0
"conn restraint Residue_881-Residue_882 of Nup133",4638.883869627391,4638.883869627391,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",1173.9469077464958,1173.9469077464958,1.0
"ev_restraint_resol_10",9401.62264982716,9401.62264982716,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",4571.291732788086,4571.291732788086,100.0
"Nup120_close_to_membrane",0.0,0.0,100.0
