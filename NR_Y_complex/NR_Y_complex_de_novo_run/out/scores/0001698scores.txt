score 31070.21451832508
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",6863.637683001652,6863.637683001652,1.0
"conn restraint Residue_544-Residue_553 of Nup85",974.6821432945193,974.6821432945193,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1817.8618066679514,1817.8618066679514,1.0
"conn restraint Residue_480-Residue_490 of Nup133",3757.7294559820502,3757.7294559820502,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2954.645380418112,2954.645380418112,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",525.1418186553717,525.1418186553717,1.0
"ev_restraint_resol_10",6385.702479946532,6385.702479946532,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",4470.811080932617,4470.811080932617,100.0
"Nup120_close_to_membrane",8227.21939086914,8227.21939086914,100.0
