score 37772.764696426995
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2024.9306729054326,-2024.9306729054326,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-681.537778020655,-681.537778020655,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",164.16807218914943,164.16807218914943,1.0
"conn restraint Residue_544-Residue_553 of Nup85",172.37346423019335,172.37346423019335,1.0
"conn restraint Residue_711-Residue_715 of Nup120",132.45545646804692,132.45545646804692,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1282.234541602577,1282.234541602577,1.0
"conn restraint Residue_881-Residue_882 of Nup133",9322.116499821894,9322.116499821894,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",544.1957253626543,544.1957253626543,1.0
"ev_restraint_resol_10",20748.4647180236,20748.4647180236,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3871.8338012695312,3871.8338012695312,100.0
"Nup120_close_to_membrane",7714.282989501953,7714.282989501953,100.0
