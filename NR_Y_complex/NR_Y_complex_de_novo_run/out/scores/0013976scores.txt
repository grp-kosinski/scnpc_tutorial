score 30717.0161424063
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1764.459423835831,-1764.459423835831,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-400.60893148812374,-400.60893148812374,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1419.1875177101124,1419.1875177101124,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1196.8248214692183,1196.8248214692183,1.0
"conn restraint Residue_480-Residue_490 of Nup133",271.92773964289364,271.92773964289364,1.0
"conn restraint Residue_881-Residue_882 of Nup133",215.96090556035446,215.96090556035446,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",867.8746825484809,867.8746825484809,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",22207.835375091396,22207.835375091396,1.0
"ev_restraint_resol_10",1339.1230536787189,1339.1230536787189,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3014.217948913574,3014.217948913574,100.0
"Nup120_close_to_membrane",5797.966766357422,5797.966766357422,100.0
