score 34336.152839387425
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-101.97298382629741,-101.97298382629741,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",416.6835720473452,416.6835720473452,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1015.3411071417524,1015.3411071417524,1.0
"conn restraint Residue_711-Residue_715 of Nup120",126.57968706929287,126.57968706929287,1.0
"conn restraint Residue_480-Residue_490 of Nup133",134.4892983415332,134.4892983415332,1.0
"conn restraint Residue_881-Residue_882 of Nup133",6834.518571506751,6834.518571506751,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",6451.747600426184,6451.747600426184,1.0
"ev_restraint_resol_10",8665.094328168383,8665.094328168383,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",5043.756484985352,5043.756484985352,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
