score 9246.624359331507
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-807.0368347996739,-807.0368347996739,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-588.3107157369626,-588.3107157369626,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-695.1045108627361,-695.1045108627361,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",89.27935116370698,89.27935116370698,1.0
"conn restraint Residue_711-Residue_715 of Nup120",4952.41443068769,4952.41443068769,1.0
"conn restraint Residue_480-Residue_490 of Nup133",548.5467334890005,548.5467334890005,1.0
"conn restraint Residue_881-Residue_882 of Nup133",534.4683407551045,534.4683407551045,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",106.11379996722334,106.11379996722334,1.0
"ev_restraint_resol_10",1501.1344149528386,1501.1344149528386,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",128.42041385331342,128.42041385331342,1000.0
"Nup133_close_to_membrane",673.9990234375,673.9990234375,100.0
"Nup120_close_to_membrane",5598.666000366211,5598.666000366211,100.0
