score 44514.1946797069
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-744.5355599987361,-744.5355599987361,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-717.4361808126814,-717.4361808126814,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",13.748160044975865,13.748160044975865,1.0
"conn restraint Residue_711-Residue_715 of Nup120",952.2165242831919,952.2165242831919,1.0
"conn restraint Residue_480-Residue_490 of Nup133",4192.117981365347,4192.117981365347,1.0
"conn restraint Residue_881-Residue_882 of Nup133",7976.512575671409,7976.512575671409,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",1.9412780932266906,1.9412780932266906,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",12863.947030715346,12863.947030715346,1.0
"ev_restraint_resol_10",6065.234843936968,6065.234843936968,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",8631.412506103516,8631.412506103516,100.0
"Nup120_close_to_membrane",8787.881469726562,8787.881469726562,100.0
