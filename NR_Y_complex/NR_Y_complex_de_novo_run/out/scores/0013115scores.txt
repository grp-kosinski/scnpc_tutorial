score 32675.021255290216
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",8232.108996265555,8232.108996265555,1.0
"conn restraint Residue_544-Residue_553 of Nup85",5114.793524941353,5114.793524941353,1.0
"conn restraint Residue_711-Residue_715 of Nup120",3282.404947908771,3282.404947908771,1.0
"conn restraint Residue_480-Residue_490 of Nup133",12.018960760291652,12.018960760291652,1.0
"conn restraint Residue_881-Residue_882 of Nup133",3930.546020349768,3930.546020349768,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",15.679523802685155,15.679523802685155,1.0
"ev_restraint_resol_10",4072.3511734740437,4072.3511734740437,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",4786.929687214318,4786.929687214318,1000.0
"Nup133_close_to_membrane",5264.110946655273,5264.110946655273,100.0
"Nup120_close_to_membrane",2859.5415115356445,2859.5415115356445,100.0
