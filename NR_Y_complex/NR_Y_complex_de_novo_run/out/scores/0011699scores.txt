score 22201.08329881735
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",2378.1738502834396,2378.1738502834396,1.0
"conn restraint Residue_544-Residue_553 of Nup85",3012.808119055299,3012.808119055299,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1426.0300668196228,1426.0300668196228,1.0
"conn restraint Residue_480-Residue_490 of Nup133",2494.722174830545,2494.722174830545,1.0
"conn restraint Residue_881-Residue_882 of Nup133",852.6810248093369,852.6810248093369,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",1410.3989488211425,1410.3989488211425,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",4408.0282643500295,4408.0282643500295,1.0
"ev_restraint_resol_10",1020.8407924567653,1020.8407924567653,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3691.6526794433594,3691.6526794433594,100.0
"Nup120_close_to_membrane",6394.124984741211,6394.124984741211,100.0
