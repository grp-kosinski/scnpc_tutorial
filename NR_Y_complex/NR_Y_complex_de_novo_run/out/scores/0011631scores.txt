score 25354.276923922054
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-683.1966036630552,-683.1966036630552,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-717.4361808126814,-717.4361808126814,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",4070.792494665865,4070.792494665865,1.0
"conn restraint Residue_711-Residue_715 of Nup120",419.85584499668283,419.85584499668283,1.0
"conn restraint Residue_480-Residue_490 of Nup133",169.06736054117948,169.06736054117948,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1361.043833545852,1361.043833545852,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",595.231463028269,595.231463028269,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",5150.495489240438,5150.495489240438,1.0
"ev_restraint_resol_10",3689.876248709379,3689.876248709379,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",4813.324737548828,4813.324737548828,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
