score 25878.182671908195
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2490.677149709458,-2490.677149709458,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",539.2484785970734,539.2484785970734,1.0
"conn restraint Residue_711-Residue_715 of Nup120",357.0321937958197,357.0321937958197,1.0
"conn restraint Residue_480-Residue_490 of Nup133",39.910373021199256,39.910373021199256,1.0
"conn restraint Residue_881-Residue_882 of Nup133",3223.0013619086317,3223.0013619086317,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",173.44266150226997,173.44266150226997,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",10281.877884150457,10281.877884150457,1.0
"ev_restraint_resol_10",1155.8035240134182,1155.8035240134182,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1131.4278741239975,1131.4278741239975,1000.0
"Nup133_close_to_membrane",9484.01870727539,9484.01870727539,100.0
"Nup120_close_to_membrane",6103.330612182617,6103.330612182617,100.0
