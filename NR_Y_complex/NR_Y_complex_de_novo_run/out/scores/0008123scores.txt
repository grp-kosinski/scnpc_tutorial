score 32710.78218039956
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-101.97298382629741,-101.97298382629741,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",796.060499055229,796.060499055229,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1651.4849858273747,1651.4849858273747,1.0
"conn restraint Residue_711-Residue_715 of Nup120",12910.733234476105,12910.733234476105,1.0
"conn restraint Residue_480-Residue_490 of Nup133",327.8163556409753,327.8163556409753,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2970.2757281675163,2970.2757281675163,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",6451.747600426184,6451.747600426184,1.0
"ev_restraint_resol_10",8775.881030541379,8775.881030541379,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",1650.956153869629,1650.956153869629,100.0
"Nup120_close_to_membrane",1507.1076393127441,1507.1076393127441,100.0
