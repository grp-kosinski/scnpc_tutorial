score 42329.394703888756
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2668.2519743574753,-2668.2519743574753,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-672.9534638142914,-672.9534638142914,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1810.6320018254612,1810.6320018254612,1.0
"conn restraint Residue_544-Residue_553 of Nup85",516.2042192982784,516.2042192982784,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1706.4520219248968,1706.4520219248968,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1156.6213599590722,1156.6213599590722,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1531.9254378149976,1531.9254378149976,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",17780.86953295263,17780.86953295263,1.0
"ev_restraint_resol_10",12779.179493677284,12779.179493677284,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",4315.708541870117,4315.708541870117,100.0
"Nup120_close_to_membrane",7535.552215576172,7535.552215576172,100.0
