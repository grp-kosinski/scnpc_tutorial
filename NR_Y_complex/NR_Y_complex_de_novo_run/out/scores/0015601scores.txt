score 33077.66773832476
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-714.5242342118627,-714.5242342118627,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",4272.165280791563,4272.165280791563,1.0
"conn restraint Residue_544-Residue_553 of Nup85",6287.169089429058,6287.169089429058,1.0
"conn restraint Residue_711-Residue_715 of Nup120",408.2433047981917,408.2433047981917,1.0
"conn restraint Residue_480-Residue_490 of Nup133",37.62138129148515,37.62138129148515,1.0
"conn restraint Residue_881-Residue_882 of Nup133",4742.053914670876,4742.053914670876,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",1583.901975140046,1583.901975140046,1.0
"ev_restraint_resol_10",11586.366252471642,11586.366252471642,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",3721.2708847113117,3721.2708847113117,1000.0
"Nup133_close_to_membrane",2859.5420837402344,2859.5420837402344,100.0
"Nup120_close_to_membrane",2521.8759536743164,2521.8759536743164,100.0
