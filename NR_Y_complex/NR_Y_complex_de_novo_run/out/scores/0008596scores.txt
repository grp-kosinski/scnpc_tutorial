score 31506.448782347725
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-144.12033089659784,-144.12033089659784,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-714.9270379782511,-714.9270379782511,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",717.2169766460229,717.2169766460229,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1418.6524656788192,1418.6524656788192,1.0
"conn restraint Residue_480-Residue_490 of Nup133",251.44460848699995,251.44460848699995,1.0
"conn restraint Residue_881-Residue_882 of Nup133",344.6738749295287,344.6738749295287,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",2650.2853437959548,2650.2853437959548,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",7348.357596635677,7348.357596635677,1.0
"ev_restraint_resol_10",6730.307430988669,6730.307430988669,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1709.0956314019695,1709.0956314019695,1000.0
"Nup133_close_to_membrane",4717.998504638672,4717.998504638672,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
