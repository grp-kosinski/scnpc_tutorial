score 23114.48832142315
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",6367.790947551541,6367.790947551541,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2244.850182499286,2244.850182499286,1.0
"conn restraint Residue_711-Residue_715 of Nup120",76.44948993893678,76.44948993893678,1.0
"conn restraint Residue_480-Residue_490 of Nup133",625.6210333680012,625.6210333680012,1.0
"conn restraint Residue_881-Residue_882 of Nup133",917.774824815785,917.774824815785,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",7178.592674044783,7178.592674044783,1.0
"ev_restraint_resol_10",2386.183447278002,2386.183447278002,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",5177.093505859375,5177.093505859375,100.0
"Nup120_close_to_membrane",3014.2221450805664,3014.2221450805664,100.0
