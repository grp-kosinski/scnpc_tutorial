score 43039.282090773806
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-588.3107157369626,-588.3107157369626,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",6367.790947551541,6367.790947551541,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1478.1028194446963,1478.1028194446963,1.0
"conn restraint Residue_711-Residue_715 of Nup120",2084.613136893817,2084.613136893817,1.0
"conn restraint Residue_480-Residue_490 of Nup133",3020.573754832599,3020.573754832599,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1105.4095419572247,1105.4095419572247,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",2193.346709455438,2193.346709455438,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",17769.629729032924,17769.629729032924,1.0
"ev_restraint_resol_10",1184.2709628742202,1184.2709628742202,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",5433.963775634766,5433.963775634766,100.0
"Nup120_close_to_membrane",7164.7186279296875,7164.7186279296875,100.0
