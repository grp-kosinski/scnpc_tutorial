score 41385.64992503059
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2490.677149709458,-2490.677149709458,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",103.70804021646882,103.70804021646882,1.0
"conn restraint Residue_544-Residue_553 of Nup85",515.6521176220572,515.6521176220572,1.0
"conn restraint Residue_711-Residue_715 of Nup120",11695.672625065312,11695.672625065312,1.0
"conn restraint Residue_480-Residue_490 of Nup133",698.1896075184445,698.1896075184445,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2493.379550489857,2493.379550489857,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",4282.452905142416,4282.452905142416,1.0
"ev_restraint_resol_10",17358.655838639555,17358.655838639555,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",110.06988407756789,110.06988407756789,1000.0
"Nup133_close_to_membrane",5875.794219970703,5875.794219970703,100.0
"Nup120_close_to_membrane",4906.794357299805,4906.794357299805,100.0
