score 20767.230808449196
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2668.2519743574753,-2668.2519743574753,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-649.7674606984147,-649.7674606984147,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.574963241611,-706.574963241611,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",45.21944035968403,45.21944035968403,1.0
"conn restraint Residue_544-Residue_553 of Nup85",7143.231596887353,7143.231596887353,1.0
"conn restraint Residue_711-Residue_715 of Nup120",5622.5824506653125,5622.5824506653125,1.0
"conn restraint Residue_480-Residue_490 of Nup133",2494.722174830545,2494.722174830545,1.0
"conn restraint Residue_881-Residue_882 of Nup133",852.6810248093369,852.6810248093369,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2823.119673230991,2823.119673230991,1.0
"ev_restraint_resol_10",3398.4201698164106,3398.4201698164106,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3691.6526794433594,3691.6526794433594,100.0
"Nup120_close_to_membrane",1507.1104049682617,1507.1104049682617,100.0
