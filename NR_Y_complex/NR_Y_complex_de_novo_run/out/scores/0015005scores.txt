score 35930.1695773946
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3715.057495813188,-3715.057495813188,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-629.4218331948545,-629.4218331948545,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1045.2219206776315,1045.2219206776315,1.0
"conn restraint Residue_544-Residue_553 of Nup85",208.2032508568878,208.2032508568878,1.0
"conn restraint Residue_711-Residue_715 of Nup120",2981.0233350541203,2981.0233350541203,1.0
"conn restraint Residue_480-Residue_490 of Nup133",459.2265251568175,459.2265251568175,1.0
"conn restraint Residue_881-Residue_882 of Nup133",3077.7936313908367,3077.7936313908367,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",15156.569047424648,15156.569047424648,1.0
"ev_restraint_resol_10",10634.305053535329,10634.305053535329,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",2778.9758682250977,2778.9758682250977,100.0
"Nup120_close_to_membrane",7414.00146484375,7414.00146484375,100.0
