score 36247.03939398224
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",6789.171483264598,6789.171483264598,1.0
"conn restraint Residue_544-Residue_553 of Nup85",4838.327713769228,4838.327713769228,1.0
"conn restraint Residue_711-Residue_715 of Nup120",7237.994491275114,7237.994491275114,1.0
"conn restraint Residue_480-Residue_490 of Nup133",2152.7400004236915,2152.7400004236915,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2494.2914886209524,2494.2914886209524,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2343.1416987518114,2343.1416987518114,1.0
"ev_restraint_resol_10",7410.872101428911,7410.872101428911,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1755.9701986774999,1755.9701986774999,1000.0
"Nup133_close_to_membrane",5433.962249755859,5433.962249755859,100.0
"Nup120_close_to_membrane",673.9990234375,673.9990234375,100.0
