score 39501.59370737204
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2389.7828544179806,-2389.7828544179806,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-629.4218331948545,-629.4218331948545,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",118.86866449401778,118.86866449401778,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2111.191158434215,2111.191158434215,1.0
"conn restraint Residue_711-Residue_715 of Nup120",5200.131408142412,5200.131408142412,1.0
"conn restraint Residue_480-Residue_490 of Nup133",0.0,0.0,1.0
"conn restraint Residue_881-Residue_882 of Nup133",5242.421803646523,5242.421803646523,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",4456.146591600766,4456.146591600766,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",14447.872294578458,14447.872294578458,1.0
"ev_restraint_resol_10",3813.2352991180924,3813.2352991180924,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3436.7401123046875,3436.7401123046875,100.0
"Nup120_close_to_membrane",7196.351623535156,7196.351623535156,100.0
