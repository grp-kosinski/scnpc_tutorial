score 28679.787493068285
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-118.6905485308098,-118.6905485308098,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1051.9062925851908,1051.9062925851908,1.0
"conn restraint Residue_544-Residue_553 of Nup85",0.0,0.0,1.0
"conn restraint Residue_711-Residue_715 of Nup120",4344.483057811296,4344.483057811296,1.0
"conn restraint Residue_480-Residue_490 of Nup133",287.3939250351953,287.3939250351953,1.0
"conn restraint Residue_881-Residue_882 of Nup133",870.0810861134889,870.0810861134889,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",35.480945479228005,35.480945479228005,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",10948.119141511417,10948.119141511417,1.0
"ev_restraint_resol_10",8939.771450836754,8939.771450836754,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",2022.0001220703125,2022.0001220703125,100.0
"Nup120_close_to_membrane",4521.328353881836,4521.328353881836,100.0
