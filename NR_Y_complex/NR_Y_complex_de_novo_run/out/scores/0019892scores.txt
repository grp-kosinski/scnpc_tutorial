score 36313.85366136513
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2663.9530548835914,-2663.9530548835914,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-695.1045108627361,-695.1045108627361,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",11.634647648087146,11.634647648087146,1.0
"conn restraint Residue_711-Residue_715 of Nup120",978.7570549255761,978.7570549255761,1.0
"conn restraint Residue_480-Residue_490 of Nup133",6.814260090218883,6.814260090218883,1.0
"conn restraint Residue_881-Residue_882 of Nup133",10872.938162925722,10872.938162925722,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",69.07368915547384,69.07368915547384,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",7941.7125703630645,7941.7125703630645,1.0
"ev_restraint_resol_10",5596.644468612665,5596.644468612665,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",8890.670776367188,8890.670776367188,100.0
"Nup120_close_to_membrane",8787.881469726562,8787.881469726562,100.0
