score 17228.93646683939
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-694.0143108003517,-694.0143108003517,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",825.2028286221592,825.2028286221592,1.0
"conn restraint Residue_711-Residue_715 of Nup120",2764.4181204281613,2764.4181204281613,1.0
"conn restraint Residue_480-Residue_490 of Nup133",631.9139805778618,631.9139805778618,1.0
"conn restraint Residue_881-Residue_882 of Nup133",742.6116703124695,742.6116703124695,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",5084.7280808528285,5084.7280808528285,1.0
"ev_restraint_resol_10",4896.554797661435,4896.554797661435,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",2453.0551717144804,2453.0551717144804,1000.0
"Nup133_close_to_membrane",4717.999267578125,4717.999267578125,100.0
"Nup120_close_to_membrane",0.0,0.0,100.0
