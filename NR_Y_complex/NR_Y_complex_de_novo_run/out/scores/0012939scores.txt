score 33335.90894261066
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3715.057495813188,-3715.057495813188,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-810.2755386342355,-810.2755386342355,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1890.75266647743,1890.75266647743,1.0
"conn restraint Residue_711-Residue_715 of Nup120",3416.782049510545,3416.782049510545,1.0
"conn restraint Residue_480-Residue_490 of Nup133",768.7785156308405,768.7785156308405,1.0
"conn restraint Residue_881-Residue_882 of Nup133",5399.261376202454,5399.261376202454,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",4179.008466730779,4179.008466730779,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",19355.13279479959,19355.13279479959,1.0
"ev_restraint_resol_10",426.63111895430666,426.63111895430666,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3691.6526794433594,3691.6526794433594,100.0
"Nup120_close_to_membrane",2235.4028701782227,2235.4028701782227,100.0
