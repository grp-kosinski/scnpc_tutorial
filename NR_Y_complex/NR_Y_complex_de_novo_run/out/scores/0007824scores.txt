score 44288.215794560034
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1158.9207577402758,-1158.9207577402758,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-697.1398708797929,-697.1398708797929,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-720.4621136690541,-720.4621136690541,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",5085.197475401485,5085.197475401485,1.0
"conn restraint Residue_711-Residue_715 of Nup120",13266.48052474927,13266.48052474927,1.0
"conn restraint Residue_480-Residue_490 of Nup133",30.032236828396947,30.032236828396947,1.0
"conn restraint Residue_881-Residue_882 of Nup133",5242.421803646523,5242.421803646523,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",24.805863321106635,24.805863321106635,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2565.6240179044935,2565.6240179044935,1.0
"ev_restraint_resol_10",8161.838023132419,8161.838023132419,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",5264.105224609375,5264.105224609375,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
