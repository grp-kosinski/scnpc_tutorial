score 19563.32756497313
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-697.1398708797929,-697.1398708797929,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",3266.6499225998728,3266.6499225998728,1.0
"conn restraint Residue_544-Residue_553 of Nup85",3841.966130891497,3841.966130891497,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1020.7029522455167,1020.7029522455167,1.0
"conn restraint Residue_480-Residue_490 of Nup133",2418.6796557434527,2418.6796557434527,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2057.9650286355354,2057.9650286355354,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2716.9454669985626,2716.9454669985626,1.0
"ev_restraint_resol_10",4681.994210983959,4681.994210983959,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",1167.4022674560547,1167.4022674560547,100.0
"Nup120_close_to_membrane",3301.912307739258,3301.912307739258,100.0
