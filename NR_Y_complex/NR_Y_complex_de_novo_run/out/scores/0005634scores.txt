score 32688.15798464697
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1721.6785784706417,-1721.6785784706417,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1890.8772968111116,1890.8772968111116,1.0
"conn restraint Residue_711-Residue_715 of Nup120",148.881220334257,148.881220334257,1.0
"conn restraint Residue_480-Residue_490 of Nup133",863.3022849098845,863.3022849098845,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2129.7926836739493,2129.7926836739493,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",713.6529510211735,713.6529510211735,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",13629.874203290348,13629.874203290348,1.0
"ev_restraint_resol_10",5190.886304714488,5190.886304714488,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3987.4385833740234,3987.4385833740234,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
