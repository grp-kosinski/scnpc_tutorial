score 30236.00950152991
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2336.9020107503266,-2336.9020107503266,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-671.8315915911545,-671.8315915911545,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",3727.1343826825205,3727.1343826825205,1.0
"conn restraint Residue_544-Residue_553 of Nup85",3385.847095304667,3385.847095304667,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1811.3455783025743,1811.3455783025743,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1846.1607605170516,1846.1607605170516,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1489.665359926361,1489.665359926361,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",7200.525026514193,7200.525026514193,1.0
"ev_restraint_resol_10",12156.628231212862,12156.628231212862,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3088.655471801758,3088.655471801758,100.0
"Nup120_close_to_membrane",2021.9970703125,2021.9970703125,100.0
