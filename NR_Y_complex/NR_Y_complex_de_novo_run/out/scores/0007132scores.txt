score 28912.814648988133
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-807.0368347996739,-807.0368347996739,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-810.2755386342355,-810.2755386342355,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",3998.6122438493608,3998.6122438493608,1.0
"conn restraint Residue_544-Residue_553 of Nup85",722.3730161631656,722.3730161631656,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1604.1667644587624,1604.1667644587624,1.0
"conn restraint Residue_480-Residue_490 of Nup133",62.59902392085741,62.59902392085741,1.0
"conn restraint Residue_881-Residue_882 of Nup133",4479.794594268505,4479.794594268505,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",2833.9669570486976,2833.9669570486976,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",7906.693356627938,7906.693356627938,1.0
"ev_restraint_resol_10",10138.055400914984,10138.055400914984,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",14.845793467786343,14.845793467786343,1000.0
"Nup133_close_to_membrane",0.0,0.0,100.0
"Nup120_close_to_membrane",2235.4028701782227,2235.4028701782227,100.0
