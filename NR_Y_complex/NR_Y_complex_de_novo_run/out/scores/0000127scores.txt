score 38124.52621202582
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",1785.551802540845,1785.551802540845,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1797.2401049816858,1797.2401049816858,1.0
"conn restraint Residue_480-Residue_490 of Nup133",287.9928683817929,287.9928683817929,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1856.437531492229,1856.437531492229,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",6847.595435298279,6847.595435298279,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",16382.869449149293,16382.869449149293,1.0
"ev_restraint_resol_10",8382.223910003288,8382.223910003288,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",2336.0159680706815,2336.0159680706815,1000.0
"Nup133_close_to_membrane",0.0,0.0,100.0
"Nup120_close_to_membrane",3301.912307739258,3301.912307739258,100.0
