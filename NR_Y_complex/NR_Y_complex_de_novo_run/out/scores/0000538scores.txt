score 20605.85223592506
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-421.6879855026631,-421.6879855026631,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",417.1428636427921,417.1428636427921,1.0
"conn restraint Residue_544-Residue_553 of Nup85",2558.939702074726,2558.939702074726,1.0
"conn restraint Residue_711-Residue_715 of Nup120",4824.266966023448,4824.266966023448,1.0
"conn restraint Residue_480-Residue_490 of Nup133",160.3982965910603,160.3982965910603,1.0
"conn restraint Residue_881-Residue_882 of Nup133",1597.1657498323032,1597.1657498323032,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",2394.4468061169473,2394.4468061169473,1.0
"ev_restraint_resol_10",6228.604321731576,6228.604321731576,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1501.44212151414,1501.44212151414,1000.0
"Nup133_close_to_membrane",4043.9971923828125,4043.9971923828125,100.0
"Nup120_close_to_membrane",1507.1076393127441,1507.1076393127441,100.0
