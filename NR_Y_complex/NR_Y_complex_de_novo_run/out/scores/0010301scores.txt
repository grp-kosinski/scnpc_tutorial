score 24915.479549097854
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-694.0143108003517,-694.0143108003517,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-810.2755386342355,-810.2755386342355,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-687.3553582782749,-687.3553582782749,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",454.2139490959264,454.2139490959264,1.0
"conn restraint Residue_711-Residue_715 of Nup120",8.363895504993234,8.363895504993234,1.0
"conn restraint Residue_480-Residue_490 of Nup133",547.3150001615807,547.3150001615807,1.0
"conn restraint Residue_881-Residue_882 of Nup133",11212.574627090442,11212.574627090442,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",5127.314272966946,5127.314272966946,1.0
"ev_restraint_resol_10",7331.758436620908,7331.758436620908,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",1501.44212151414,1501.44212151414,1000.0
"Nup133_close_to_membrane",2235.4055404663086,2235.4055404663086,100.0
"Nup120_close_to_membrane",1507.1076393127441,1507.1076393127441,100.0
