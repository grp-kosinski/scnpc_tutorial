score 17701.47236498596
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2668.2519743574753,-2668.2519743574753,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-717.4361808126814,-717.4361808126814,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1289.9252084779923,1289.9252084779923,1.0
"conn restraint Residue_544-Residue_553 of Nup85",33.41833040760261,33.41833040760261,1.0
"conn restraint Residue_711-Residue_715 of Nup120",12130.01741602912,12130.01741602912,1.0
"conn restraint Residue_480-Residue_490 of Nup133",481.9716957517886,481.9716957517886,1.0
"conn restraint Residue_881-Residue_882 of Nup133",127.30868428098336,127.30868428098336,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",4695.1217922469605,4695.1217922469605,1.0
"ev_restraint_resol_10",651.1883575102501,651.1883575102501,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",966.1382683573315,966.1382683573315,1000.0
"Nup133_close_to_membrane",1650.956153869629,1650.956153869629,100.0
"Nup120_close_to_membrane",2521.876335144043,2521.876335144043,100.0
