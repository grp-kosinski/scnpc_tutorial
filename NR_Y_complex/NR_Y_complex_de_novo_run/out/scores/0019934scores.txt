score 23516.395210400206
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2668.2519743574753,-2668.2519743574753,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-725.9584469154344,-725.9584469154344,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-717.4361808126814,-717.4361808126814,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",1474.1006821571782,1474.1006821571782,1.0
"conn restraint Residue_544-Residue_553 of Nup85",186.50951827030076,186.50951827030076,1.0
"conn restraint Residue_711-Residue_715 of Nup120",0.0,0.0,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1497.8542819555194,1497.8542819555194,1.0
"conn restraint Residue_881-Residue_882 of Nup133",891.2162562904542,891.2162562904542,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",3832.2213569802843,3832.2213569802843,1.0
"ev_restraint_resol_10",18649.235941471867,18649.235941471867,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",1650.956153869629,1650.956153869629,100.0
"Nup120_close_to_membrane",2235.404586791992,2235.404586791992,100.0
