score 17529.004629888146
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2336.9020107503266,-2336.9020107503266,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-708.0265481429858,-708.0265481429858,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",2097.808932494735,2097.808932494735,1.0
"conn restraint Residue_544-Residue_553 of Nup85",8691.50599197395,8691.50599197395,1.0
"conn restraint Residue_711-Residue_715 of Nup120",301.8616218616011,301.8616218616011,1.0
"conn restraint Residue_480-Residue_490 of Nup133",462.60352999132635,462.60352999132635,1.0
"conn restraint Residue_881-Residue_882 of Nup133",6312.92340998791,6312.92340998791,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",1220.9414231956644,1220.9414231956644,1.0
"ev_restraint_resol_10",1026.7720694144077,1026.7720694144077,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",932.5914445952696,932.5914445952696,1000.0
"Nup133_close_to_membrane",2022.003173828125,2022.003173828125,100.0
"Nup120_close_to_membrane",953.178596496582,953.178596496582,100.0
