score 19372.31436857267
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-678.4859665353889,-678.4859665353889,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-693.435876312438,-693.435876312438,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-717.4361808126814,-717.4361808126814,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",11.10926487091236,11.10926487091236,1.0
"conn restraint Residue_544-Residue_553 of Nup85",237.68706217848884,237.68706217848884,1.0
"conn restraint Residue_711-Residue_715 of Nup120",2428.0470134702373,2428.0470134702373,1.0
"conn restraint Residue_480-Residue_490 of Nup133",664.0521499420441,664.0521499420441,1.0
"conn restraint Residue_881-Residue_882 of Nup133",127.30868428098336,127.30868428098336,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",664.8895923504167,664.8895923504167,1.0
"ev_restraint_resol_10",8615.30488284563,8615.30488284563,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",35.84105117730632,35.84105117730632,1000.0
"Nup133_close_to_membrane",1507.1104049682617,1507.1104049682617,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
