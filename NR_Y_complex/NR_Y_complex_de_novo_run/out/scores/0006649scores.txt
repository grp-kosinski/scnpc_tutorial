score 22092.931330246105
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-810.2755386342355,-810.2755386342355,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-691.1936739161237,-691.1936739161237,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",4123.4593218720165,4123.4593218720165,1.0
"conn restraint Residue_544-Residue_553 of Nup85",722.3730161631656,722.3730161631656,1.0
"conn restraint Residue_711-Residue_715 of Nup120",1604.1667644587624,1604.1667644587624,1.0
"conn restraint Residue_480-Residue_490 of Nup133",950.5158899697619,950.5158899697619,1.0
"conn restraint Residue_881-Residue_882 of Nup133",2129.7926836739493,2129.7926836739493,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",693.5744512806697,693.5744512806697,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",9352.570940883954,9352.570940883954,1.0
"ev_restraint_resol_10",1854.0723703102458,1854.0723703102458,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",3436.7408752441406,3436.7408752441406,100.0
"Nup120_close_to_membrane",2235.4028701782227,2235.4028701782227,100.0
