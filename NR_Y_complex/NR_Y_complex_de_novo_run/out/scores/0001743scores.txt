score 12561.056927934707
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-807.0368347996739,-807.0368347996739,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-696.4070379554294,-696.4070379554294,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-672.9534638142914,-672.9534638142914,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-698.972743562084,-698.972743562084,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",38.15863418355818,38.15863418355818,1.0
"conn restraint Residue_711-Residue_715 of Nup120",989.6541641627612,989.6541641627612,1.0
"conn restraint Residue_480-Residue_490 of Nup133",1496.5424596209896,1496.5424596209896,1.0
"conn restraint Residue_881-Residue_882 of Nup133",320.9263986361415,320.9263986361415,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",107.73739072265444,107.73739072265444,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",3150.5604434481943,3150.5604434481943,1.0
"ev_restraint_resol_10",758.4075438406672,758.4075438406672,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",673.9990234375,673.9990234375,100.0
"Nup120_close_to_membrane",10000.0,10000.0,100.0
