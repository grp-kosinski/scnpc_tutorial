score 17210.41103416557
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-588.3107157369626,-588.3107157369626,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-686.0388056700027,-686.0388056700027,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-666.5585578577212,-666.5585578577212,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",6852.433092560191,6852.433092560191,1.0
"conn restraint Residue_711-Residue_715 of Nup120",26.911798678763045,26.911798678763045,1.0
"conn restraint Residue_480-Residue_490 of Nup133",11.388291851799405,11.388291851799405,1.0
"conn restraint Residue_881-Residue_882 of Nup133",5132.0450793889395,5132.0450793889395,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",207.97762151404729,207.97762151404729,1.0
"ev_restraint_resol_10",264.82580063626426,264.82580063626426,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",2165.8334753061704,2165.8334753061704,1000.0
"Nup133_close_to_membrane",6638.128662109375,6638.128662109375,100.0
"Nup120_close_to_membrane",674.005126953125,674.005126953125,100.0
