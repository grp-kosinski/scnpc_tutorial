score 44430.86441396999
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-733.0793166783093,-733.0793166783093,1000.0
"DiscreteMoverRestraint rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0",-706.3940354463247,-706.3940354463247,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-689.8979153151489,-689.8979153151489,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-69.48023418723712,-69.48023418723712,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-700.306446411319,-700.306446411319,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.8585681286361,-692.8585681286361,1000.0
"DiscreteMoverRestraint rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0",-717.4361808126814,-717.4361808126814,1000.0
"conn restraint Residue_436-Residue_506 of Nup84",0.0,0.0,1.0
"conn restraint Residue_544-Residue_553 of Nup85",356.8666429736137,356.8666429736137,1.0
"conn restraint Residue_711-Residue_715 of Nup120",15611.008069610523,15611.008069610523,1.0
"conn restraint Residue_480-Residue_490 of Nup133",2463.1058477784863,2463.1058477784863,1.0
"conn restraint Residue_881-Residue_882 of Nup133",3778.497152041216,3778.497152041216,1.0
"conn restraint Residue_99-Residue_149 of Nup145c",463.0922234724161,463.0922234724161,1.0
"conn restraint Residue_550-Residue_554 of Nup145c",11833.613607395184,11833.613607395184,1.0
"ev_restraint_resol_10",1950.5039595239095,1950.5039595239095,10.0
"membrane_cerevisiae_no_neg_relative_NR_v3.mrc",0.0,0.0,1000.0
"Nup133_close_to_membrane",4717.999267578125,4717.999267578125,100.0
"Nup120_close_to_membrane",7565.630340576172,7565.630340576172,100.0
