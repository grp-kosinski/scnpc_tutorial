{"rb_ScNup133N_56-480_renamed.pdb_0": [{"rot": [[0.28977869497, 0.8949427608595089, -0.33927240195253694], [-0.6419874516444672, -0.08115567887519992, -0.7624079404868159], [-0.709845349264561, 0.4387382027718415, 0.5510248356980001]], "trans": [609.901601393, 445.89125141, 34.80699725490001]}], "rb_ScNup133C_490_881_renamed_relative_3CQC.pdb_0": [{"rot": [[0.27748402610700007, -0.7183063568067702, 0.6379957625458295], [-0.23362399379222337, 0.5936900475939999, 0.7700337375157047], [-0.9318918632438105, -0.36272317979255037, -0.0030740951774999026]], "trans": [447.981307531, 504.308593416, 274.272980388]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb_0": [{"rot": [[0.7106325419199999, 0.33854855332525624, -0.6167546249568748], [0.48355533666623995, 0.401734396422, 0.7776784111141639], [0.5110534480325112, -0.8508785763978473, 0.12177857562500002]], "trans": [521.905727103, 518.399428919, 262.142687384]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0": [{"rot": [[-0.628124340307, -0.5723255457058674, 0.5271653278112827], [-0.20492623993568654, 0.775249263078, 0.5974895951251697], [-0.7506430904895598, 0.2672677493254641, -0.6042374540450001]], "trans": [440.15140262400007, 477.200353954, 301.670871772]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0": [{"rot": [[0.814334950513, -0.5304219287853842, -0.23560807676432216], [0.21942230647432012, 0.657173009911, -0.7210946445966759], [0.5373196811903315, 0.5355149040952172, 0.651545353523]], "trans": [560.335775239, 589.020849415, 370.383526072]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0": [{"rot": [[-0.11035910258400011, 0.8286231779009129, -0.5488210068157433], [0.7184554235859179, -0.31504670462, -0.6201349677514225], [-0.6867624573291944, -0.46274096744665955, -0.560560544678]], "trans": [643.6935378839999, 477.924060656, 227.84363451299998]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0": [{"rot": [[0.31082112250000005, 0.9143631315049157, 0.2594808153840368], [0.6456286083953351, -0.40346525205499995, 0.6483667869390739], [0.6975341782517492, -0.03399785476643874, -0.7157445186949999]], "trans": [641.90648174, 411.173498689, 364.717488015]}]}
