{"rb_ScNup133N_56-480_renamed_1.pdb_0": [{"rot": [[-0.5023188611760729, 0.010369392686066348, 0.8646202850975064], [0.20343373609021914, -0.9704429556963085, 0.12982751926993655], [0.8404108975537776, 0.24110774652576558, 0.4853623160465993]], "trans": [658.9373823005278, 229.42011494523175, -380.4716192778205]}], "rb_ScNup133N_56-480_renamed_1.pdb_1": [{"rot": [[-0.19608217466796718, -0.6720344729299832, 0.7140878433156512], [0.5462711105207398, -0.6796204324373508, -0.4895956919991267], [0.8143338716938626, 0.2940845711821442, 0.5003744701767826]], "trans": [305.64975240415276, -185.4102137587403, -350.2528238760709]}], "rb_ScNup133N_56-480_renamed_1.pdb_2": [{"rot": [[0.2622031619144809, -0.9497450810256236, 0.17097889621142548], [0.5713647872218401, 0.010005156752578959, -0.8206352275897586], [0.7776836000617393, 0.31286447210571094, 0.5452742798711716]], "trans": [-236.66438353097269, -228.01172816172416, -306.77851443375175]}], "rb_ScNup133N_56-480_renamed_1.pdb_3": [{"rot": [[0.6040671365780276, -0.6600756416588547, -0.44654567716536114], [0.26401453656117385, 0.6944414180052122, -0.6693634598955157], [0.7519303285483359, 0.2864459185388376, 0.5937588035234765]], "trans": [-650.307844705472, 126.57225635008106, -275.4925495087503]}], "rb_ScNup133N_56-480_renamed_1.pdb_4": [{"rot": [[0.6292410126827244, 0.027281403250175223, -0.7767312746341025], [-0.19572953098570608, 0.972735938927473, -0.12439752336360976], [0.7521606867273322, 0.23030527166260362, 0.6174251235463937]], "trans": [-692.960545851257, 670.6215588106569, -274.69868358505136]}], "rb_ScNup133N_56-480_renamed_1.pdb_5": [{"rot": [[0.32297757865478705, 0.709662605059423, -0.6261505175806859], [-0.538542858258485, 0.681859865210356, 0.4950138523658634], [0.7782397275294217, 0.17733051400022787, 0.6024091759753856]], "trans": [-339.6340198947772, 1085.4245435386133, -304.8379696130804]}], "rb_ScNup133N_56-480_renamed_1.pdb_6": [{"rot": [[-0.1353097275377989, 0.9873194341740861, -0.08301573667567735], [-0.563600564472855, -0.0077874978100118675, 0.8260107496901808], [0.8148899811382653, 0.15855500553441326, 0.5575075146224097]], "trans": [202.68998569390976, 1127.983890153408, -348.2303839724988]}], "rb_ScNup133N_56-480_renamed_1.pdb_7": [{"rot": [[-0.47714974007639943, 0.6975966051375043, 0.5344970552263587], [-0.2562234920147799, -0.6922010008484809, 0.6746905191005053], [0.840641212221226, 0.18497770385910312, 0.5090241658214641]], "trans": [616.3123350117357, 773.3678501164869, -379.4320335973298]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_7": [{"rot": [[0.6774974610140331, 0.3977758167766323, -0.6186853723074599], [-0.3002785684067709, 0.9174391936262135, 0.2610327706516212], [0.6714387326295062, 0.008928918532762314, 0.7410062771250433]], "trans": [271.4344634664142, 108.33027580218334, -300.0966469406703]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_8": [{"rot": [[0.28188682034952023, 0.9289616375862904, -0.23993769275841936], [-0.6511283841066822, 0.36889736910507925, 0.6632846737849218], [0.7046784003528537, -0.03074096550772515, 0.7088605963909932]], "trans": [-52.84549813979697, 6.694778357341619, -273.71378306450003]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_9": [{"rot": [[-0.2452665575672155, 0.9180410461789548, 0.3115203898138542], [-0.6184875582087956, -0.39562200936011926, 0.6789376746438373], [0.7465369756951407, -0.026150778965638777, 0.6648298133202519]], "trans": [-353.8715990875472, 164.88877521674334, -250.44858905419795]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_10": [{"rot": [[-0.5951487910899212, 0.37141147898138216, 0.7126369550795588], [-0.22147754675973863, -0.9282527101412323, 0.29882202461790297], [0.7724931150149434, 0.02001048216103693, 0.6347077814697011]], "trans": [-455.29659163435747, 490.24010927997494, -243.9061386990482]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_11": [{"rot": [[-0.5627939344815037, -0.3907037560651922, 0.7284322633624742], [0.30732758769247, -0.916972158627803, -0.2543851688770234], [0.7673413459171565, 0.08070090023051352, 0.6361404118180424]], "trans": [-297.7023717146475, 792.1534983941641, -257.8950867644606]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_12": [{"rot": [[-0.16715591894665216, -0.9218468084456708, 0.3496532003566224], [0.6581463412333306, -0.36838866095217504, -0.6566073316701857], [0.7340996473792083, 0.12036717260279678, 0.6682884493071295]], "trans": [26.59205790337927, 893.7639390326117, -284.19650532836266]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_13": [{"rot": [[0.35999492751829865, -0.910866588958531, -0.20180611804438078], [0.6254641414941822, 0.39612983491995213, -0.6722170494648544], [0.6922414751125779, 0.11577223762776279, 0.7123191202885265]], "trans": [327.6123611372892, 735.5467191572822, -307.37864764483214]}], "rb_ScNup133C_490_881_renamed_relative_3CQC_1.pdb_14": [{"rot": [[0.709846206227245, -0.364195465000434, -0.6028930475448377], [0.22842668190123427, 0.9287176143958942, -0.2920699294798913], [0.6662879366312862, 0.06960787300236958, 0.7424386368688702]], "trans": [429.0185133056459, 410.18783382912306, -313.8370829754876]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_14": [{"rot": [[0.9723349495358551, 0.2065533038292648, 0.10908931473027768], [-0.22276160748885598, 0.9604761314692833, 0.16692173946727523], [-0.07029944622887872, -0.18660475223052458, 0.97991665681572]], "trans": [-77.13329178338381, 94.25087962687851, 101.40679289991391]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_15": [{"rot": [[0.5308947259984214, 0.819871050268275, 0.2143880846480437], [-0.8468304561740233, 0.5228329328469654, 0.09759048532892967], [-0.03207753736115991, -0.23336063348953306, 0.9718610221292996]], "trans": [-301.9483273345903, 265.5234647904939, 118.53385958846033]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_16": [{"rot": [[-0.22126726379945694, 0.945219531736778, 0.24000173914788114], [-0.9747967563939994, -0.22156180859242836, -0.026108402804265865], [0.028497047117889404, -0.2397298517009473, 0.9704213087669719]], "trans": [-340.2451686544123, 546.0583079920447, 120.2626587895588]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_17": [{"rot": [[-0.8435239207501262, 0.5091678403769355, 0.17092543827125686], [-0.5316960451550887, -0.8366311573478467, -0.13171037210956274], [0.07593886151880097, -0.20198122903054408, 0.976440921116346]], "trans": [-169.58707277297202, 771.5142593379524, 105.60444045632408]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_18": [{"rot": [[-0.9713485213930216, -0.23283879468380753, 0.04762505304753974], [0.2228970331789255, -0.9620588164405481, -0.15735229998015093], [0.0824558220359377, -0.14222844089422532, 0.9863934853866252]], "trans": [110.05395480249479, 809.81616039605, 83.17020047042979]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_19": [{"rot": [[-0.5298596123446664, -0.8461224243333143, -0.05766831232378514], [0.846931213557574, -0.5243674946440355, -0.08801278351825684], [0.04423020130921769, -0.09547551309655922, 0.9944486490971234]], "trans": [334.8621705190547, 638.5263342648782, 66.12623783958327]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_20": [{"rot": [[0.22231237848079566, -0.9714128510237254, -0.08327231977174393], [0.9748384742331007, 0.22003706028565717, 0.035688110845559784], [-0.01634489305839424, -0.08911096995770046, 0.9958875837684222]], "trans": [373.1436651533691, 357.9888930738058, 64.4811510939939]}], "rb_ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb_21": [{"rot": [[0.8445344932327383, -0.535313176204782, -0.014187780716817366], [0.531688938409407, 0.8350721639664913, 0.14128465501036472], [-0.0637837166775205, -0.1268632505884508, 0.9898673411810961]], "trans": [202.47451271884609, 132.54674224566304, 79.22266014613788]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_21": [{"rot": [[0.9917046834943322, -0.043968740409608076, 0.12078315529161408], [0.046809754861615205, 0.9986875642240429, -0.020784511397749107], [-0.11971076637128826, 0.026265927187901984, 0.992461300748678]], "trans": [-15.885569090313993, -19.928028420967394, 55.29433216476423]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_22": [{"rot": [[0.733694949957069, 0.6734123543593608, 0.09059537186675702], [-0.6727137279966066, 0.738672292776584, -0.04265541054885558], [-0.09564497149563463, -0.02964869103968419, 0.994973866263447]], "trans": [-339.9337887987274, 138.96635498078973, 80.391123882859]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_23": [{"rot": [[0.04400016125548001, 0.9975805061477987, 0.05382489724460294], [-0.998266552345177, 0.04601060358596698, -0.03670033823450919], [-0.03908805800195394, -0.05211677380225417, 0.997875726536165]], "trans": [-457.15015458127505, 481.1435889224644, 88.11037851748733]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_24": [{"rot": [[-0.6733467506925228, 0.738633440867099, 0.03201239392250191], [-0.7391352833858807, -0.6735265192901271, -0.0064078597829736895], [0.01682813673267538, -0.027976201417527546, 0.9994669309028441]], "trans": [-298.86575327251967, 806.1515418572052, 73.95405276338016]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_25": [{"rot": [[-0.9981138502763414, 0.04826600051231628, 0.03793593390283754], [-0.04712267222596573, -0.9984240837974496, 0.030476263804895258], [0.039347117414361775, 0.028631138429452217, 0.9988153294095021]], "trans": [42.19609437007523, 923.5960891388833, 46.23912675267297]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_26": [{"rot": [[-0.7400479913436707, -0.669094834175809, 0.06812542394321541], [0.6723805361392723, -0.738352515591952, 0.052344792865181414], [0.01527694764214569, 0.08454386789283251, 0.9963026393985195]], "trans": [366.23846236468984, 764.6766757969548, 21.225399824081762]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_27": [{"rot": [[-0.050327792785880454, -0.9932089714492038, 0.10489543510617266], [0.997879217254239, -0.04566540633752664, 0.04638683472810942], [-0.041281727742014794, 0.10700752168347974, 0.9934008301072588]], "trans": [483.43467588195375, 422.49065172602934, 13.590270807111153]}], "rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb_28": [{"rot": [[0.6669989280270765, -0.7342057796837785, 0.12670557639529154], [0.7386916541903837, 0.6738513684763441, 0.016092645277585797], [-0.09719603922075565, 0.08286257467331432, 0.9918098223340523]], "trans": [325.13145391610743, 97.49553144497361, 27.83051083640811]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_28": [{"rot": [[0.6692311065487054, -0.7171303056027948, 0.1945606610125464], [0.30108676909833487, 0.5010931196315865, 0.8113275805319402], [-0.6793206043552062, -0.48438601367497003, 0.5512655496714362]], "trans": [477.47706225717025, -106.93844556083263, 715.208241971934]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_29": [{"rot": [[0.673279607001901, -0.16471418210823827, 0.720807747605269], [-0.2978798039925953, 0.8318301958303281, 0.4683226960955579], [-0.676729039696613, -0.5300261913502892, 0.5109892790597547]], "trans": [-38.46388288841911, -232.25239272669808, 756.9828798471676]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_30": [{"rot": [[0.25393576057944867, 0.4591807726138998, 0.8512753065610676], [-0.7241129370525147, 0.6737425526547766, -0.147415830681129], [-0.6412309130821117, -0.5789853113887506, 0.5035860654386236]], "trans": [-491.7552902812528, 45.160871386392486, 791.8045035824899]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_31": [{"rot": [[-0.34314289473614434, 0.7890680971685297, 0.5095336022510544], [-0.7279192508477469, 0.11944016051293871, -0.67517968889911], [-0.5936215276060621, -0.6025824309482313, 0.5333928157326137]], "trans": [-616.8507487225015, 562.7888619291464, 799.2981199100411]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_32": [{"rot": [[-0.7681792638519634, 0.6316951454202728, -0.10422025638193386], [-0.3070689530880203, -0.5063588236147671, -0.8057973689438905], [-0.5617911325726695, -0.5869940246714013, 0.582948315344326]], "trans": [-340.4656614720536, 1017.3979202821306, 775.097867626338]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_33": [{"rot": [[-0.772181048727158, 0.0792532114618782, -0.6304406050212862], [0.29189791780981866, -0.8370525310606554, -0.4627511921359503], [-0.5643864222010652, -0.5413520007517564, 0.6232222538686752]], "trans": [175.49160366831194, 1142.6717668696242, 733.4046006439173]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_34": [{"rot": [[-0.3528039473566717, -0.5446294303625582, -0.7608601437271544], [0.7180981234409366, -0.6789159252216492, 0.1529975541995268], [-0.599887039233351, -0.4923941003539616, 0.6306215902555642]], "trans": [628.7679191211944, 865.2233327946157, 698.6668275373322]}], "rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb_35": [{"rot": [[0.24427502105953464, -0.8744735317270315, -0.419077267810145], [0.7218575739237251, -0.12458765952438142, 0.6807345724018946], [-0.6474962216533139, -0.4688005518374406, 0.6008116889189101]], "trans": [753.8295414375111, 347.5859395563456, 691.2584304962077]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_35": [{"rot": [[0.9973867092214473, -0.03441462264408531, 0.06352468824541337], [0.03238683537542567, 0.9989409988524419, 0.032679866983365564], [-0.06458208081795465, -0.030537101367854168, 0.9974450562699043]], "trans": [-1.7242334627917444, -29.287603303748256, 53.252280046955434]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_36": [{"rot": [[0.7286450077300743, 0.6792413987308381, 0.08779279560541291], [-0.6837412935524934, 0.7288462932224575, 0.03579000345132145], [-0.03967740164378496, -0.08610576697328223, 0.9954956055617421]], "trans": [-336.53299563907797, 122.25018249695046, 79.24254695057454]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_37": [{"rot": [[0.03375214926435144, 0.9936696318883638, 0.10715155193873652], [-0.999281441071634, 0.03170266156234358, 0.02077360776709354], [0.01724511379592747, -0.10777571114451674, 0.9940256546676541]], "trans": [-466.5466241346931, 466.8606727346852, 87.96998372368833]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_38": [{"rot": [[-0.6802138316971971, 0.7246735844067254, 0.11026032482685892], [-0.7293857396492034, -0.6840933269761996, -0.0035725037463183035], [0.07283945334913149, -0.08285237503983574, 0.9938963215477039]], "trans": [-315.5993888454803, 802.6663000154587, 74.34593974893511]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_39": [{"rot": [[-0.9950015965869643, 0.029834933744936763, 0.09529795127822029], [-0.03216289670785189, -0.9992182895838548, -0.022986035684185096], [0.09453766902537243, -0.025936200368400417, 0.9951833713671557]], "trans": [27.883604779385678, 932.9475098140282, 46.37557698693166]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_40": [{"rot": [[-0.7262040342250395, -0.6838000413143521, 0.07102960068708015], [0.6839441155152914, -0.7290675799030846, -0.026094267306504142], [0.06962864013666735, 0.029630515229199527, 0.9971328321943717]], "trans": [362.68708930351175, 781.383849260513, 20.46833322696125]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_41": [{"rot": [[-0.03128654668866826, -0.9981739101282621, 0.05167201501347176], [0.9994297121510374, -0.03189929278989134, -0.011076352702855109], [0.012704427023327032, 0.05129600626520016, 0.9986026823893717]], "trans": [492.68037610279765, 436.7635645499556, 11.825042056937946]}], "rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb_42": [{"rot": [[0.6826584034867698, -0.7291220276740539, 0.048565140886952865], [0.7294779823432528, 0.6838755590919687, 0.013270001999348545], [-0.04288796364144891, 0.026368322607288684, 0.9987318629830362]], "trans": [341.713478204141, 100.97019440699489, 25.533070215853684]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_42": [{"rot": [[0.9999757887474392, 0.004992819031246382, 0.0048470276519620645], [-0.006283284426511775, 0.9471929950081053, 0.32060248056493795], [-0.0029903604721196308, -0.3206251736306697, 0.9472014335813423]], "trans": [-5.228991431017562, -60.783190345817616, 208.25176718991963]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_43": [{"rot": [[0.7044360886908341, 0.6649624275582731, 0.24818293028684968], [-0.709361988470746, 0.6477735209491611, 0.2778399446983019], [0.023986793507846382, -0.3717720208581257, 0.9280141153259882]], "trans": [-358.1575412329388, 111.37752266701722, 235.58595308656697]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_44": [{"rot": [[-0.00021431681812134062, 0.9207438636837284, 0.3901675173013005], [-0.9966692549841323, -0.032014711120604056, 0.07500302954706853], [0.08154967956819717, -0.3888518943770538, 0.9176839619398937]], "trans": [-486.45153049844185, 483.41806221634556, 244.04286382245925]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_45": [{"rot": [[-0.7011812846615642, 0.6224967556070715, 0.34762421563957246], [-0.6998964362106712, -0.6939421698406502, -0.169083539992666], [0.1359771474180859, -0.3618591634538493, 0.9222625229317951]], "trans": [-314.9526180312821, 837.3919512577435, 228.69232674725595]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_46": [{"rot": [[-0.9878284814425586, -0.055061535476168225, 0.14547549127565923], [0.007102766115398909, -0.950242416715606, -0.31143040986389486], [0.155384818963751, -0.30660655046083124, 0.9390676127149266]], "trans": [55.8746284498759, 965.9364113541034, 198.55090782337044]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_47": [{"rot": [[-0.6922339375613242, -0.7150078090775246, -0.09785708276104521], [0.7101582505827134, -0.6507711532926421, -0.2686487766051868], [0.1284034065655731, -0.25546181515115285, 0.9582545727425638]], "trans": [408.79629263145836, 793.74840209927, 171.2998801516082]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_48": [{"rot": [[0.012438895293443775, -0.9707362328788653, -0.23982585361054565], [0.9974102013079886, 0.02903713177025552, -0.0658007242005223], [0.07083900204431964, -0.23838626460999163, 0.9685834112943805]], "trans": [537.0678007796934, 421.6982039608347, 162.9272848827642]}], "rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb_49": [{"rot": [[0.7133827357484785, -0.6724374904465896, -0.19726351354953567], [0.7005823753038533, 0.6909411597864217, 0.1782824980939076], [0.016413645226644596, -0.26538299710962243, 0.9640033491100988]], "trans": [365.5478081323435, 67.73818571126651, 178.36191128010728]}]}
