from string import Template

protocol = 'refine'

SA_schedule = [
    (100,   20000),
    (10,   20000),
    (1,   20000)
]

do_ini_opt = False

traj_frame_period = 50
print_frame_period = traj_frame_period

print_log_scores_to_files = True
print_log_scores_to_files_frame_period = 10

print_total_score_to_files = True
print_total_score_to_files_frame_period = 10

states = 1
struct_resolutions = [1,10]
add_missing = False
missing_resolution = 1

add_rbs_from_pdbs = True

connectivity_restraint_weight = 1.
max_conn_gap = None
conn_first_copy_only = True

rb_max_rot = 2
rb_max_trans = 3

add_symmetry_constraints = True
add_symmetry_restraints = False
symmetry_restraints_weight = 1

add_parsimonious_states_restraints = False
parsimonious_states_weight = 10
parsimonious_states_distance = 0.0

add_xlink_restraints = False

add_em_restr = True

discrete_restraints_weight=1000

ev_restraints = [
    {
        'name': 'ev_restraints_lowres',
        'weight': 10,
        'repr_resolution': 10
    },
    {
        'name': 'ev_restraints_highres',
        'weight': 10,
        'repr_resolution': 1
    }
]

scoring_functions = {
    'score_func_ini_opt': {
        'restraints': [
            # 'discrete_restraints',
            'conn_restraints',
            'ev_restraints_lowres',
            # 'parsimonious_states_restraints'
            # 'exclude_membrane'
            'membrane_cerevisiae_no_neg_relative_NR_v3.mrc',
            'Nup133_close_to_membrane',
            'Nup120_close_to_membrane',
            'FitRestraint_NR_merged_unerased_tail_relative_clean_v1.3'
        ]
    },
    'score_func_highres': {
        'restraints': [
            # 'discrete_restraints',
            #'xlink_restraints',
            'conn_restraints',
            'ev_restraints_highres'
            # 'parsimonious_states_restraints'
        ]
    },
    'score_func_lowres': {
        'restraints': [
            'conn_restraints',
            'ev_restraints_lowres',
            'membrane_cerevisiae_no_neg_relative_NR_v3.mrc',
            'Nup133_close_to_membrane',
            'Nup120_close_to_membrane',
            'FitRestraint_NR_merged_unerased_tail_relative_clean_v1.3'
        ]
    },
    'score_func_for_CG': {
        'restraints': [
            #'xlink_restraints',
            'conn_restraints',
            'ev_restraints_lowres'
            # 'parsimonious_states_restraints'
        ]
    },
    'score_func_precond': {
        'restraints': [
            # 'discrete_restraints',
            #'xlink_restraints',
            'conn_restraints',
            'ev_restraints_lowres'
            # 'parsimonious_states_restraints'
        ]
    }
}

score_func = 'score_func_lowres'
score_func_for_CG = 'score_func_for_CG'
score_func_ini_opt = 'score_func_ini_opt'
score_func_preconditioned_mc = 'score_func_precond'

# SLURM template, adjust to your cluster environment
ntasks = 1
cluster_submission_command = 'sbatch'
run_script_templ = Template("""#!/bin/bash
#
#SBATCH --ntasks=$ntasks
#SBATCH --mem-per-cpu=2000
#SBATCH --job-name=${prefix}fit
#SBATCH --time=10:00:00
#SBATCH -e $outdir/logs/${prefix}_err.txt
#SBATCH -o $outdir/logs/${prefix}_out.txt

echo "Running on:"
srun hostname

$cmd

wait
""")