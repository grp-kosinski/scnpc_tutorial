score 32417.60390679827
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1821.4839553906504,-1821.4839553906504,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-2342.1662202300213,-2342.1662202300213,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-563.3260728117407,-563.3260728117407,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-674.6987696311741,-674.6987696311741,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2012.7394536527413,2012.7394536527413,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",2854.5668738229288,2854.5668738229288,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",10915.148635743519,10915.148635743519,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",6214.831711740859,6214.831711740859,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",0.0,0.0,1.0
"ev_restraint_resol_10",16015.271697726348,16015.271697726348,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1095.991963654562,1095.991963654562,1000.0
