score 29306.97300980232
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-606.3607289905439,-606.3607289905439,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-623.771967152006,-623.771967152006,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2600.8783991242253,2600.8783991242253,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3565.359397056857,3565.359397056857,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",7535.148825573674,7535.148825573674,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1929.5921135176332,1929.5921135176332,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",10935.907498416862,10935.907498416862,1.0
"ev_restraint_resol_10",6391.845317389694,6391.845317389694,10.0
"membrane_cerevisiae_no_neg_relative",79.50271656310636,79.50271656310636,1000.0
"P-complex_neg_stain_map_fit1",591.1223280663584,591.1223280663584,1000.0
