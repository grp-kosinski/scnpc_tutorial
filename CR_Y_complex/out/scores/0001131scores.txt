score 30527.840133869686
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-5186.583950727636,-5186.583950727636,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-648.567952318089,-648.567952318089,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",22092.497201109505,22092.497201109505,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1122.2417527596865,1122.2417527596865,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1644.5761940004832,1644.5761940004832,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0,0.0,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",1170.7313889464767,1170.7313889464767,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",9087.188619410028,9087.188619410028,1.0
"ev_restraint_resol_10",2076.5098413603478,2076.5098413603478,10.0
"membrane_cerevisiae_no_neg_relative",1885.77072708695,1885.77072708695,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
