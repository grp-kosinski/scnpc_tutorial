score 132217.95309036932
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-793.2318895139816,-793.2318895139816,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-574.9925432012986,-574.9925432012986,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",122155.08132255799,122155.08132255799,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",430.97752793658515,430.97752793658515,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",523.0592227439606,523.0592227439606,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",574.9352525588394,574.9352525588394,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",195.5540296132828,195.5540296132828,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",6399.367006624007,6399.367006624007,1.0
"ev_restraint_resol_10",6367.417624538657,6367.417624538657,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",553.3171115617606,553.3171115617606,1000.0
