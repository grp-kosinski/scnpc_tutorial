score 36832.44314068533
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2874.2731125791097,-2874.2731125791097,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-781.6407002810954,-781.6407002810954,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",10926.686187772848,10926.686187772848,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",6664.324609207643,6664.324609207643,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1588.9659752571147,1588.9659752571147,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0,0.0,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",239.3561343536258,239.3561343536258,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",9071.653659568075,9071.653659568075,1.0
"ev_restraint_resol_10",13138.54254630186,13138.54254630186,10.0
"membrane_cerevisiae_no_neg_relative",1645.9379611711356,1645.9379611711356,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
