score 72279.93442613346
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3320.521278624884,-3320.521278624884,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-801.7255596679113,-801.7255596679113,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-827.1565497960279,-827.1565497960279,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",1384.1439280153954,1384.1439280153954,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",586.0100217343361,586.0100217343361,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",26544.436187008414,26544.436187008414,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1112.7436668533942,1112.7436668533942,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",1685.6676980110087,1685.6676980110087,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",24498.080679326336,24498.080679326336,1.0
"ev_restraint_resol_10",23498.415323347115,23498.415323347115,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
