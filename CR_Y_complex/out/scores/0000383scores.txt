score 41488.677154439494
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-1133.3407072638888,-1133.3407072638888,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-661.3813001858331,-661.3813001858331,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-681.2571936227569,-681.2571936227569,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",13237.337889001643,13237.337889001643,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",678.594539525455,678.594539525455,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1394.7374202854191,1394.7374202854191,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",2060.8464285257905,2060.8464285257905,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",7428.121793140498,7428.121793140498,1.0
"ev_restraint_resol_10",20969.12004554308,20969.12004554308,10.0
"membrane_cerevisiae_no_neg_relative",1020.3653992131385,1020.3653992131385,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
