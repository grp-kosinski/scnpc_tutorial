score 36333.36167933596
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3762.050375481624,-3762.050375481624,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",22479.216678197463,22479.216678197463,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1866.5810533835934,1866.5810533835934,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2691.3544874018335,2691.3544874018335,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",661.7466289009848,661.7466289009848,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",5278.60431108747,5278.60431108747,1.0
"ev_restraint_resol_10",5065.019665825313,5065.019665825313,10.0
"membrane_cerevisiae_no_neg_relative",4909.1143660036305,4909.1143660036305,1000.0
"P-complex_neg_stain_map_fit1",526.0968675800398,526.0968675800398,1000.0
