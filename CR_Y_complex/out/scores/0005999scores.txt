score 50503.53969298215
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2203.168020309487,-2203.168020309487,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",17186.93911400915,17186.93911400915,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4481.67413047047,4481.67413047047,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",15809.618523525281,15809.618523525281,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0,0.0,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",1331.0989951645201,1331.0989951645201,1.0
"ev_restraint_resol_10",16714.566958012205,16714.566958012205,10.0
"membrane_cerevisiae_no_neg_relative",565.1319956727538,565.1319956727538,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
