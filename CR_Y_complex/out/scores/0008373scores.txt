score 37427.52261118512
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1015.281895278654,-1015.281895278654,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-712.9312352980479,-712.9312352980479,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-594.400204709195,-594.400204709195,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",27561.805182396143,27561.805182396143,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",2093.044169237436,2093.044169237436,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1026.15449213733,1026.15449213733,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",2320.5398294553065,2320.5398294553065,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",508.70264484760577,508.70264484760577,1.0
"ev_restraint_resol_10",6274.46369604173,6274.46369604173,10.0
"membrane_cerevisiae_no_neg_relative",120.47089055213014,120.47089055213014,1000.0
"P-complex_neg_stain_map_fit1",1966.2668460854259,1966.2668460854259,1000.0
