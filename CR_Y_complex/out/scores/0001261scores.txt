score 388537.43824582436
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1332.955772168515,-1332.955772168515,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-801.7255596679113,-801.7255596679113,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-706.8625909351038,-706.8625909351038,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",15935.079955982346,15935.079955982346,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3041.2056147601857,3041.2056147601857,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3602.8762464476704,3602.8762464476704,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",116.88372232254241,116.88372232254241,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",165695.64226734836,165695.64226734836,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",189334.8432597372,189334.8432597372,1.0
"ev_restraint_resol_10",14467.326259852405,14467.326259852405,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1272.0336062196554,1272.0336062196554,1000.0
