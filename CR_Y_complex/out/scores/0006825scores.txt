score 49595.39161874397
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1526.6777188843846,-1526.6777188843846,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-643.7571344012123,-643.7571344012123,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2727.8393324122676,2727.8393324122676,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",52.906897966057016,52.906897966057016,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",11762.77904624102,11762.77904624102,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",6391.846151526246,6391.846151526246,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",7232.132671481373,7232.132671481373,1.0
"ev_restraint_resol_10",25090.79362192389,25090.79362192389,10.0
"membrane_cerevisiae_no_neg_relative",1267.2203101363837,1267.2203101363837,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
