score 50409.23201932888
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1465.6168557800052,-1465.6168557800052,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2388.851453032202,2388.851453032202,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",81.13670005650457,81.13670005650457,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",4213.235843432727,4213.235843432727,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",84.15736321859221,84.15736321859221,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",2486.1848456369958,2486.1848456369958,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",41693.24316103088,41693.24316103088,1.0
"ev_restraint_resol_10",4418.372923002486,4418.372923002486,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
