score 42591.138478209126
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3762.050375481624,-3762.050375481624,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-940.1686042230821,-940.1686042230821,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-599.548639407922,-599.548639407922,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",18899.267718763942,18899.267718763942,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",5102.41447531063,5102.41447531063,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",26.571011587207046,26.571011587207046,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3662.468455268276,3662.468455268276,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",8141.42838672101,8141.42838672101,1.0
"ev_restraint_resol_10",11639.91068067124,11639.91068067124,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2492.81425933173,2492.81425933173,1000.0
