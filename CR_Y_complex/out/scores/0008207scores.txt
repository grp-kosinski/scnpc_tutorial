score 25667.341412789985
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1575.0001449234937,-1575.0001449234937,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-931.568694308468,-931.568694308468,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",7859.251257033155,7859.251257033155,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",297.54143188320086,297.54143188320086,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",0.0,0.0,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",413.8089602021903,413.8089602021903,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",7.344764045302576,7.344764045302576,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",11339.599246965145,11339.599246965145,1.0
"ev_restraint_resol_10",10626.441634643135,10626.441634643135,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",312.66114810453263,312.66114810453263,1000.0
