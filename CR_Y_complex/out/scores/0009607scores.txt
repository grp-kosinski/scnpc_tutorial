score 42253.82480692867
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2206.883991420493,-2206.883991420493,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-739.8068200983575,-739.8068200983575,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",21219.152642762514,21219.152642762514,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",449.5862063032499,449.5862063032499,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",7418.218817531997,7418.218817531997,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1747.9743531090326,1747.9743531090326,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",5829.35809426448,5829.35809426448,1.0
"ev_restraint_resol_10",8791.687385134082,8791.687385134082,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2418.085829624683,2418.085829624683,1000.0
