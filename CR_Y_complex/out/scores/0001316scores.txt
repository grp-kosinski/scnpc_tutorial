score 46911.777238476985
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-8594.596498770567,-8594.596498770567,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-602.1743694187013,-602.1743694187013,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-674.6987696311741,-674.6987696311741,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",19934.088188751844,19934.088188751844,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4265.680624660667,4265.680624660667,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1371.0799355710615,1371.0799355710615,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1512.6390127081618,1512.6390127081618,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",17267.56228866857,17267.56228866857,1.0
"ev_restraint_resol_10",13478.429672529453,13478.429672529453,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1040.6759174820643,1040.6759174820643,1000.0
