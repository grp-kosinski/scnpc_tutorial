score 110361.23806116881
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2803.270531498489,-2803.270531498489,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-621.0587298029255,-621.0587298029255,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-574.9925432012986,-574.9925432012986,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",10340.494691539465,10340.494691539465,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4037.6396614817977,4037.6396614817977,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",43.83279874779354,43.83279874779354,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",2040.7601535097788,2040.7601535097788,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",15887.497870862484,15887.497870862484,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",51538.60409326979,51538.60409326979,1.0
"ev_restraint_resol_10",31033.840597365444,31033.840597365444,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1557.3779399098912,1557.3779399098912,1000.0
