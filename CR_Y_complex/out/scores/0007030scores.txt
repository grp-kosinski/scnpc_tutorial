score 17888.934197564977
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-2342.1662202300213,-2342.1662202300213,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-748.8593716770544,-748.8593716770544,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",830.5375977893257,830.5375977893257,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",2809.596369381374,2809.596369381374,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",88.91724408439654,88.91724408439654,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",4406.492215260834,4406.492215260834,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",1809.9522071067033,1809.9522071067033,1.0
"ev_restraint_resol_10",12602.77627006519,12602.77627006519,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1407.6571558036057,1407.6571558036057,1000.0
