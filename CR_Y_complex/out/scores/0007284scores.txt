score 35338.337144609286
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2394.636131076744,-2394.636131076744,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-940.1686042230821,-940.1686042230821,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-638.8233971861972,-638.8233971861972,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",529.8985325051179,529.8985325051179,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",415.52236547698294,415.52236547698294,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3968.4793983279606,3968.4793983279606,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",127.32138850505932,127.32138850505932,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",11453.611773158733,11453.611773158733,1.0
"ev_restraint_resol_10",24080.348336963114,24080.348336963114,10.0
"membrane_cerevisiae_no_neg_relative",79.50271656310636,79.50271656310636,1000.0
"P-complex_neg_stain_map_fit1",744.8070729739785,744.8070729739785,1000.0
