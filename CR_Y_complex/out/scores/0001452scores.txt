score 24407.04185669117
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-4130.9757257890205,-4130.9757257890205,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-599.548639407922,-599.548639407922,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-736.791730048198,-736.791730048198,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2803.638323200985,2803.638323200985,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1309.907041596175,1309.907041596175,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",332.5244187352671,332.5244187352671,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",430.7294535688794,430.7294535688794,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",11292.14250028909,11292.14250028909,1.0
"ev_restraint_resol_10",13200.13616499903,13200.13616499903,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2624.5992864139203,2624.5992864139203,1000.0
