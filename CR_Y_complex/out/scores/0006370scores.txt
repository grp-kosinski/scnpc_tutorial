score 29537.1505622548
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3762.050375481624,-3762.050375481624,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-495.2853480903129,-495.2853480903129,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-712.915629203358,-712.915629203358,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",25434.758066302064,25434.758066302064,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",614.8651466966429,614.8651466966429,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",983.2629600009828,983.2629600009828,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",100.61234526459033,100.61234526459033,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",1189.541311450449,1189.541311450449,1.0
"ev_restraint_resol_10",6110.533742259167,6110.533742259167,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2191.3237166560557,2191.3237166560557,1000.0
