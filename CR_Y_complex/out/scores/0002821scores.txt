score 40603.78927283709
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-827.1565497960279,-827.1565497960279,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",10437.586286032187,10437.586286032187,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",0.707415856461432,0.707415856461432,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",242.8114396368111,242.8114396368111,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1271.404493117515,1271.404493117515,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",5577.833471615603,5577.833471615603,1.0
"ev_restraint_resol_10",27683.84925399922,27683.84925399922,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
