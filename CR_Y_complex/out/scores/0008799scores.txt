score 43961.08275845458
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2203.168020309487,-2203.168020309487,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-673.8487789073953,-673.8487789073953,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",18965.77229348159,18965.77229348159,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",140.13955517846577,140.13955517846577,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",16453.312494740265,16453.312494740265,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",750.736956362981,750.736956362981,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",3401.351880975431,3401.351880975431,1.0
"ev_restraint_resol_10",9474.426152446315,9474.426152446315,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",451.2113309374042,451.2113309374042,1000.0
