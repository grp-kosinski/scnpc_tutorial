score 156682.8294758667
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2803.270531498489,-2803.270531498489,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-1007.863631742324,-1007.863631742324,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",138009.6856699992,138009.6856699992,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",615.8023353451769,615.8023353451769,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",13638.57079277321,13638.57079277321,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",97.46275850103201,97.46275850103201,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",43.01643953430644,43.01643953430644,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",1447.4575793063195,1447.4575793063195,1.0
"ev_restraint_resol_10",7790.046602077184,7790.046602077184,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1648.9487047548648,1648.9487047548648,1000.0
