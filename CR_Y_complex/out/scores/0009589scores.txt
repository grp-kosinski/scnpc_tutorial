score 27476.15197155372
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1015.281895278654,-1015.281895278654,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-940.1686042230821,-940.1686042230821,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-602.1743694187013,-602.1743694187013,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",6131.080086931639,6131.080086931639,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",2673.106666111867,2673.106666111867,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",686.1978014695769,686.1978014695769,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",97.28799523260102,97.28799523260102,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",10.011973135404446,10.011973135404446,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",3985.9683573431485,3985.9683573431485,1.0
"ev_restraint_resol_10",15668.507394533653,15668.507394533653,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2869.1428730950115,2869.1428730950115,1000.0
