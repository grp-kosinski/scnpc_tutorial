score 23736.526349580192
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2206.883991420493,-2206.883991420493,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-713.1388555260743,-713.1388555260743,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-827.1565497960279,-827.1565497960279,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-674.6987696311741,-674.6987696311741,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",11008.017872361062,11008.017872361062,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1614.7086255716001,1614.7086255716001,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3406.0181001020765,3406.0181001020765,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1040.9455110638348,1040.9455110638348,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",734.9569086805063,734.9569086805063,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",3170.1996103692177,3170.1996103692177,1.0
"ev_restraint_resol_10",7315.795567869753,7315.795567869753,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1275.139214426344,1275.139214426344,1000.0
