score 41275.89940226906
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-623.771967152006,-623.771967152006,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",9111.424992037872,9111.424992037872,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",63.26288777281407,63.26288777281407,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",7535.148825573674,7535.148825573674,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",661.7466289009848,661.7466289009848,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",10935.907498416862,10935.907498416862,1.0
"ev_restraint_resol_10",16210.28328500419,16210.28328500419,10.0
"membrane_cerevisiae_no_neg_relative",79.50271656310636,79.50271656310636,1000.0
"P-complex_neg_stain_map_fit1",991.6811716582521,991.6811716582521,1000.0
