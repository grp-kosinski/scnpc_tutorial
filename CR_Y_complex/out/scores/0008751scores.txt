score 12436.206297357447
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2394.636131076744,-2394.636131076744,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-574.9925432012986,-574.9925432012986,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",5226.131508579782,5226.131508579782,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",602.9501334647678,602.9501334647678,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",523.0592227439606,523.0592227439606,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",621.0478986653245,621.0478986653245,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",1908.4770230345448,1908.4770230345448,1.0
"ev_restraint_resol_10",6319.84455305192,6319.84455305192,10.0
"membrane_cerevisiae_no_neg_relative",2380.7721418492933,2380.7721418492933,1000.0
"P-complex_neg_stain_map_fit1",497.10020052841287,497.10020052841287,1000.0
