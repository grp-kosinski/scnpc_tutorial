score 41420.04623855396
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2206.883991420493,-2206.883991420493,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-786.1256517801003,-786.1256517801003,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",11008.017872361062,11008.017872361062,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",6761.705757312948,6761.705757312948,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",556.6849913255193,556.6849913255193,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1040.9455110638348,1040.9455110638348,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",555.5794535850162,555.5794535850162,1.0
"ev_restraint_resol_10",26346.415597013274,26346.415597013274,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",971.9689333880389,971.9689333880389,1000.0
