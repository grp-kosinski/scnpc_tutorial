score 29758.002315959944
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",10143.753720795743,10143.753720795743,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3.9863594850454613,3.9863594850454613,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",51.42214361365498,51.42214361365498,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",2914.7673160600843,2914.7673160600843,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",12575.013695073805,12575.013695073805,1.0
"ev_restraint_resol_10",7922.131646851232,7922.131646851232,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",613.8402278127234,613.8402278127234,1000.0
