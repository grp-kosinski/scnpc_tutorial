score 17311.73271975515
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1015.281895278654,-1015.281895278654,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-793.1524854108916,-793.1524854108916,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-651.8112100855857,-651.8112100855857,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2970.596115112881,2970.596115112881,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",805.8015262102994,805.8015262102994,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3712.5594575701743,3712.5594575701743,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",87.49256797331361,87.49256797331361,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",2502.111257276745,2502.111257276745,1.0
"ev_restraint_resol_10",9807.674712214954,9807.674712214954,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1987.043283084379,1987.043283084379,1000.0
