score 52060.1948672266
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1821.4839553906504,-1821.4839553906504,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-600.8608032046055,-600.8608032046055,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-651.8112100855857,-651.8112100855857,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",28910.982751853368,28910.982751853368,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4253.50137276543,4253.50137276543,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3712.5594575701743,3712.5594575701743,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1772.6691900812348,1772.6691900812348,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",16142.661255973086,16142.661255973086,1.0
"ev_restraint_resol_10",1291.7985768260016,1291.7985768260016,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1169.6661718530593,1169.6661718530593,1000.0
