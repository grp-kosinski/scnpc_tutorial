score 29167.916977998048
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1152.5369856628247,-1152.5369856628247,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-712.8253514767514,-712.8253514767514,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-739.8068200983575,-739.8068200983575,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2773.5549474524582,2773.5549474524582,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",398.5419050019258,398.5419050019258,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",376.51927890522006,376.51927890522006,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",41.40385800847562,41.40385800847562,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",1475.139767213505,1475.139767213505,1.0
"ev_restraint_resol_10",28825.421752254257,28825.421752254257,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
