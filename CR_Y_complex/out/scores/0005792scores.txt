score 223532.7716105004
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3930.792785401962,-3930.792785401962,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-741.4888919798148,-741.4888919798148,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-316.07785014029434,-316.07785014029434,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.8210894978491,-711.8210894978491,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",19915.92314670645,19915.92314670645,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",10425.97995878921,10425.97995878921,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1618.8501968419348,1618.8501968419348,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3998.6928320580823,3998.6928320580823,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",38126.09003455803,38126.09003455803,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",150476.2476127177,150476.2476127177,1.0
"ev_restraint_resol_10",5697.45485868843,5697.45485868843,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",379.2666183837359,379.2666183837359,1000.0
