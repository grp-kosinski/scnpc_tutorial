score 43845.28466728646
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-653.2832369632208,-653.2832369632208,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",17398.932471614913,17398.932471614913,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1151.3795543744202,1151.3795543744202,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",8037.810483118742,8037.810483118742,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1772.6691900812348,1772.6691900812348,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",10645.428853203228,10645.428853203228,1.0
"ev_restraint_resol_10",3476.7404047092596,3476.7404047092596,10.0
"membrane_cerevisiae_no_neg_relative",5662.217032156219,5662.217032156219,1000.0
"P-complex_neg_stain_map_fit1",562.1333735181355,562.1333735181355,1000.0
