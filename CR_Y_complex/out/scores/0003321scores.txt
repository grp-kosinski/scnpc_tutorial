score 25728.157032615065
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-801.7255596679113,-801.7255596679113,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-574.9925432012986,-574.9925432012986,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",3929.654177053417,3929.654177053417,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",8829.6495783556,8829.6495783556,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",610.3847634222166,610.3847634222166,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1542.5969544831307,1542.5969544831307,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",2107.8918887532627,2107.8918887532627,1.0
"ev_restraint_resol_10",13621.908865997953,13621.908865997953,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",117.4725966775599,117.4725966775599,1000.0
