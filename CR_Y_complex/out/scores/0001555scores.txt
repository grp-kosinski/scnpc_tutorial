score 42173.216593617195
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2874.2731125791097,-2874.2731125791097,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-643.7571344012123,-643.7571344012123,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-574.9925432012986,-574.9925432012986,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",4340.528976573797,4340.528976573797,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3254.2501274363444,3254.2501274363444,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",4364.610022798705,4364.610022798705,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1437.6381153331374,1437.6381153331374,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",2459.74937903491,2459.74937903491,1.0
"ev_restraint_resol_10",32705.896246063912,32705.896246063912,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
