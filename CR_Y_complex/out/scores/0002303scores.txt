score 30923.988156901312
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-602.1743694187013,-602.1743694187013,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-658.6060462979481,-658.6060462979481,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",7806.971651216005,7806.971651216005,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",931.9585886041035,931.9585886041035,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",4375.072341956037,4375.072341956037,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0,0.0,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",638.0014356587678,638.0014356587678,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",9303.637127797718,9303.637127797718,1.0
"ev_restraint_resol_10",11350.589319979716,11350.589319979716,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1435.0456599316565,1435.0456599316565,1000.0
