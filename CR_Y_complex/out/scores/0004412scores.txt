score 23028.137953058886
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-732.1934521787192,-732.1934521787192,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-648.567952318089,-648.567952318089,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2600.8783991242253,2600.8783991242253,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",350.34700765143333,350.34700765143333,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2992.6023816964016,2992.6023816964016,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1991.886831987256,1991.886831987256,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",3258.5732001192273,3258.5732001192273,1.0
"ev_restraint_resol_10",15561.33365293631,15561.33365293631,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",745.5287738043758,745.5287738043758,1000.0
