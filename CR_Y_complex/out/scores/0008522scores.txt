score 22689.866236789167
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1152.5369856628247,-1152.5369856628247,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-608.0703845899029,-608.0703845899029,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-827.1565497960279,-827.1565497960279,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",16899.137913742525,16899.137913742525,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",71.02972416718788,71.02972416718788,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",252.0358325212764,252.0358325212764,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",172.15247458094342,172.15247458094342,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",54.17698238830799,54.17698238830799,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",5238.5250783024285,5238.5250783024285,1.0
"ev_restraint_resol_10",3153.6986947571545,3153.6986947571545,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1515.2092831846414,1515.2092831846414,1000.0
