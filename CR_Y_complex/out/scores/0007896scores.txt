score 12832.750506907167
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1821.4839553906504,-1821.4839553906504,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-608.0703845899029,-608.0703845899029,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2580.1912307420416,2580.1912307420416,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",573.1709222507329,573.1709222507329,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2073.3460158633884,2073.3460158633884,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0,0.0,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",2690.3848741757333,2690.3848741757333,1.0
"ev_restraint_resol_10",6341.8744477509645,6341.8744477509645,10.0
"membrane_cerevisiae_no_neg_relative",2748.5745997210734,2748.5745997210734,1000.0
"P-complex_neg_stain_map_fit1",1081.201127411753,1081.201127411753,1000.0
