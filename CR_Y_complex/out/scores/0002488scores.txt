score 30872.026497647563
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1015.281895278654,-1015.281895278654,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-606.3607289905439,-606.3607289905439,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.9423423766015,-711.9423423766015,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",18736.796277462723,18736.796277462723,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",162.28403984815589,162.28403984815589,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",861.8836994415075,861.8836994415075,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",4889.757045040188,4889.757045040188,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",89.29364916903026,89.29364916903026,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",3166.6260147297703,3166.6260147297703,1.0
"ev_restraint_resol_10",5098.098543999157,5098.098543999157,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2285.95709541005,2285.95709541005,1000.0
