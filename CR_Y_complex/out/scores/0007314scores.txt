score 38432.589965666004
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-5186.583950727636,-5186.583950727636,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-1925.2963707523666,-1925.2963707523666,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-598.5520362566216,-598.5520362566216,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",3613.254516537472,3613.254516537472,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1655.4447166394384,1655.4447166394384,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2584.0049098162845,2584.0049098162845,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3275.7482343105194,3275.7482343105194,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",4712.991623398186,4712.991623398186,1.0
"ev_restraint_resol_10",30111.257508257877,30111.257508257877,10.0
"membrane_cerevisiae_no_neg_relative",2248.645445879768,2248.645445879768,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
