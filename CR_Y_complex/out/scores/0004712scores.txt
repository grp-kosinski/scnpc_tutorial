score 23235.244618470486
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-8594.596498770567,-8594.596498770567,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-1925.2963707523666,-1925.2963707523666,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-322.6816127889249,-322.6816127889249,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2172.427739558594,2172.427739558594,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3448.530844374982,3448.530844374982,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1318.4689739186542,1318.4689739186542,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",11.000871745117262,11.000871745117262,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",4529.739881859648,4529.739881859648,1.0
"ev_restraint_resol_10",24052.303558612635,24052.303558612635,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",548.5535519834697,548.5535519834697,1000.0
