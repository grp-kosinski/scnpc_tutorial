score 41921.341942972285
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1593.1967380167694,-1593.1967380167694,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-468.87413520747464,-468.87413520747464,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2745.154206152014,2745.154206152014,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",2196.278541724805,2196.278541724805,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",6871.304851192991,6871.304851192991,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",11.89582587416351,11.89582587416351,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",8696.392157540715,8696.392157540715,1.0
"ev_restraint_resol_10",24721.035675807692,24721.035675807692,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1632.996924822107,1632.996924822107,1000.0
