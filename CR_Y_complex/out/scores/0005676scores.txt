score 47672.1014887894
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1113.6590301450872,-1113.6590301450872,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-673.8487789073953,-673.8487789073953,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-842.3721313679321,-842.3721313679321,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",20604.7979275557,20604.7979275557,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",610.6738089813243,610.6738089813243,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1143.6756264888058,1143.6756264888058,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",35.08384148944886,35.08384148944886,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",23898.487850755977,23898.487850755977,1.0
"ev_restraint_resol_10",4431.286171944246,4431.286171944246,10.0
"membrane_cerevisiae_no_neg_relative",1647.3295756155107,1647.3295756155107,1000.0
"P-complex_neg_stain_map_fit1",50.13456739371638,50.13456739371638,1000.0
