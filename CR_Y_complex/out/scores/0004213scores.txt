score 39499.625407026266
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2198.528502720699,-2198.528502720699,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-801.7255596679113,-801.7255596679113,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",15793.02973131731,15793.02973131731,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",256.3809327207342,256.3809327207342,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",136.61409470294518,136.61409470294518,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.004507320149703964,0.004507320149703964,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",615.1850680703276,615.1850680703276,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",16702.111284587867,16702.111284587867,1.0
"ev_restraint_resol_10",10659.876134470625,10659.876134470625,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1123.7878363116893,1123.7878363116893,1000.0
