score 37910.02909197931
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1015.281895278654,-1015.281895278654,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-940.1686042230821,-940.1686042230821,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-1019.0019871001692,-1019.0019871001692,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-709.8238128628991,-709.8238128628991,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",18135.58923896055,18135.58923896055,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3910.1351720444063,3910.1351720444063,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",4171.401730417197,4171.401730417197,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",14.7386036087224,14.7386036087224,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",14614.98328847283,14614.98328847283,1.0
"ev_restraint_resol_10",670.197602431627,670.197602431627,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1442.278215828025,1442.278215828025,1000.0
