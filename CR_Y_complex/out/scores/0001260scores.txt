score 42041.13896858021
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1575.0001449234937,-1575.0001449234937,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-1925.2963707523666,-1925.2963707523666,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-574.9925432012986,-574.9925432012986,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",10294.003539204572,10294.003539204572,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",932.6343083830853,932.6343083830853,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",236.68870718252447,236.68870718252447,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",271.4555530895587,271.4555530895587,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",3784.5103225936305,3784.5103225936305,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",20029.42792326859,20029.42792326859,1.0
"ev_restraint_resol_10",11710.043315647414,11710.043315647414,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",958.9649670004585,958.9649670004585,1000.0
