score 9100.765362081704
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1575.0001449234937,-1575.0001449234937,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-712.9312352980479,-712.9312352980479,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",576.3804995855626,576.3804995855626,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3140.895338054201,3140.895338054201,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",712.2028405459566,712.2028405459566,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1102.269904292867,1102.269904292867,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",2.3125193686652517,2.3125193686652517,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",7131.2130942733365,7131.2130942733365,1.0
"ev_restraint_resol_10",962.6744180856984,962.6744180856984,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",546.0343849165507,546.0343849165507,1000.0
