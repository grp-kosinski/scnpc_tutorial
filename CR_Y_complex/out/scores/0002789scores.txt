score 29518.688502892706
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-6620.868037184918,-6620.868037184918,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-769.9998003813348,-769.9998003813348,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-674.6987696311741,-674.6987696311741,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",12990.76069093209,12990.76069093209,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",5669.034194037366,5669.034194037366,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",84.02325223631182,84.02325223631182,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",661.7466289009848,661.7466289009848,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",4958.937755296707,4958.937755296707,1.0
"ev_restraint_resol_10",14648.977612483903,14648.977612483903,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",685.1023007062525,685.1023007062525,1000.0
