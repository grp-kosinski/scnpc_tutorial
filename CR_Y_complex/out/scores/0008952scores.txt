score 79394.13150982093
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1465.6168557800052,-1465.6168557800052,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-459.6403327507044,-459.6403327507044,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",4563.874720179964,4563.874720179964,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",459.72799784515905,459.72799784515905,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",6021.244466707405,6021.244466707405,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",10323.571213894282,10323.571213894282,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",1015.7040068578738,1015.7040068578738,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",27955.813954785754,27955.813954785754,1.0
"ev_restraint_resol_10",33750.776380163945,33750.776380163945,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
