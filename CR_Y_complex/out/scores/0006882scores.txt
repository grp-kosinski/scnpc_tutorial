score 30841.72996539067
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1015.281895278654,-1015.281895278654,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-801.7255596679113,-801.7255596679113,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-679.617340364991,-679.617340364991,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",18639.726138035272,18639.726138035272,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4632.479786895092,4632.479786895092,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",418.6942748284884,418.6942748284884,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",69.48545508057651,69.48545508057651,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",9192.262595883147,9192.262595883147,1.0
"ev_restraint_resol_10",111.58950371415509,111.58950371415509,10.0
"membrane_cerevisiae_no_neg_relative",1079.49245885047,1079.49245885047,1000.0
"P-complex_neg_stain_map_fit1",1279.7094482222412,1279.7094482222412,1000.0
