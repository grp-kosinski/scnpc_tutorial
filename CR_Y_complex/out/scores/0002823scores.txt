score 29512.83818099599
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3930.792785401962,-3930.792785401962,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-691.4507798272908,-691.4507798272908,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-2126.6213806493947,-2126.6213806493947,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",4439.237178246784,4439.237178246784,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4058.87275773634,4058.87275773634,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1089.1467449931401,1089.1467449931401,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3614.6060959152214,3614.6060959152214,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",121.95130367403713,121.95130367403713,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",23266.50673430849,23266.50673430849,1.0
"ev_restraint_resol_10",1732.1339546767622,1732.1339546767622,10.0
"membrane_cerevisiae_no_neg_relative",58.73629833877995,58.73629833877995,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
