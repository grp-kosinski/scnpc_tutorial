score 35617.00951893372
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1997.2677634904285,-1997.2677634904285,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-602.1743694187013,-602.1743694187013,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2486.5042643949296,2486.5042643949296,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4265.680624660667,4265.680624660667,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",7580.693827331268,7580.693827331268,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",4734.998229376705,4734.998229376705,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",4220.400863110052,4220.400863110052,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",15376.017006746768,15376.017006746768,1.0
"ev_restraint_resol_10",680.9594921037765,680.9594921037765,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1845.3388338774637,1845.3388338774637,1000.0
