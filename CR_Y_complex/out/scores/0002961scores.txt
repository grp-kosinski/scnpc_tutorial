score 32398.285538752247
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2822.5002106520146,-2822.5002106520146,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",3228.9014164659443,3228.9014164659443,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",2763.2661357773027,2763.2661357773027,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3980.6829755648073,3980.6829755648073,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",2696.9298466590812,2696.9298466590812,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",17636.190949220738,17636.190949220738,1.0
"ev_restraint_resol_10",6469.7678133807185,6469.7678133807185,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1925.462903540119,1925.462903540119,1000.0
