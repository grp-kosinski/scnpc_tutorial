score 54130.09050142596
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2394.636131076744,-2394.636131076744,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-793.1524854108916,-793.1524854108916,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-608.0703845899029,-608.0703845899029,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",1154.1570100544238,1154.1570100544238,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",181.745561160477,181.745561160477,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",9495.493420461919,9495.493420461919,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",16533.957899754118,16533.957899754118,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",3013.0953915744926,3013.0953915744926,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",12788.946433843865,12788.946433843865,1.0
"ev_restraint_resol_10",14655.008772686526,14655.008772686526,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2175.5139032999673,2175.5139032999673,1000.0
