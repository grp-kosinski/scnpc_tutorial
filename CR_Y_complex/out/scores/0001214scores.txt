score 74039.61760999532
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-598.5520362566216,-598.5520362566216,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",34654.8855946793,34654.8855946793,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",10035.033926646203,10035.033926646203,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2506.080797562718,2506.080797562718,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",788.5214297745242,788.5214297745242,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",606.1721924864498,606.1721924864498,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",17379.788565177845,17379.788565177845,1.0
"ev_restraint_resol_10",12413.59799402342,12413.59799402342,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
