score 14844.96649070543
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1575.0001449234937,-1575.0001449234937,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-931.568694308468,-931.568694308468,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-1007.863631742324,-1007.863631742324,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-842.3721313679321,-842.3721313679321,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",7859.251257033155,7859.251257033155,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",121.83821143337396,121.83821143337396,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2127.0172081705555,2127.0172081705555,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",413.8089602021903,413.8089602021903,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",1584.8771198157847,1584.8771198157847,1.0
"ev_restraint_resol_10",8329.283266476466,8329.283266476466,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",173.68950771090772,173.68950771090772,1000.0
