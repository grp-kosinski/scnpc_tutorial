score 228676.7777540676
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-6620.868037184918,-6620.868037184918,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-940.1686042230821,-940.1686042230821,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",19305.575685757525,19305.575685757525,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",40.317616593383995,40.317616593383995,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",21590.983812420454,21590.983812420454,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0030285419913953864,0.0030285419913953864,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",51392.08751466582,51392.08751466582,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",139185.63424050246,139185.63424050246,1.0
"ev_restraint_resol_10",7200.818110246688,7200.818110246688,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",289.4933114643969,289.4933114643969,1000.0
