score 38435.3146118667
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1370.9882173352169,-1370.9882173352169,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-447.7804017578781,-447.7804017578781,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-711.3387926848428,-711.3387926848428,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",8869.240016577258,8869.240016577258,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",247.30679401131553,247.30679401131553,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1274.2058502241289,1274.2058502241289,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",88.90089852146916,88.90089852146916,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",18438.667412613297,18438.667412613297,1.0
"ev_restraint_resol_10",14132.185952504384,14132.185952504384,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
