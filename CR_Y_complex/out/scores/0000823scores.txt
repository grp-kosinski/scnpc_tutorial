score 6840.083225068487
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-664.1113081749828,-664.1113081749828,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-679.617340364991,-679.617340364991,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2351.744376816745,2351.744376816745,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",180.26746517374974,180.26746517374974,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2869.5853590174506,2869.5853590174506,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1368.1657199670226,1368.1657199670226,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",3282.0217885044963,3282.0217885044963,1.0
"ev_restraint_resol_10",545.409579726362,545.409579726362,10.0
"membrane_cerevisiae_no_neg_relative",898.4301843414639,898.4301843414639,1000.0
"P-complex_neg_stain_map_fit1",225.9719262715259,225.9719262715259,1000.0
