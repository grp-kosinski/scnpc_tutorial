score 69653.06843094071
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1534.7895181216475,-1534.7895181216475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-661.3813001858331,-661.3813001858331,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",37128.486835711185,37128.486835711185,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",5301.7919682617185,5301.7919682617185,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",6052.19736854047,6052.19736854047,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1437.6381153331374,1437.6381153331374,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",1206.551730058809,1206.551730058809,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",11991.158983054007,11991.158983054007,1.0
"ev_restraint_resol_10",9674.871514961294,9674.871514961294,10.0
"membrane_cerevisiae_no_neg_relative",1948.1881002455234,1948.1881002455234,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
