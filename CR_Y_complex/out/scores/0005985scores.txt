score 70877.23393855964
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-694.8464639588954,-694.8464639588954,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-539.363015157061,-539.363015157061,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",37678.12408261869,37678.12408261869,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4651.868853343059,4651.868853343059,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",11274.280540314649,11274.280540314649,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1437.6381153331374,1437.6381153331374,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",7896.761408712574,7896.761408712574,1.0
"ev_restraint_resol_10",11057.044697038693,11057.044697038693,10.0
"membrane_cerevisiae_no_neg_relative",1906.202837792357,1906.202837792357,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
