score 81987.38086421152
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-1470.5357855218037,-1470.5357855218037,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-674.815603586615,-674.815603586615,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",57958.45403680203,57958.45403680203,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",236.9740508273025,236.9740508273025,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",18271.25119189417,18271.25119189417,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0,0.0,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",5646.173951372562,5646.173951372562,1.0
"ev_restraint_resol_10",4650.585636269535,4650.585636269535,10.0
"membrane_cerevisiae_no_neg_relative",428.96509897734865,428.96509897734865,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
