score 25345.220979850492
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-648.567952318089,-648.567952318089,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",5744.137413712314,5744.137413712314,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",2150.487410588252,2150.487410588252,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",568.6470652305512,568.6470652305512,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",172.89077439200497,172.89077439200497,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",7696.563294313014,7696.563294313014,1.0
"ev_restraint_resol_10",9780.24983400335,9780.24983400335,10.0
"membrane_cerevisiae_no_neg_relative",4060.1884492861677,4060.1884492861677,1000.0
"P-complex_neg_stain_map_fit1",29.368149169389977,29.368149169389977,1000.0
