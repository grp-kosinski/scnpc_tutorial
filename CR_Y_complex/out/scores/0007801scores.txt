score 25768.903196575415
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-2342.1662202300213,-2342.1662202300213,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-602.1743694187013,-602.1743694187013,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-658.6060462979481,-658.6060462979481,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",830.5375977893257,830.5375977893257,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",931.9585886041035,931.9585886041035,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",4375.072341956037,4375.072341956037,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",4406.492215260834,4406.492215260834,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",18475.660504639665,18475.660504639665,1.0
"ev_restraint_resol_10",1080.0985129317767,1080.0985129317767,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1652.7874578837475,1652.7874578837475,1000.0
