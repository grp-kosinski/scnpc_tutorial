score 23283.123816204406
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3231.336924009913,-3231.336924009913,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-801.7255596679113,-801.7255596679113,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",9101.141580918895,9101.141580918895,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",5525.838965902866,5525.838965902866,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",6491.122749902202,6491.122749902202,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3998.6928320580823,3998.6928320580823,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",3475.8256363739793,3475.8256363739793,1.0
"ev_restraint_resol_10",605.7476674861608,605.7476674861608,10.0
"membrane_cerevisiae_no_neg_relative",496.41794524681075,496.41794524681075,1000.0
"P-complex_neg_stain_map_fit1",379.2666183837359,379.2666183837359,1000.0
