score 31832.68173028717
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-8594.596498770567,-8594.596498770567,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-693.3061711176803,-693.3061711176803,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-574.9925432012986,-574.9925432012986,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",9356.905666801762,9356.905666801762,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",4423.997318476134,4423.997318476134,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",523.0592227439606,523.0592227439606,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",152.13811865627764,152.13811865627764,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",7804.508541121051,7804.508541121051,1.0
"ev_restraint_resol_10",20578.24842686943,20578.24842686943,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",936.8793387818181,936.8793387818181,1000.0
