score 36654.78239310617
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1588.4195061915743,-1588.4195061915743,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-261.10807946603853,-261.10807946603853,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2846.943289927194,2846.943289927194,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1542.9435688739343,1542.9435688739343,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",653.2146480102091,653.2146480102091,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",11.89582587416351,11.89582587416351,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",8515.573681332102,8515.573681332102,1.0
"ev_restraint_resol_10",25749.178966700732,25749.178966700732,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2187.943911500486,2187.943911500486,1000.0
