score 47540.86809897782
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",8637.423869786462,8637.423869786462,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1027.9414694127768,1027.9414694127768,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1394.7936954705585,1394.7936954705585,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3614.6060959152214,3614.6060959152214,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",4922.534216724938,4922.534216724938,1.0
"ev_restraint_resol_10",28459.659756425892,28459.659756425892,10.0
"membrane_cerevisiae_no_neg_relative",4370.234734612093,4370.234734612093,1000.0
"P-complex_neg_stain_map_fit1",29.368149169389977,29.368149169389977,1000.0
