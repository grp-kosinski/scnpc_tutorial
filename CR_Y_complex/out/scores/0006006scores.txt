score 42601.619265037094
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2874.2731125791097,-2874.2731125791097,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-940.1686042230821,-940.1686042230821,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-638.8233971861972,-638.8233971861972,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2344.319018174984,2344.319018174984,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",415.52236547698294,415.52236547698294,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",11376.534828209022,11376.534828209022,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",738.7389396741053,738.7389396741053,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",10529.00608896279,10529.00608896279,1.0
"ev_restraint_resol_10",22973.564881172348,22973.564881172348,10.0
"membrane_cerevisiae_no_neg_relative",268.25868901603883,268.25868901603883,1000.0
"P-complex_neg_stain_map_fit1",455.3137615095816,455.3137615095816,1000.0
