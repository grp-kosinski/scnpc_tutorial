score 47347.68462167287
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1821.4839553906504,-1821.4839553906504,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",28910.982751853368,28910.982751853368,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",67.99026100884518,67.99026100884518,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",6435.800703945717,6435.800703945717,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1772.6691900812348,1772.6691900812348,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",4259.382035020454,4259.382035020454,1.0
"ev_restraint_resol_10",10294.43473529485,10294.43473529485,10.0
"membrane_cerevisiae_no_neg_relative",79.50271656310636,79.50271656310636,1000.0
"P-complex_neg_stain_map_fit1",742.637877370797,742.637877370797,1000.0
