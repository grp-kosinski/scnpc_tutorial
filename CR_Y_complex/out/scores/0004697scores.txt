score 36424.023113996
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2027.4745809615692,-2027.4745809615692,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",2117.572330977673,2117.572330977673,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1407.268885445681,1407.268885445681,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2140.2274517832175,2140.2274517832175,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",4734.998229376705,4734.998229376705,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",1773.8342207256637,1773.8342207256637,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",19475.594304806942,19475.594304806942,1.0
"ev_restraint_resol_10",9397.99135368287,9397.99135368287,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",932.2121771851915,932.2121771851915,1000.0
