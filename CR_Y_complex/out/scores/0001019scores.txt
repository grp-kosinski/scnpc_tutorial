score 61942.826740434626
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2874.2731125791097,-2874.2731125791097,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-648.567952318089,-648.567952318089,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",20416.768600512736,20416.768600512736,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",121.6642397389052,121.6642397389052,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",2825.9340910975543,2825.9340910975543,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",2473.4437801567833,2473.4437801567833,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",21214.457124906785,21214.457124906785,1.0
"ev_restraint_resol_10",21185.079803198056,21185.079803198056,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",217.7418644391893,217.7418644391893,1000.0
