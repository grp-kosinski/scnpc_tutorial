score 154151.12980903816
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2822.5002106520146,-2822.5002106520146,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-673.0594833763569,-673.0594833763569,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-2126.6213806493947,-2126.6213806493947,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",126612.28550044476,126612.28550044476,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",46.12857612955361,46.12857612955361,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",293.67922526278886,293.67922526278886,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",97.46275850103201,97.46275850103201,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",11748.67147143759,11748.67147143759,1.0
"ev_restraint_resol_10",23094.571292955115,23094.571292955115,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
