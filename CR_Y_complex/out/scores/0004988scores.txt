score 36605.01979062134
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-709.8238128628991,-709.8238128628991,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",3103.626150670454,3103.626150670454,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1300.8248692331015,1300.8248692331015,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",4512.412396344717,4512.412396344717,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1102.269904292867,1102.269904292867,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",1169.7919281398922,1169.7919281398922,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",23752.35313476096,23752.35313476096,1.0
"ev_restraint_resol_10",6175.202739660755,6175.202739660755,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",546.0343849165507,546.0343849165507,1000.0
