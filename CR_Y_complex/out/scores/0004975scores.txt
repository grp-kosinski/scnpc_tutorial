score 348030.67483478395
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1332.955772168515,-1332.955772168515,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-666.9499406277997,-666.9499406277997,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-316.07785014029434,-316.07785014029434,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",15935.079955982346,15935.079955982346,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",762.7569845560722,762.7569845560722,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1374.8927337645134,1374.8927337645134,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",116.88372232254241,116.88372232254241,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",77563.7177602819,77563.7177602819,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",230372.53572445814,230372.53572445814,1.0
"ev_restraint_resol_10",25070.06971441748,25070.06971441748,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1272.0336062196554,1272.0336062196554,1000.0
