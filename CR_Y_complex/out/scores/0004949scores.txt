score 20892.873066503438
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1015.281895278654,-1015.281895278654,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-931.568694308468,-931.568694308468,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-340.060519617179,-340.060519617179,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",5252.561741724848,5252.561741724848,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",3063.3722440706233,3063.3722440706233,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3296.1989156504665,3296.1989156504665,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3941.0018617450423,3941.0018617450423,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",2475.376887966541,2475.376887966541,1.0
"ev_restraint_resol_10",5856.425580164197,5856.425580164197,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1298.053265656781,1298.053265656781,1000.0
