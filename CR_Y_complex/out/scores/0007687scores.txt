score 27115.623233736733
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1821.4839553906504,-1821.4839553906504,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-258.14136162748525,-258.14136162748525,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",7317.908660639003,7317.908660639003,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1680.4766275798372,1680.4766275798372,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",5873.143932207218,5873.143932207218,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",2914.7673160600843,2914.7673160600843,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",13010.617736585828,13010.617736585828,1.0
"ev_restraint_resol_10",277.4670018669583,277.4670018669583,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",796.2388493656333,796.2388493656333,1000.0
