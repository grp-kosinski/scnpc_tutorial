score 40941.93754760772
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3231.336924009913,-3231.336924009913,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-647.0350238332176,-647.0350238332176,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",9101.141580918895,9101.141580918895,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1612.4146000816818,1612.4146000816818,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",9020.914683492585,9020.914683492585,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3998.6928320580823,3998.6928320580823,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",362.81621257579667,362.81621257579667,1.0
"ev_restraint_resol_10",23144.082778538956,23144.082778538956,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",379.2666183837359,379.2666183837359,1000.0
