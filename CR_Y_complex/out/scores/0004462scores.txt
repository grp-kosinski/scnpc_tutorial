score 80745.77647006535
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3654.472811376847,-3654.472811376847,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-1084.6708065258583,-1084.6708065258583,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-713.6466796036948,-713.6466796036948,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-1994.791982166091,-1994.791982166091,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-674.6987696311741,-674.6987696311741,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",62193.043751014986,62193.043751014986,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",0.0,0.0,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",523.2766228866864,523.2766228866864,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",0.0,0.0,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",10417.407612042545,10417.407612042545,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",12129.223398827951,12129.223398827951,1.0
"ev_restraint_resol_10",3369.377549345826,3369.377549345826,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",927.9642499869685,927.9642499869685,1000.0
