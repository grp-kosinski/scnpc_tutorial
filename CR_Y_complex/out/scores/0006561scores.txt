score 53267.33446618216
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2206.883991420493,-2206.883991420493,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-602.1743694187013,-602.1743694187013,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-676.744102592659,-676.744102592659,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",28973.874756901176,28973.874756901176,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1850.0697089053124,1850.0697089053124,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",692.5772433052008,692.5772433052008,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1772.6691900812348,1772.6691900812348,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",99.30475529998341,99.30475529998341,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",8392.141668184237,8392.141668184237,1.0
"ev_restraint_resol_10",15978.781185279426,15978.781185279426,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",1113.2063626723611,1113.2063626723611,1000.0
