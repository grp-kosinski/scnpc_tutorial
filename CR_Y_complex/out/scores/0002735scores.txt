score 33826.74703904802
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2027.4745809615692,-2027.4745809615692,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-831.8586133465209,-831.8586133465209,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",21549.774051658664,21549.774051658664,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",525.4161466682281,525.4161466682281,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3616.198867410453,3616.198867410453,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1269.9097412258884,1269.9097412258884,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",2215.0880308422584,2215.0880308422584,1.0
"ev_restraint_resol_10",8786.068183291187,8786.068183291187,10.0
"membrane_cerevisiae_no_neg_relative",1396.0966762634143,1396.0966762634143,1000.0
"P-complex_neg_stain_map_fit1",44.05222375408497,44.05222375408497,1000.0
