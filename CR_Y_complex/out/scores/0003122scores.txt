score 26754.066899253557
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-972.7629487486198,-972.7629487486198,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-621.7816926642624,-621.7816926642624,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-679.617340364991,-679.617340364991,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",10437.586286032187,10437.586286032187,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",637.0967827333902,637.0967827333902,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1140.9953650576913,1140.9953650576913,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",37.565825813553474,37.565825813553474,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",10051.793134468604,10051.793134468604,1.0
"ev_restraint_resol_10",8808.27638773322,8808.27638773322,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
