score 14926.474172356426
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3654.472811376847,-3654.472811376847,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-770.8389231275279,-770.8389231275279,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-674.6987696311741,-674.6987696311741,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",9144.418621365834,9144.418621365834,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",49.93532330355264,49.93532330355264,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",10431.639759665002,10431.639759665002,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1102.269904292867,1102.269904292867,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",437.7561642989389,437.7561642989389,1.0
"ev_restraint_resol_10",66.8204786193272,66.8204786193272,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",878.7293257536711,878.7293257536711,1000.0
