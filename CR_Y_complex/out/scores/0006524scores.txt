score 9699.851800303795
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3654.472811376847,-3654.472811376847,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",9144.418621365834,9144.418621365834,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",798.9694927216935,798.9694927216935,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",3968.4793983279606,3968.4793983279606,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1102.269904292867,1102.269904292867,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",654.6841148173582,654.6841148173582,1.0
"ev_restraint_resol_10",41.3903472253717,41.3903472253717,10.0
"membrane_cerevisiae_no_neg_relative",79.50271656310636,79.50271656310636,1000.0
"P-complex_neg_stain_map_fit1",1056.618042241514,1056.618042241514,1000.0
