score 148599.25914746325
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-3930.792785401962,-3930.792785401962,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-706.950430013049,-706.950430013049,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-679.617340364991,-679.617340364991,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",4439.237178246784,4439.237178246784,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1678.5051990553116,1678.5051990553116,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",13238.587901157392,13238.587901157392,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3614.6060959152214,3614.6060959152214,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",55778.22978806248,55778.22978806248,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",73989.44014296829,73989.44014296829,1.0
"ev_restraint_resol_10",3276.5546443372014,3276.5546443372014,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",0.0,0.0,1000.0
