score 59443.924063715815
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-2874.2731125791097,-2874.2731125791097,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-931.568694308468,-931.568694308468,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-595.2118834759732,-595.2118834759732,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-672.7827955832848,-672.7827955832848,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",12392.837064418956,12392.837064418956,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",810.4671058894969,810.4671058894969,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",1868.2815201727271,1868.2815201727271,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",3664.84366399209,3664.84366399209,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",15497.993485765612,15497.993485765612,1.0
"ev_restraint_resol_10",31077.74853498312,31077.74853498312,10.0
"membrane_cerevisiae_no_neg_relative",499.6778554646278,499.6778554646278,1000.0
"P-complex_neg_stain_map_fit1",79.50271656310636,79.50271656310636,1000.0
