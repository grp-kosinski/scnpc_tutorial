score 51744.025626130846
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-4146.568643872789,-4146.568643872789,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-888.4390456472057,-888.4390456472057,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-608.0703845899029,-608.0703845899029,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-2115.1768505819514,-2115.1768505819514,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",4126.484961148163,4126.484961148163,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",502.776500086803,502.776500086803,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",253.74492529740675,253.74492529740675,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",1437.6381153331374,1437.6381153331374,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",3054.5087530437554,3054.5087530437554,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",29331.456093578192,29331.456093578192,1.0
"ev_restraint_resol_10",21939.723889842528,21939.723889842528,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",263.9417502874902,263.9417502874902,1000.0
