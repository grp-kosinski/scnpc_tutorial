score 138665.92359632102
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1535.1957482439475,-1535.1957482439475,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-692.9882152761896,-692.9882152761896,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-679.531869583957,-679.531869583957,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-692.2356647359527,-692.2356647359527,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",135795.01581269398,135795.01581269398,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",1005.6460837364513,1005.6460837364513,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",4117.822924472244,4117.822924472244,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",97.46275850103201,97.46275850103201,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",2459.0333952675933,2459.0333952675933,1.0
"ev_restraint_resol_10",0.4045313295385329,0.4045313295385329,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",217.7418644391893,217.7418644391893,1000.0
