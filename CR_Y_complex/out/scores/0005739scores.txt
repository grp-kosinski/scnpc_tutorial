score 49941.64631667715
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb_0",-1353.3342430051223,-1353.3342430051223,1000.0
"DiscreteMoverRestraint rb_ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb_0",-713.3173664873073,-713.3173664873073,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb_0",-612.0819127640872,-612.0819127640872,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb_0",-1134.118397091789,-1134.118397091789,1000.0
"DiscreteMoverRestraint rb_4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb_0",-713.9349097916579,-713.9349097916579,1000.0
"DiscreteMoverRestraint rb_ScNup133N_56-480_renamed.pdb_0",-694.0595280031271,-694.0595280031271,1000.0
"conn restraint Residue_436-Residue_437 of Nup84.0",16520.925200028392,16520.925200028392,1.0
"conn restraint Residue_544-Residue_553 of Nup85.0",60.33927008094728,60.33927008094728,1.0
"conn restraint Residue_711-Residue_715 of Nup120.0",5311.106668055909,5311.106668055909,1.0
"conn restraint Residue_480-Residue_490 of Nup133.0",116.88372232254241,116.88372232254241,1.0
"conn restraint Residue_99-Residue_149 of Nup145c.0",0.0,0.0,1.0
"conn restraint Residue_550-Residue_554 of Nup145c.0",11802.97119923904,11802.97119923904,1.0
"ev_restraint_resol_10",18647.50463157247,18647.50463157247,10.0
"membrane_cerevisiae_no_neg_relative",0.0,0.0,1000.0
"P-complex_neg_stain_map_fit1",2702.761982520941,2702.761982520941,1000.0
