from string import Template

protocol = 'denovo_MC-SA'

SA_schedule = [
    (100,   10000),
    (10,   10000),
    (1,   10000)
]

do_ini_opt = False


traj_frame_period = 100
print_frame_period = traj_frame_period

print_log_scores_to_files = True
print_log_scores_to_files_frame_period = 10

print_total_score_to_files = True
print_total_score_to_files_frame_period = 10

states = 1
struct_resolutions = [1,10]
add_missing = False
missing_resolution = 1

add_rbs_from_pdbs = True


connectivity_restraint_weight = 1.
max_conn_gap = None
conn_first_copy_only = False

rb_max_rot = 2
rb_max_trans = 3

add_symmetry_constraints = False
add_symmetry_restraints = False

add_parsimonious_states_restraints = False
parsimonious_states_weight = 10
parsimonious_states_distance = 0.0

add_xlink_restraints = False

add_em_restr = False

em_restraints = [
    {
        'name': 'em_restraints_highres',
        'type': 'FitRestraint',
        'weight': 100,
        'repr_resolution': 1
    },
    {
        'name': 'em_restraints_highres1',
        'type': 'EnvelopePenetrationRestraint',
        'weight': 50,
        'repr_resolution': 1
    },
    {
        'name': 'em_restraints_lowres',
        'type': 'EnvelopePenetrationRestraint',
        'weight': 50,
        'repr_resolution': 10
    },
]


discrete_restraints_weight=1000 # weight for the restraint derived from the precomputed fits

ev_restraints = [
    {
        'name': 'ev_restraints_lowres',
        'weight': 10,
        'repr_resolution': 10
    },
    {
        'name': 'ev_restraints_highres',
        'weight': 10,
        'repr_resolution': 1
    }
]

# example function for defining custom restraints, define also at the configuration file
def create_custom_restraints(r):
    em_restraints = r.add_em_restraints(
                        weight=1000,
                        first_copy_only=False,
                        resolution=10)

    restraints = {}
    for i, restr in enumerate(em_restraints):
        restraints[str(restr).replace('"','')+str(i)] = [restr]

    return restraints


scoring_functions = {
    'score_func_ini_opt': {
        'restraints': [
            'discrete_restraints',
            'conn_restraints',
            'ev_restraints_lowres',
            "ExcludeMapRestraint0",
            "ExcludeMapRestraint1"
        ]
    },
    'score_func_highres': {
        'restraints': [
            'discrete_restraints',
            #'xlink_restraints',
            'conn_restraints',
            'ev_restraints_highres'
        ]
    },
    'score_func_lowres': {
        'restraints': [
            'discrete_restraints',
            'conn_restraints',
            'ev_restraints_lowres',
            "ExcludeMapRestraint0",
            "ExcludeMapRestraint1"
        ]
    },
    'score_func_for_CG': {
        'restraints': [
            #'xlink_restraints',
            'conn_restraints',
            'ev_restraints_lowres'
        ]
    },
    'score_func_precond': {
        'restraints': [
            'discrete_restraints',
            #'xlink_restraints',
            'conn_restraints',
            'ev_restraints_lowres'
        ]
    }
}

score_func = 'score_func_lowres'
score_func_for_CG = 'score_func_for_CG'
score_func_ini_opt = 'score_func_ini_opt'
score_func_preconditioned_mc = 'score_func_precond'

# SLURM template, adjust to your cluster environment
ntasks = 1
cluster_submission_command = 'sbatch'
run_script_templ = Template("""#!/bin/bash
#
#SBATCH --ntasks=$ntasks
#SBATCH --mem-per-cpu=2000
#SBATCH --job-name=${prefix}fit
#SBATCH --time=10:00:00
#SBATCH -e $outdir/logs/${prefix}_err.txt
#SBATCH -o $outdir/logs/${prefix}_out.txt

echo "Running on:"
srun hostname

$cmd

wait
""")