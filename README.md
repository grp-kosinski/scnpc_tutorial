# ScNPC_tutorial

This repository includes all the relevant input data, precalculated modelling results and templates in order to set up and run the integrative modelling of S. cerevisiae nuclear pore complex (ScNPC) adapted from ``Allegretti, Matteo, et al. "In-cell architecture of the nuclear pore and snapshots of its turnover." Nature 586.7831 (2020): 796-800.``. Use the data sets provided in the repository to follow and complete the online ScNPC integrative modelling tutorial with Assembline from here: https://scnpc-tutorial.readthedocs.io/en/latest/index.html  
