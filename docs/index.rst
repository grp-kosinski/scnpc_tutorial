.. ScNPC_tutorial documentation master file, created by
   sphinx-quickstart on Wed Jan 27 14:46:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tutorial: integrative modelling of S. cerevisiae nuclear pore complex (ScNPC) using Assembline
==============================================================================================

`Assembline <https://assembline.readthedocs.io/en/latest/#>`__ is a software for integrative structural modelling of macromolecular assemblies - an assembly line of macromolecular assemblies!

This tutorial demonstrates the usage of Assembline on an example of S. cerevisiae nuclear pore complex (ScNPC) from wild-type (wt) and Nup116 (NPC member protein) knock-out (k.o.) cells. By following the detailed data preparation and modelling steps for Assembline the users will be able to reproduce our previously published integrative models (`Allegretti et al. (2020) <https://doi.org/10.1038/s41586-020-2670-5>`__) of:
Cytoplasmic Ring Y-complex (CR Y-complex), Inner Ring asymmetric unit (IR asymmetric unit), Nuclear Ring Y-complex (NR Y-complex), Nup116 k.o. ScNPC (at 25C), Nup116 k.o. ScNPC (at 37C).

* All the necessary input data and modelling templates are provided (as well as some precalculated output) and should be downloaded from our `ScNPC_tutorial git repository <https://git.embl.de/rantos/scnpc_tutorial.git>`__. In this repository you will find the following directories which include all relevant files per modelling target (subcomplexes and k.o. ScNPC models) ::

	CR_Y_complex
	IR_asymmetric_unit_refinement
	NR_Y_complex
	Nup116delta25C
	Nup116delta37C 

.. note:: In case you want to directly jump to modelling ScNPCs hence skipping the theory, set ups and general usage instructions then you can continue from the :doc:`cr_y_complex_tutorial`.

Use the **Next** button at the bottom right or menu on the left to navigate through the tutorial.

.. toctree::
   :maxdepth: 2

   about-complex

.. toctree::
   :maxdepth: 2   
   :caption: Data preparation

   data_prep

.. toctree::
   :maxdepth: 2   
   :caption: Calculation of fit libraries

   fit_libraries_intro
   efitter_params
   efitter_run
   efitter_analyze

.. toctree::
   :maxdepth: 2   
   :caption: Global optimization

   json_setup
   params_setup
   run_denovo
   analysis_denovo

.. toctree::
   :maxdepth: 2   
   :caption: Refinement

   json_setup_refinement
   params_setup_refinement
   run_refinement
   analysis_refinement

.. toctree::
   :maxdepth: 2   
   :caption: CR Y-complex modelling

   cr_y_complex_tutorial

.. toctree::
   :maxdepth: 2   
   :caption: IR asymmetric unit modelling

   ir_unit_tutorial

.. toctree::
   :maxdepth: 2   
   :caption: NR Y-complex modelling

   nr_y_complex_tutorial

.. toctree::
   :maxdepth: 2   
   :caption: Modelling of Nup116 k.o. ScNPC (at 25C)

   nup116delta_25c_scnpc_tutorial

.. toctree::
   :maxdepth: 2   
   :caption: Modelling of Nup116 k.o. ScNPC (at 37C)

   nup116delta_37c_scnpc_tutorial