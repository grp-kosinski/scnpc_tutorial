Nup116 k.o. ScNPC (at 37C) modelling
====================================

For modelling the Nup116 k.o. ScNPC (at 37C) the local rigid body refinement method from `Assembline <https://assembline.readthedocs.io/en/latest/#>`__ will be used. The dir ``scnpc_tutorial/Nup116delta37C`` includes source code files, input files and precalculated modelling results for the Nup116 k.o. ScNPC model (37C): ::

    - parameters files & configuration files for 'refinement' integrative modelling of Nup116delta37C ScNPC
    - sequence fasta file, input_PDB, EM (input data)
    - ScNPC_Nup116delta37C_final_model.pdb, ScNPC_Nup116delta37C_IR_final_model.pdb, ScNPC_Nup116delta37C_NR_final_model.pdb, out_IR, out_NR (output modeling results)

#. First activate your virtual environment and enter the ``Nup116delta37C`` dir

    .. code-block:: bash

        source activate Assembline

        cd Nup116delta37C/

    or depending on your computer setup:

    .. code-block:: bash

        conda activate Assembline

        cd Nup116delta37C/

#. Run the refinement for NR Y-complex and half-assembly of IR unit (two separate refinement runs)

    .. code-block:: bash

            # this will run 500 refinement modelling runs for NR Y-complex on a slurm cluster (for options/parameters or local runs inspect the Assembline manual)
            assembline.py --traj --models -o out --multi --start_idx 0 --njobs 500 config_NR.json params_NR.py

            # this will run 500 refinement modelling runs for IR unit subunits on a slurm cluster (for options/parameters or local runs inspect the Assembline manual)
            assembline.py --traj --models -o out --multi --start_idx 0 --njobs 500 config_IR.json params_IR.py

    .. note:: There are already output dirs ``Nup116delta37C/out_NR`` and ``Nup116delta37C/out_IR`` so in case you want to run the modelling then rename the ``out_IR/`` and/or ``out_NR/`` dir as it will be overwritten from the run above 

#. Enter any of the ``out/`` dir (e.g. ``out_NR/``), generate output scoring lists and rebuild atomic structures of models 

    .. code-block:: bash

        #these steps can be followed for any of the out_NR and out_IR
        cd out_NR

        extract_scores.py #this should create a couple of files including all_scores_sorted_uniq.csv

        rebuild_atomic.py --top 10 --project_dir <full path to the original project directory Nup116delta37C> config_NR.json all_scores_sorted_uniq.csv

#. While in the ``out/`` dir run the following command to prepare your output models for analysis with ``imp-sampcon`` tool from ``IMP``
    
    .. code-block:: bash

        setup_analysis.py -s all_scores.csv -o analysis -d density.txt

    .. note:: The density.txt is not provided but only in the ``CR_Y_complex/out``, therefore visit this dir to inspect it. To generate it yourself please inspect `Assembline analysis section <https://assembline.readthedocs.io/en/latest/sampling_exhaustiveness.html>`__.

#. Run ``imp-sampcon exhaust`` tool (command-line tool provided with IMP) to perform the sampling analysis:

    .. code-block:: bash

        cd analysis

        imp_sampcon exhaust -n <prefix for output files> \
        --rmfA sample_A/sample_A_models.rmf3 \
        --rmfB sample_B/sample_B_models.rmf3 \
        --scoreA scoresA.txt --scoreB scoresB.txt \
        -d ../density.txt \
        -m cpu_omp \
        -c <int for cores to process> \
        -gp \
        -g <float with clustering threshold step> \

    .. note:: For further descriptions of settings for ``imp_sampcon`` please see `Sampling exhaustiveness and precision with Assembline <https://assembline.readthedocs.io/en/latest/sampling_exhaustiveness.html#>`_