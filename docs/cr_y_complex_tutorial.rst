CR Y-complex modelling
======================

For modelling the CR Y-complex the calculation of fit libraries and global optimization with `Assembline <https://assembline.readthedocs.io/en/latest/#>`__ will be used. The dir ``scnpc_tutorial/CR_Y_complex`` includes source code files, input files and precalculated modelling results for the CR Y-complex: ::

    - parameters file & configuration file for global optimization of wt CR Y-complex
    - sequence fasta file, input_PDB, EM (input data)
    - CR_Y_complex_final_model.pdb, out (output modelling results)
    - systematic_fits (directory):

        - parameters file for systematic fitting
        - em_maps, PDB (input data)
        - result_fits_chimera (output fitting results)

#. First activate your virtual environment and enter the ``CR_Y_complex/systematic_fits`` dir

    .. code-block:: bash

        source activate Assembline

        cd CR_Y_complex/systematic_fits/

    or depending on your computer setup:

    .. code-block:: bash

        conda activate Assembline

        cd CR_Y_complex/systematic_fits/

#. Run the generation of fit libraries for CR Y-complex rigid bodies (results calculated in dir ``systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/``)

    .. code-block:: bash

        fit.py systematic_fitting_parameters.py

#. The fit libraries have been precalculated and analysed in dir ``systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/``. To analyse the fit results on your own run the following while in the systematic_fits/ dir

    .. code-block:: bash

            genpval.py result_fits_chimera

#. To generate the top five fits of each input rigid body (i.e. top five fits from each fit library) run the following

    .. code-block:: bash

            #upon successful run of fit.py and genpval.py
            cd result_fits_chimera/search100000_metric_cam_rad_600_inside0.3_res_40

            genPDBs_many.py -n5 top5 */*/solutions.csv

#. After completing the calculation of fit libraries (or use the precalculated results) enter again the main project dir (i.e. CR_Y_complex) and run the global optimization

    .. code-block:: bash

            cd CR_Y_complex

            # this will run 20000 global optimization modelling runs on a slurm cluster (for options/parameters or local runs inspect the Assembline manual)
            assembline.py --traj --models -o out --multi --start_idx 0 --njobs 20000 config.json params.py

    .. note:: There is already an output dir ``CR_Y_complex/out`` so in case you want to run the modelling then rename the ``out/`` dir as it will be overwritten from the run above 

#. Enter ``out/`` dir, generate output scoring lists and rebuild atomic structures of models 

    .. code-block:: bash

        cd out

        extract_scores.py #this should create a couple of files including all_scores_sorted_uniq.csv

        rebuild_atomic.py --top 10 --project_dir <full path to the original project directory CR_Y_complex> config.json all_scores_sorted_uniq.csv

#. While in the ``out/`` dir run the following command to prepare your output models for analysis with ``imp-sampcon`` tool from ``IMP``
    
    .. code-block:: bash

        setup_analysis.py -s all_scores.csv -o analysis -d density.txt

    .. note:: The density.txt is provided in the ``CR_Y_complex/out``. To generate it yourself please inspect `Assembline analysis section <https://assembline.readthedocs.io/en/latest/sampling_exhaustiveness.html>`__.

#. Run ``imp-sampcon exhaust`` tool (command-line tool provided with IMP) to perform the sampling analysis:

    .. code-block:: bash

        cd analysis

        imp_sampcon exhaust -n <prefix for output files> \
        --rmfA sample_A/sample_A_models.rmf3 \
        --rmfB sample_B/sample_B_models.rmf3 \
        --scoreA scoresA.txt --scoreB scoresB.txt \
        -d ../density.txt \
        -m cpu_omp \
        -c <int for cores to process> \
        -gp \
        -g <float with clustering threshold step> \

    .. note:: For further descriptions of settings for ``imp_sampcon`` please see `Sampling exhaustiveness and precision with Assembline <https://assembline.readthedocs.io/en/latest/sampling_exhaustiveness.html#>`_