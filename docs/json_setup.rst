Setting up the JSON project file
================================

.. note:: You can find a ready-to-use JSON file for global optimization (configuration file) named ``config.json`` in ``CR_Y_complex`` and ``NR_Y_complex/NR_Y_complex_de_novo_run`` folders of the `ScNPC_tutorial git repository <https://git.embl.de/rantos/scnpc_tutorial.git>`__.

Create Xlink Analyzer project
-----------------------------

Here is the instruction how to create the JSON file from scratch.

First, you need to create `XlinkAnalyzer <https://www.embl-hamburg.de/XlinkAnalyzer/XlinkAnalyzer.html>`_ project file for your complex

.. note:: `XlinkAnalyzer <https://www.embl-hamburg.de/XlinkAnalyzer/XlinkAnalyzer.html>`_ is used here as a graphical interface for input preparation in Assembline.

    Does not matter if you do not have crosslinks - we use XlinkAnalyzer to prepare the input file for modeling.

#. Open Xlink Analyzer

#. In the Xlink Analyzer project Setup menu, define subunits

    Set up the chain IDs as you want them in the final models, they do not have to correspond to chain IDs in your input PDB files.

    Example:
        * ``Nup133`` - chain IDs: ``K``
          
        * ``Nup84`` - chain IDs: ``L``
          
        * ``Nup120`` - chain IDs: ``R``

    .. note:: Remember that you can inspect the exact chain definition for ScNPC subcomplexes in each subcomplex folder (e.g. ``CR_Y_complex``) from our `ScNPC_tutorial git repository <https://git.embl.de/rantos/scnpc_tutorial.git>`__.
   
#. Load sequence data

    Add sequences of all proteins in a single file in `FASTA format <https://en.wikipedia.org/wiki/FASTA_format>`_

    Here, use the ``ScNPC_sequences.fasta`` file provided in the tutorial materials and map the sequence names to names of subunits using the Map button
   
#. Save the JSON file under a name like ::

    xla_project.json

#. And make a copy (to your modelling directory) that you will modify for modelling ::
   
    cp xla_project.json config.json

Add modelling information to the project file
--------------------------------------------

#. Open ``config.json`` in a text editor.
   
   .. note:: The project file is in so-called `JSON format <https://en.wikipedia.org/wiki/JSON>`_

        While it may look difficult to edit at the first time, it is actually quite OK with a proper editor (and a bit of practice ;-)
    
        We recommend to use a good editor such as:

            * `SublimeText <https://www.sublimetext.com/>`_
        
            * `Atom <https://atom.io/>`_

    At this point, the JSON has the following format:

    .. code-block:: JSON

        {
            "data": [
                {
                    "some xlink definition 1"
                },
                {
                    "some xlink definition 2"
                },
                {
                    "sequence file definition"
                }
            ],
            "subunits": [
                    "subunit definitions"
            ],
            "xlinkanalyzerVersion": "..."
        }

#. Add symmetry (if any, following example is from ``IR_asymmetric_unit_refinement/``)

	#. First, specify the series of symmetry related molecules. Here, each of the three subunits is in two symmetrical copies, so we add series as below:
	       
	        .. code-block:: json

	        	"series": [
	                {
	                "name": "IR_cyt_outer",
	                "subunit": "Nic96",
	                "mode": "auto",  
	                "chainIds": ["A"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_cyt_inner",
	                "subunit": "Nic96",
	                "mode": "auto",  
	                "chainIds": ["A"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_nuc_inner",
	                "subunit": "Nic96",
	                "mode": "auto",  
	                "chainIds": ["A"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_inner"
	                },
	                {
	                "name": "IR_nuc_outer",
	                "subunit": "Nic96",
	                "mode": "auto",  
	                "chainIds": ["A"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_outer"
	                },

	                {
	                "name": "IR_cyt_outer",
	                "subunit": "Nup157",
	                "mode": "auto",  
	                "chainIds": ["D"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_nuc_outer",
	                "subunit": "Nup157",
	                "mode": "auto",  
	                "chainIds": ["D"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_outer"
	                },
	                {
	                "name": "IR_cyt_inner",
	                "subunit": "Nup170",
	                "mode": "auto",  
	                "chainIds": ["d"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_nuc_inner",
	                "subunit": "Nup170",
	                "mode": "auto",  
	                "chainIds": ["d"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_inner"
	                },

	                {
	                "name": "IR_cyt_inner",
	                "subunit": "Nup192",
	                "mode": "auto",  
	                "chainIds": ["C"],
	                "cell_count": 1,
	                "inipos": "input"
	                },

	                {
	                "name": "IR_nuc_inner",
	                "subunit": "Nup192",
	                "mode": "auto",  
	                "chainIds": ["C"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_inner"
	                },
	                {
	                "name": "IR_cyt_outer",
	                "subunit": "Nup188",
	                "mode": "auto",  
	                "chainIds": ["B"],
	                "cell_count": 1,
	                "inipos": "input"
	                },

	                {
	                "name": "IR_nuc_outer",
	                "subunit": "Nup188",
	                "mode": "auto",  
	                "chainIds": ["B"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_outer"
	                },

	                {
	                "name": "IR_cyt_outer",
	                "subunit": "Nsp1",
	                "mode": "auto",  
	                "chainIds": ["J"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_cyt_inner",
	                "subunit": "Nsp1",
	                "mode": "auto",  
	                "chainIds": ["J"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_nuc_inner",
	                "subunit": "Nsp1",
	                "mode": "auto",  
	                "chainIds": ["J"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_inner"
	                },
	                {
	                "name": "IR_nuc_outer",
	                "subunit": "Nsp1",
	                "mode": "auto",  
	                "chainIds": ["J"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_outer"
	                },

	                {
	                "name": "IR_cyt_outer",
	                "subunit": "Nup49",
	                "mode": "auto",  
	                "chainIds": ["I"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_cyt_inner",
	                "subunit": "Nup49",
	                "mode": "auto",  
	                "chainIds": ["I"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_nuc_inner",
	                "subunit": "Nup49",
	                "mode": "auto",  
	                "chainIds": ["I"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_inner"
	                },
	                {
	                "name": "IR_nuc_outer",
	                "subunit": "Nup49",
	                "mode": "auto",  
	                "chainIds": ["I"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_outer"
	                },

	                {
	                "name": "IR_cyt_outer",
	                "subunit": "Nup57",
	                "mode": "auto",  
	                "chainIds": ["H"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_cyt_inner",
	                "subunit": "Nup57",
	                "mode": "auto",  
	                "chainIds": ["H"],
	                "cell_count": 1,
	                "inipos": "input"
	                },
	                {
	                "name": "IR_nuc_inner",
	                "subunit": "Nup57",
	                "mode": "auto",  
	                "chainIds": ["H"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_inner"
	                },
	                {
	                "name": "IR_nuc_outer",
	                "subunit": "Nup57",
	                "mode": "auto",  
	                "chainIds": ["H"],
	                "cell_count": 1,
	                "inipos": "input",
	                "ref_serie": "IR_cyt_outer"
	                }
		        ]

	#. Second, define the coordinates of the symmetry axis:

        .. code-block:: json

			    "symmetry": {
			        "sym_tr3ds": [
			            {
			                "name": "2-fold",
			                "rot": [0.91878368,   0.18447527,  -0.34900635, 0.18489318,  -0.98222332,  -0.03243228, -0.34878513, -0.03473065,  -0.93655898],
			                "trans": [113.25839839, 916.45466348, 1106.65193191]      
			            }          
			        ],
			        "apply_symmetry": [
			            {
			                "sym": "2-fold",
			                "restraint_type": "symmetry_restraint",
			                "selectors": [
			                    {"subunit": "Nic96", "copies": [0, 3]},
			                    {"subunit": "Nic96", "copies": [1, 2]},
			                    {"subunit": "Nup188", "copies": [0, 1]},
			                    {"subunit": "Nup192", "copies": [0, 1]},
			                    {"subunit": "Nup157", "copies": [0, 1]},
			                    {"subunit": "Nup170", "copies": [0, 1]}
			                ]
			            }
			        ],
			        }     

#. Add specification of input PDB files

	.. note:: The input structures for the tutorial are in the ``in_pdbs/`` directories of each subcomplex in our git repository `ScNPC_tutorial <https://git.embl.de/rantos/scnpc_tutorial.git>`__.

	Add them to the JSON like this (following example is from ``IR_asymmetric_unit_refinement/``):

    .. code-block:: json

    	"data": [
        {
            "type": "pdb_files",
            "name": "pdb_files",
            "data": [
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNic96_CTD_cyt_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNic96_CTD_cyt_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNic96_CTD_nucl_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNic96_CTD_nucl_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNsp1_complex_cyt_out.pdb"
                                },
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nsp1",
                                "subunit": "Nsp1",
                                "chain_id": "J",
                                "filename": "input_PDB/ScNsp1_complex_cyt_out.pdb"
                                },
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nup57",
                                "subunit": "Nup57",
                                "chain_id": "H",
                                "filename": "input_PDB/ScNsp1_complex_cyt_out.pdb"
                                },
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nup49",
                                "subunit": "Nup49",
                                "chain_id": "I",
                                "filename": "input_PDB/ScNsp1_complex_cyt_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNsp1_complex_cyt_in.pdb"
                                },
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nsp1",
                                "subunit": "Nsp1",
                                "chain_id": "J",
                                "filename": "input_PDB/ScNsp1_complex_cyt_in.pdb"
                                },
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nup57",
                                "subunit": "Nup57",
                                "chain_id": "H",
                                "filename": "input_PDB/ScNsp1_complex_cyt_in.pdb"
                                },
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nup49",
                                "subunit": "Nup49",
                                "chain_id": "I",
                                "filename": "input_PDB/ScNsp1_complex_cyt_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNsp1_complex_nucl_in.pdb"
                                },
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nsp1",
                                "subunit": "Nsp1",
                                "chain_id": "J",
                                "filename": "input_PDB/ScNsp1_complex_nucl_in.pdb"
                                },
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nup57",
                                "subunit": "Nup57",
                                "chain_id": "H",
                                "filename": "input_PDB/ScNsp1_complex_nucl_in.pdb"
                                },
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nup49",
                                "subunit": "Nup49",
                                "chain_id": "I",
                                "filename": "input_PDB/ScNsp1_complex_nucl_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nic96",
                                "subunit": "Nic96",
                                "chain_id": "A",
                                "filename": "input_PDB/ScNsp1_complex_nucl_out.pdb"
                                },
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nsp1",
                                "subunit": "Nsp1",
                                "chain_id": "J",
                                "filename": "input_PDB/ScNsp1_complex_nucl_out.pdb"
                                },
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nup57",
                                "subunit": "Nup57",
                                "chain_id": "H",
                                "filename": "input_PDB/ScNsp1_complex_nucl_out.pdb"
                                },
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nup49",
                                "subunit": "Nup49",
                                "chain_id": "I",
                                "filename": "input_PDB/ScNsp1_complex_nucl_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nup188",
                                "subunit": "Nup188",
                                "chain_id": "B",
                                "filename": "input_PDB/ScNup188_NTD_cyt_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nup188",
                                "subunit": "Nup188",
                                "chain_id": "B",
                                "filename": "input_PDB/ScNup188_NTD_nucl_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nup188",
                                "subunit": "Nup188",
                                "chain_id": "B",
                                "filename": "input_PDB/ScNup188_CTD_cyt_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nup188",
                                "subunit": "Nup188",
                                "chain_id": "B",
                                "filename": "input_PDB/ScNup188_CTD_nucl_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nup192",
                                "subunit": "Nup192",
                                "chain_id": "C",
                                "filename": "input_PDB/ScNup192_NTD_cyt_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nup192",
                                "subunit": "Nup192",
                                "chain_id": "C",
                                "filename": "input_PDB/ScNup192_NTD_nucl_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nup192",
                                "subunit": "Nup192",
                                "chain_id": "C",
                                "filename": "input_PDB/ScNup192_CTD_cyt_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nup192",
                                "subunit": "Nup192",
                                "chain_id": "C",
                                "filename": "input_PDB/ScNup192_CTD_nucl_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nup157",
                                "subunit": "Nup157",
                                "chain_id": "D",
                                "filename": "input_PDB/ScNup157_NTD_cyt_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nup157",
                                "subunit": "Nup157",
                                "chain_id": "D",
                                "filename": "input_PDB/ScNup157_NTD_nucl_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_outer",
                                "name": "Nup157",
                                "subunit": "Nup157",
                                "chain_id": "D",
                                "filename": "input_PDB/ScNup157_CTD_cyt_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_outer",
                                "name": "Nup157",
                                "subunit": "Nup157",
                                "chain_id": "D",
                                "filename": "input_PDB/ScNup157_CTD_nucl_out.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nup170",
                                "subunit": "Nup170",
                                "chain_id": "d",
                                "filename": "input_PDB/ScNup170_NTD_cyt_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nup170",
                                "subunit": "Nup170",
                                "chain_id": "d",
                                "filename": "input_PDB/ScNup170_NTD_nucl_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_cyt_inner",
                                "name": "Nup170",
                                "subunit": "Nup170",
                                "chain_id": "d",
                                "filename": "input_PDB/ScNup170_CTD_cyt_in.pdb"
                                }
                            ]
                        },
                        {
                            "foreach_copy": true,
                            "components": [
                                {
                                "serie": "IR_nuc_inner",
                                "name": "Nup170",
                                "subunit": "Nup170",
                                "chain_id": "d",
                                "filename": "input_PDB/ScNup170_CTD_nucl_in.pdb"
                                }
                            ]
                        }
                ]

    The ``foreach_serie`` and ``foreach_copy`` indicate the given PDB file specification will be applied to each serie with this subunit and 
    for each copy within the series. 

    All PDB selections within the same ``components`` block will be grouped into a rigid body, unless a separate ``rigid_bodies`` block is specified and ``add_rbs_from_pdbs`` is set to ``False`` in :doc:`params_setup`
   
#. Add pointers to fit libraries and sequence FASTA file (following example is from ``CR_Y_complex/``)
	
	## First add the pointers to input fit libraries

		 .. code-block:: json

				"data": [
	                        {
	                            "components": [
	                                {
	                                "name": "Nup133",
	                                "subunit": "Nup133",
	                                "chain_id": "K",
	                                "filename": "input_PDB/ScNup133N_56-480_renamed.pdb"
	                                }
	                            ],
	                            "positions": "systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/ScNup133N_56-480_renamed.pdb/solutions_pvalues.csv",
	                            "positions_type": "chimera", // optional, chimera is default
	                            "max_positions": 10000, // optional, by default all fits are read
	                            "positions_score": "log_BH_adjusted_pvalues_one_tailed"
	                        },
	                        {
	                            "components": [
	                                {
	                                "name": "Nup84",
	                                "subunit": "Nup84",
	                                "chain_id": "L",
	                                "filename": "input_PDB/ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb"
	                                },
	                                {
	                                "name": "Nup133",
	                                "subunit": "Nup133",
	                                "chain_id": "K",
	                                "filename": "input_PDB/ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb"
	                                }
	                            ],
	                            "positions": "systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb/solutions_pvalues.csv",
	                            "positions_type": "chimera", // optional, chimera is default
	                            "max_positions": 10000, // optional, by default all fits are read
	                            "positions_score": "log_BH_adjusted_pvalues_one_tailed"
	                        },
	                        {
	                            "components": [
	                                {
	                                "name": "Nup145c",
	                                "subunit": "Nup145c",
	                                "chain_id": "M",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb"
	                                },
	                                {
	                                "name": "Sec13",
	                                "subunit": "Sec13",
	                                "chain_id": "N",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb"
	                                },
	                                {
	                                "name": "Nup84",
	                                "subunit": "Nup84",
	                                "chain_id": "L",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb"
	                                }
	                            ],
	                            "positions": "systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb/solutions_pvalues.csv",
	                            "positions_type": "chimera", // optional, chimera is default
	                            "max_positions": 100, // optional, by default all fits are read
	                            "positions_score": "log_BH_adjusted_pvalues_one_tailed"
	                        },
	                        {
	                            "components": [
	                                
	                                {
	                                "name": "Seh1",
	                                "subunit": "Seh1",
	                                "chain_id": "O",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb"
	                                },
	                                {
	                                "name": "Nup85",
	                                "subunit": "Nup85",
	                                "chain_id": "P",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb"
	                                }
	                            ],
	                            "positions": "systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb/solutions_pvalues.csv",
	                            "positions_type": "chimera", // optional, chimera is default
	                            "max_positions": 10000, // optional, by default all fits are read
	                            "positions_score": "log_BH_adjusted_pvalues_one_tailed"
	                        },
	                        {
	                            "components": [
	                                {
	                                "name": "Nup85",
	                                "subunit": "Nup85",
	                                "chain_id": "P",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb"
	                                },
	                                {
	                                "name": "Nup145c",
	                                "subunit": "Nup145c",
	                                "chain_id": "M",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb"
	                                },
	                                {
	                                "name": "Nup120",
	                                "subunit": "Nup120",
	                                "chain_id": "R",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb"
	                                }
	                            ],
	                            "positions": "systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb/solutions_pvalues.csv",
	                            "positions_type": "chimera", // optional, chimera is default
	                            "max_positions": 10000, // optional, by default all fits are read
	                            "positions_score": "log_BH_adjusted_pvalues_one_tailed"
	                        },
	                        {
	                            "components": [
	                                {
	                                "name": "Nup120",
	                                "subunit": "Nup120",
	                                "chain_id": "R",
	                                "filename": "input_PDB/4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb"
	                                }
	                            ],
	                            "positions": "systematic_fits/result_fits_chimera/CR_with_Y_no_env_relative.mrc/4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb/solutions_pvalues.csv",
	                            "positions_type": "chimera", // optional, chimera is default
	                            "max_positions": 10000, // optional, by default all fits are read
	                            "positions_score": "log_BH_adjusted_pvalues_one_tailed"
	                        }
	                ]

    ## Now add the fasta sequence pointer and maping of the subunits

    		.. code-block:: json

	            "fileGroup": {
	                "files": [
	                    "ScNPC_sequences.fasta"
	                ]
	            },
	            "mapping": {
	                "sp|P35729|NU120_YEAST Nucleoporin NUP120 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP120 PE=1 SV=1":["Nup120"],                
	                "sp|P49687|NU145C Nucleoporin NUP145 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP145 PE=1 SV=1":["Nup145c"],
	                "sp|P49687|NU145N Nucleoporin NUP145 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP145 PE=1 SV=1":["Nup145n"],
	                "sp|P46673|NUP85_YEAST Nucleoporin NUP85 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP85 PE=1 SV=1":["Nup85"],
	                "sp|P53011|SEH1_YEAST Nucleoporin SEH1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=SEH1 PE=1 SV=1":["Seh1"],
	                "sp|Q04491|SEC13_YEAST Protein transport protein SEC13 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=SEC13 PE=1 SV=1":["Sec13"],
	                "sp|P52891|NUP84_YEAST Nucleoporin NUP84 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP84 PE=1 SV=1":["Nup84"],
	                "sp|P36161|NU133_YEAST Nucleoporin NUP133 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP133 PE=1 SV=1":["Nup133"],
	                "sp|P52593|NU188_YEAST Nucleoporin NUP188 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP188 PE=1 SV=1":["Nup188"],
	                "sp|P47054|NU192_YEAST Nucleoporin NUP192 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP192 PE=1 SV=1":["Nup192"],
	                "sp|P40064|NU157_YEAST Nucleoporin NUP157 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP157 PE=1 SV=1":["Nup157"],
	                "sp|P38181|NU170_YEAST Nucleoporin NUP170 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP170 PE=1 SV=1":["Nup170"],
	                "sp|P34077|NIC96_YEAST Nucleoporin NIC96 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NIC96 PE=1 SV=2":["Nic96"],
	                "sp|Q03790|NUP53_YEAST Nucleoporin NUP53 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP53 PE=1 SV=1":["Nup53"],
	                "sp|Q05166|NUP59_YEAST Nucleoporin ASM4 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=ASM4 PE=1 SV=1":["Nup59"],
	                "sp|P14907|NSP1_YEAST Nucleoporin NSP1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NSP1 PE=1 SV=1":["Nsp1"],
	                "sp|P48837|NUP57_YEAST Nucleoporin NUP57 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP57 PE=1 SV=1":["Nup57"],
	                "sp|Q02199|NUP49_YEAST Nucleoporin NUP49/NSP49 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP49 PE=1 SV=1":["Nup49"],
	                "sp|P40368|NUP82_YEAST Nucleoporin NUP82 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP82 PE=1 SV=2":["Nup82"],
	                "sp|P40477|NU159_YEAST Nucleoporin NUP159 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP159 PE=1 SV=1":["Nup159"],
	                "sp|Q02630|NU116_YEAST Nucleoporin NUP116/NSP116 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP116 PE=1 SV=2":["Nup116"],
	                "sp|Q02629|NU100_YEAST Nucleoporin NUP100/NSP100 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP100 PE=1 SV=1":["Nup100"],
	                "sp|P32500|NDC1_YEAST Nucleoporin NDC1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NDC1 PE=1 SV=1":["Ndc1"],
	                "sp|P39685|PO152_YEAST Nucleoporin POM152 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=POM152 PE=1 SV=1":["Pom152"],
	                "sp|Q02455|MLP1_YEAST Protein MLP1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=MLP1 PE=1 SV=2":["Mlp1"],
	                "sp|P40457|MLP2_YEAST Protein MLP2 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=MLP2 PE=1 SV=1":["Mlp2"],
	                "sp|P49686|NUP42_YEAST Nucleoporin NUP42 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP42 PE=1 SV=1":["Nup42"],
	                "sp|Q12315|GLE1_YEAST Nucleoporin GLE1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=GLE1 PE=1 SV=1":["Gle1"],
	                "sp|P40066|GLE2_YEAST Nucleoporin GLE2 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=GLE2 PE=1 SV=1":["Gle2"],
	                "sp|Q12445|POM34_YEAST Nucleoporin POM34 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=POM34 PE=1 SV=1":["Pom34"],
	                "sp|Q02647|DYL1_YEAST Dynein light chain 1, cytoplasmic OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=DYN2 PE=1 SV=1":["Dyn2"],
	                "sp|P39705|NUP60_YEAST Nucleoporin NUP60 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP60 PE=1 SV=1":["Nup60"],
	                "sp|P32499|NUP2_YEAST Nucleoporin NUP2 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP2 PE=1 SV=2":["Nup2"],
	                "sp|P20676|NUP1_YEAST Nucleoporin NUP1 OS=Saccharomyces cerevisiae (strain ATCC 204508 / S288c) OX=559292 GN=NUP1 PE=1 SV=1":["Nup1"],
	                "sp|Q09747|DBP5_SCHPO ATP-dependent RNA helicase dbp5 OS=Schizosaccharomyces pombe (strain 972 / ATCC 24843) OX=284812 GN=dbp5 PE=1 SV=1":["Dbp5"]
	            },
	            "name": "sequences",
	            "type": "sequences"
	        	}

#. Add spatial restraints (following example is from ``IR_asymmetric_unit_refinement/``)
	
	.. code-block:: json

		{
            "active": true,
            "type": "em_map",
            "name": "FitRestraint_IR_relative_no_env",
            "em_restraint_type": "FitRestraint",
            "filename": "EM/IR_relative_no_env.mrc",
            "threshold": 0.0156,
            "voxel_size": 6.74,
            "resolution": 20,
            "weight": 10000,
            "first_copy_only": "false",
            "repr_resolution": 10,
            "optimized_components": [
                {"name": "Nic96", "subunit": "Nic96"},
                {"name": "Nup188", "subunit": "Nup188"},
                {"name": "Nup192", "subunit": "Nup192"},
                {"name": "Nup157", "subunit": "Nup157"},
                {"name": "Nup170", "subunit": "Nup170"},
                {"name": "Nsp1", "subunit": "Nsp1"},
                {"name": "Nup57", "subunit": "Nup57"},
                {"name": "Nup49", "subunit": "Nup49"}
            ]
        	}

And that's it!
