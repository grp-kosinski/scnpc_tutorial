Setting up the JSON project file
================================

You can find a ready-to-use JSON template file named ``config.json`` in the NR_Y_complex/ directory of our `ScNPC_tutorial git repository <https://git.embl.de/rantos/scnpc_tutorial.git>`__.

.. note:: A semi-automated method to generate modelling templates for refinement can be found in `Elongator complex tutorial <https://elongator-tutorial.readthedocs.io/en/latest/json_setup_refinement.html>`__.
