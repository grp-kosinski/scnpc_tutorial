Analyze
=======

#. Enter the refinement output directory and extract score files (following example for ``IR_asymmetric_unit_refinement``): ::

    cd IR_asymmetric_unit_refinement/out
    extract_scores.py

#. Generate PDB/CIF files (e.g. 10 top models in the following case)
	.. note:: Note that no models have been precaclulated for this tutorial therefore to run the above step you need to run modelling with Assembline (follow next sections with modelling of ScNPC subcomplexes e.g. CR Y-complex).

	.. note:: ``--project_dir`` is necessary if you use relative paths in the JSON project file. To generate top models from multiple refinement runs inspect the Elongator tutorial and Assembline manual. ::

	rebuild_atomic.py --top 10 --project_dir <full path to the original project directory> config.json all_scores_sorted_uniq.csv



#. Assess sampling exhaustiveness by following the commands and recommendations in `ScNPC global optimization analysis <https://scnpc-tutorial.readthedocs.io/en/latest/analysis_denovo.html>`__.