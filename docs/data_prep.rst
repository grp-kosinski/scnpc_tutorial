Data preparation
================

.. note:: The data needed to follow the ScNPC modelling tutorial can be obtained from our git repository `ScNPC_tutorial git repository <https://git.embl.de/rantos/scnpc_tutorial.git>`__.

Create a directory for your project and gather all your data there. For this tutorial the corresponding data files are organized per modelling target (e.g. CR Y-complex, NR Y-complex etc.) and Assembline modelling mode (sub-pipeline) that was used (see following sections of the tutorial). This repository includes the following modelling project directories (repository architecture): ::

    scnpc_tutorial/

        # CR Y-complex data
        CR_Y_complex/

            # Electron microscopy maps
            EM/
                P-complex_fit1_0.86_search_100000_correlation_600_0.3_CR_with_Y_no_env_map_best.mrc
                membrane_cerevisiae_no_neg_relative.mrc
                CR_with_Y_no_env_relative.mrc

            #input structures for modeling
            input_PDB/
                ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb
                ScNup133N_56-480_renamed.pdb
                4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb
                4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb
                4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb
                4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb

            #FASTA-formatted file with sequences of all subunits
            ScNPC_sequences.fasta

        # IR asymmetric unit data
        IR_asymmetric_unit_refinement/

            # Electron microscopy maps
            EM/
                IR_relative_no_env.mrc

            #input structures for modeling
            input_PDB/
                ScNup192_NTD_nucl_in.pdb
                ScNup192_NTD_cyt_in.pdb
                ScNup192_CTD_nucl_in.pdb
                ScNup192_CTD_cyt_in.pdb
                ScNup188_NTD_nucl_out.pdb
                ScNup188_NTD_cyt_out.pdb
                ScNup188_CTD_nucl_out.pdb
                ScNup188_CTD_cyt_out.pdb
                ScNup170_NTD_nucl_in.pdb
                ScNup170_NTD_cyt_in.pdb
                ScNup170_CTD_nucl_in.pdb
                ScNup170_CTD_cyt_in.pdb
                ScNup157_NTD_nucl_out.pdb
                ScNup157_NTD_cyt_out.pdb
                ScNup157_CTD_nucl_out.pdb
                ScNup157_CTD_cyt_out.pdb
                ScNsp1_complex_nucl_out.pdb
                ScNsp1_complex_nucl_in.pdb
                ScNsp1_complex_cyt_out.pdb
                ScNsp1_complex_cyt_in.pdb
                ScNic96_CTD_nucl_out.pdb
                ScNic96_CTD_nucl_in.pdb
                ScNic96_CTD_cyt_out.pdb
                ScNic96_CTD_cyt_in.pdb

            #FASTA-formatted file with sequences of all subunits
            ScNPC_sequences.fasta

        # NR Y-complex data (master dir with refinement & global optimization data)
        NR_Y_complex/

            # Electron microscopy maps for refinement
            EM/
                NR_merged_unerased_tail_relative_clean_v1.3.mrc
                membrane_cerevisiae_no_neg_relative_NR_v3.mrc

            #input structures for refinement
            NR_Y_complex_de_novo_model_PDBs/
                ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_1.pdb
                ScNup133N_56-480_renamed_1.pdb
                ScNup133C_490_881_renamed_relative_3CQC_1.pdb
                4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_1.pdb
                4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_1.pdb
                4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_1.pdb
                4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_1.pdb

            #FASTA-formatted file with sequences of all subunits
            ScNPC_sequences.fasta

            #NR Y-complex data (dir with global optimization data)
            NR_Y_complex_de_novo_run/

                # Electron microscopy maps for global optimization
                EM/
                    NR_merged_unerased_tail_relative_clean_v1.3.mrc
                    membrane_cerevisiae_no_neg_relative_NR_v3.mrc

                #input structures for global optimization
                input_PDB/
                    ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new.pdb
                    ScNup133N_56-480_renamed.pdb
                    ScNup133C_490_881_renamed_relative_3CQC.pdb
                    4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb
                    4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb
                    4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb
                    4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb

                #FASTA-formatted file with sequences of all subunits
                ScNPC_sequences.fasta

        # Nup116k.o. ScNPC (at 25C) data
        Nup116delta25C/

            # Electron microscopy maps
            EM/
                Spoke_116d_25C_1fold_new.mrc
                membrane_cerevisiae_wt_relative_to_Spoke_116d_25C_1fold_new.mrc

            #input structures for modeling
            input_PDB/
                ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_NR.pdb
                ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_CR.pdb
                ScNup192_NTD_nucl_in.pdb
                ScNup192_NTD_cyt_in.pdb
                ScNup192_CTD_nucl_in.pdb
                ScNup192_CTD_cyt_in.pdb
                ScNup188_NTD_nucl_out.pdb
                ScNup188_NTD_cyt_out.pdb
                ScNup188_CTD_nucl_out.pdb
                ScNup188_CTD_cyt_out.pdb
                ScNup170_NTD_nucl_in.pdb
                ScNup170_NTD_cyt_in.pdb
                ScNup170_CTD_nucl_in.pdb
                ScNup170_CTD_cyt_in.pdb
                ScNup157_NTD_nucl_out.pdb
                ScNup157_NTD_cyt_out.pdb
                ScNup157_CTD_nucl_out.pdb
                ScNup157_CTD_cyt_out.pdb
                ScNup133N_56-480_renamed_NR.pdb
                ScNup133N_56-480_renamed_CR.pdb
                ScNup133C_490_881_renamed_relative_3CQC_NR.pdb
                ScNup133C_490_881_renamed_relative_3CQC_CR.pdb
                ScNsp1_complex_nucl_out.pdb
                ScNsp1_complex_nucl_in.pdb
                ScNsp1_complex_cyt_out.pdb
                ScNsp1_complex_cyt_in.pdb
                ScNic96_CTD_nucl_out.pdb
                ScNic96_CTD_nucl_in.pdb
                ScNic96_CTD_cyt_out.pdb
                ScNic96_CTD_cyt_in.pdb
                4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_NR.pdb
                4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_CR.pdb
                4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_NR.pdb
                4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_CR.pdb
                4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_NR.pdb
                4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_CR.pdb
                4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_NR.pdb
                4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_CR.pdb

            #FASTA-formatted file with sequences of all subunits
            ScNPC_sequences.fasta

        # Nup116k.o. ScNPC (at 37C) data
        Nup116delta37C/

            # Electron microscopy maps
            EM/
                Nup116_37C_unmasked.mrc
                NR_116d37.mrc
                membrane_116d37C_relative_to_unmasked.mrc
                membrane_116d37C.mrc

            #input structures for modeling
            input_PDB/
                ScNup84_506-726_ScNup133C_882_1155_renamed_relative_3CQC_new_NR.pdb
                ScNup188_NTD_nucl_out.pdb
                ScNup188_CTD_nucl_out.pdb
                ScNup157_NTD_nucl_out.pdb
                ScNup157_CTD_nucl_out.pdb
                ScNup133N_56-480_renamed_NR.pdb
                ScNup133C_490_881_renamed_relative_3CQC_NR.pdb
                ScNsp1_complex_nucl_out.pdb
                ScNic96_CTD_nucl_out.pdb
                4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F_NR.pdb
                4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated_NR.pdb
                4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated_NR.pdb
                4XMM.renamed.noAb_Nup120_2-711_3F7F_updated_NR.pdb

            #FASTA-formatted file with sequences of all subunits
            ScNPC_sequences.fasta