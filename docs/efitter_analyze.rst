Analyze fits
============

Check if run correctly
----------------------

#. If everything run correctly, the ``result_fits_chimera`` directory should contain the following structure (example for CR Y-complex rigid bodies): ::

    result_fits_chimera/
        search100000_metric_cam_rad_600_inside0.3_res_40/
            CR_with_Y_no_env_relative.mrc/
                ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb/
                    Rplots.pdf
                    CR_with_Y_no_env_relative.mrc
                    histogram.png
                    log_err.txt
                    log_out.txt
                    ori_pdb.pdb
                    run.sh
                    solutions.csv
                    solutions_pvalues.csv
                ScNup133N_56-480_renamed.pdb/
                    Rplots.pdf
                    CR_with_Y_no_env_relative.mrc
                    histogram.png
                    log_err.txt
                    log_out.txt
                    ori_pdb.pdb
                    run.sh
                    solutions.csv
                    solutions_pvalues.csv

                ... and so on

#. If the run was successful then you can reconstruct the top fits of each input rigid body (following example for top five fits). Enter the dir ``result_fits_chimera/search100000_metric_cam_rad_600_inside0.3_res_40`` and run the following step which will generate a dir called ``top5``

   .. code-block:: bash

        genPDBs_many.py -n5 top5 */*/solutions.csv

.. warning::
    The **solutions_pvalues.csv** files contain the fit libraries and must be present to run `global optimization <https://assembline.readthedocs.io/en/latest/combinations.html>`__.

.. note::
    The pre-calculated output has been simplified (i.e. not all files described in the above architecture have been calculated) for the purpose of this tutorial. Read more on `how to analyze the fits <https://assembline.readthedocs.io/en/latest/efitter_analyze.html>`__.