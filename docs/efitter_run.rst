Run
===

#. Enter the ``systematic_fits/`` directory either in ``scnpc_tutorial/CR_Y_complex/`` or ``scnpc_tutorial/NR_Y_complex/NR_Y_complex_de_novo_run/``.

	.. note:: These directories are available and should be retrieved from our git repository `ScNPC_tutorial git repository <https://git.embl.de/rantos/scnpc_tutorial.git>`__.

#. Run the fitting
   
   .. code-block:: bash

        fit.py systematic_fitting_parameters.py 

   The fitting runs will take two to three hours (~2-3h).

#. The ``fit.py`` step will create and save all output to ``systematic_fits/result_fits_chimera``. So, while in the ``systematic_fits/`` folder, calculate p-values of resulting fits ::

    genpval.py result_fits_chimera

.. note::
   For this tutorial you do not have to run ``Efitter`` (i.e. ``fit.py`` script) again as all the fitting results have been already calculated for you. In case you still want to run the generation of fit libraries then rename the dir ``result_fits_chimera/`` and while in the ``systematic_fits/`` dir run the steps from above.