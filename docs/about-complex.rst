About the S. cerevisiae nuclear pore complex (ScNPC)
====================================================

Nuclear pore complexes (NPCs) are large macromolecular assemblies that fuse the nuclear envelope and facilitate nucleocytoplasmic transport. They are built by around 30 different nucleoporins (Nups) present in multiple copies with respect to the 8-axis rotational symmetry of the complex. The structural architecture of the pore is a three-stacked-rings conformation plus peripheral elements (e.g. nuclear basket) emanating from them (i.e. rings).

We built and published an integrative model of `S. cerevisiae NPC in 2020 <https://doi.org/10.1038/s41586-020-2670-5>`__ which included all the major scaffold Nup subcomplexes (i.e. outer rings Y-complexes & Inner ring complex). The model was built based on atomic structures (experimentally determined, homology models, integrative models) and electron microscopy (EM) densities. Spatial restraints were derived from all input data while more restraints were defined as harmomic distance restraints with respect to the available bibliography. The wild-type model of ScNPC that we built served as the basis for building ScNPC models from Nup116 k.o. cells grown under different temperatures.  

.. image:: images/PDB_dev_pores_merged_tutorial.jpg
  :width: 800
  :alt: ScNPC integrative models