Run
===

The following steps show roughly how to refine models (either top models from global fitting or locally optimize rigid bodies in general).

#. I you haven't yet, activate your modelling virtual environment before using the Assembline software by

    .. code-block:: bash

        source activate Assembline

    or depending on your computer setup:

    .. code-block:: bash
    
        conda activate Assembline

#. Enter the main project directory (e.g. ``IR_asymmetric_unit_refinement``) and create ``params.py`` and ``config.json`` files according to Assembline manual. While in the main project directory run the following

    .. code-block:: bash

        assembline.py --traj --models -o out --multi --start_idx 0 --njobs 500 config.json params.py

.. note:: In order to set up and run refinement for multiple models then instruct the `Elongator manual <https://elongator-tutorial.readthedocs.io/en/latest/run_refinement.html>`__ and `Assembline manual <https://assembline.readthedocs.io/en/latest/refinement.html>`__.