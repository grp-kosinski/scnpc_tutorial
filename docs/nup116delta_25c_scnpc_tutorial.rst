Nup116 k.o. ScNPC (at 25C) modelling
====================================

For modelling the Nup116 k.o. ScNPC (at 25C) the local rigid body refinement method from `Assembline <https://assembline.readthedocs.io/en/latest/#>`__ will be used. The dir ``scnpc_tutorial/Nup116delta25C`` includes source code files, input files and precalculated modelling results for the Nup116 k.o. ScNPC model (25C): ::

    - parameters file & configuration file for 'refinement' integrative modelling of Nup116delta25C ScNPC
    - sequence fasta file, input_PDB, EM (input data)
    - ScNPC_Nup116delta25C_final_model.pdb, out (output modelling results)

#. First activate your virtual environment and enter the ``Nup116delta25C`` dir

    .. code-block:: bash

        source activate Assembline

        cd Nup116delta25C/

    or depending on your computer setup:

    .. code-block:: bash

        conda activate Assembline

        cd Nup116delta25C/

#. Run the refinement

    .. code-block:: bash

            # this will run 500 refinement modelling runs on a slurm cluster (for options/parameters or local runs inspect the Assembline manual)
            assembline.py --traj --models -o out --multi --start_idx 0 --njobs 500 config.json params.py

    .. note:: There is already an output dir ``Nup116delta25C/out`` so in case you want to run the modelling then rename the ``out/`` dir as it will be overwritten from the run above 

#. Enter ``out/`` dir, generate output scoring lists and rebuild atomic structures of models 

    .. code-block:: bash

        cd out

        extract_scores.py #this should create a couple of files including all_scores_sorted_uniq.csv

        rebuild_atomic.py --top 10 --project_dir <full path to the original project directory Nup116delta25C> config.json all_scores_sorted_uniq.csv

#. While in the ``out/`` dir run the following command to prepare your output models for analysis with ``imp-sampcon`` tool from ``IMP``
    
    .. code-block:: bash

        setup_analysis.py -s all_scores.csv -o analysis -d density.txt

    .. note:: The density.txt is not provided but only in the ``CR_Y_complex/out``, therefore visit this dir to inspect it. To generate it yourself please inspect `Assembline analysis section <https://assembline.readthedocs.io/en/latest/sampling_exhaustiveness.html>`__.

#. Run ``imp-sampcon exhaust`` tool (command-line tool provided with IMP) to perform the sampling analysis:

    .. code-block:: bash

        cd analysis

        imp_sampcon exhaust -n <prefix for output files> \
        --rmfA sample_A/sample_A_models.rmf3 \
        --rmfB sample_B/sample_B_models.rmf3 \
        --scoreA scoresA.txt --scoreB scoresB.txt \
        -d ../density.txt \
        -m cpu_omp \
        -c <int for cores to process> \
        -gp \
        -g <float with clustering threshold step> \

    .. note:: For further descriptions of settings for ``imp_sampcon`` please see `Sampling exhaustiveness and precision with Assembline <https://assembline.readthedocs.io/en/latest/sampling_exhaustiveness.html#>`_