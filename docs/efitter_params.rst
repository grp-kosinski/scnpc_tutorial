Set up
======

#. To run ``Efitter`` you need a parameter file in Python language format that specifies:

     * input structures to fit
     * EM maps
     * fitting parameters
     * optionally, options for execution on a computer cluster 

#. For this tutorial, the parameter files are already prepared for both CR and NR Y-complexes in the ``scnpc_tutorial/CR_Y_complex/systematic_fits/`` and ``scnpc_tutorial/NR_Y_complex/NR_Y_complex_de_novo_run/systematic_fits/`` directories (from our `git repository <https://git.embl.de/rantos/scnpc_tutorial.git>`_). The ``systematic_fitting_parameters.py`` file contains (example parameters file for ``CR Y-complex`` following):

    .. code-block:: python

        from efitter import Map
        from string import Template

        method='chimera'
        dry_run = False
        run_crashed_only = True
        master_outdir = './result_fits_chimera'

        MAPS = [
            Map('em_maps/CR_with_Y_no_env_relative.mrc', threshold=0.0142, resolution=40)
        ]

        models_dir = './PDB'

        PDB_FILES = [
            'ScNup133N_56-480_renamed.pdb',
            'ScNup84_437-726_ScNup133C_490_1155_renamed_relative_3I4R.pdb',
            '4XMM.renamed.noAb_Seh1_Nup85_47_544_3F3F.pdb',
            '4XMM.renamed.noAb_Nup145c_149-550_Sec13_Nup84_7-436_3IKO_updated.pdb',
            '4XMM.renamed.noAb_Nup120_2-711_3F7F_updated.pdb',
            '4XMM.renamed.noAb_Nup120_715-1036_Nup85_553-744_Nup145c_92-99_554-712_remaining_updated.pdb'
        ]

        CA_only = False
        backbone_only = False
        move_to_center = True

        fitmap_args = [
            # We suggest to run a small test run with search 100 first
            # {
            #     'template': Template("""
            #         map $map
            #         map_threshold $threshold
            #         fitmap_args resolution $resolution metric cam envelope true search 100 placement sr clusterAngle 3 clusterShift 3.0 radius 600 inside .30
            #         saveFiles False
            #         """),
            #     'config_prefix': 'test'
            #     },
            # Paramters for main runs (https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/fitmap.html)
            {
                'template': Template("""
                    map $map
                    map_threshold $threshold
                    fitmap_args resolution $resolution metric cam envelope true search 100000 placement sr clusterAngle 3 clusterShift 3.0 radius 600 inside .30
                    saveFiles False
                    """),
                'config_prefix': 'search100000_metric_cam_rad_600_inside0.3_res_40'
                }
        ]

        # If necessary, edit the below template following specifications of
        # cluster_submission_command
        # and template for your cluster submission script (using Python string.Template formatting)
        cluster_submission_command = 'sbatch'
        run_script_templ = Template("""#!/bin/bash
        #
        #SBATCH --job-name=$job_name
        #SBATCH --nodes=1
        #SBATCH --mem-per-cpu=m=2000
        #SBATCH --time=10:00:00
        #SBATCH --error $pdb_outdir/log_err.txt
        #SBATCH --output $pdb_outdir/log_out.txt

        $cmd
        """)

    .. note:: Read more `about fit libriaries <https://assembline.readthedocs.io/en/latest/fit_libraries_intro.html>`_ and `how to set up the parameters file <https://assembline.readthedocs.io/en/latest/efitter_params.html>`_ in the Assembline manual. Remember that you can retrieve and inspect template parameter files in ``Assembline/doc/templates`` from our `Assembline git repository <https://git.embl.de/kosinski/Assembline.git>`__.

#. Based on the above example parameter file, ``Efitter`` will run six fitting runs - one for each input PDB structure. You can run these as six independent jobs on a computer cluster or six processes on a multicore computer, or one by one on a single core computer.

#. To run on a computer cluster, adjust the ``cluster_submission_command`` and ``run_script_templ`` to the queuing system on your cluster (the provided example would work only on a Slurm cluster). It is important that you include ``$job_name``, ``$pdb_outdir``, and ``$cmd`` in your script template.

#. To run on a multicore computer, remove ``cluster_submission_command`` and replace the ``run_script_templ`` with 

    .. code-block:: python
    
        run_script_templ = Template("""#!/bin/bash
        #
        echo $job_name
        $cmd &>$pdb_outdir/log&
        """)

.. note:: In the CR Y-complex parameters file example (from above), more settings than what will actually be used for modelling were left in (all the ones with `False` value, e.g. `dry_run = False`) in order to give a broader overview of the available options to users.