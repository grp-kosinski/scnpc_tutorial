score 460929.43521766225
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_892-Residue_900 of Nup157",0.0,0.0,1.0
"conn restraint Residue_892-Residue_900 of Nup157",11.720802823163654,11.720802823163654,1.0
"conn restraint Residue_992-Residue_1000 of Nup170",0.0,0.0,1.0
"conn restraint Residue_992-Residue_1000 of Nup170",0.0,0.0,1.0
"conn restraint Residue_973-Residue_974 of Nup192",19.306153284949513,19.306153284949513,1.0
"conn restraint Residue_973-Residue_974 of Nup192",74.92309213847956,74.92309213847956,1.0
"conn restraint Residue_993-Residue_994 of Nup188",14.234675802762633,14.234675802762633,1.0
"conn restraint Residue_993-Residue_994 of Nup188",34.83085697943074,34.83085697943074,1.0
"ev_restraint_resol_10",591.757564905844,591.757564905844,10.0
"FitRestraint_IR_relative_no_env",459213.48571777344,459213.48571777344,10000.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNsp1_complex_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNsp1_complex_nucl_out.pdb_0",28.200657836068235,28.200657836068235,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNsp1_complex_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNsp1_complex_nucl_out.pdb_0",15.350896337687919,15.350896337687919,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNsp1_complex_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNsp1_complex_nucl_out.pdb_0",11.453772532782146,11.453772532782146,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNic96_CTD_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNic96_CTD_nucl_out.pdb_0",35.93450279463152,35.93450279463152,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNic96_CTD_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNic96_CTD_nucl_out.pdb_0",63.71515529602681,63.71515529602681,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNic96_CTD_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNic96_CTD_nucl_out.pdb_0",129.7596127483563,129.7596127483563,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNsp1_complex_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNsp1_complex_nucl_in.pdb_0",4.128651642095981,4.128651642095981,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNsp1_complex_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNsp1_complex_nucl_in.pdb_0",5.86729489658298,5.86729489658298,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNsp1_complex_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNsp1_complex_nucl_in.pdb_0",5.039930654296019,5.039930654296019,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNic96_CTD_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNic96_CTD_nucl_in.pdb_0",81.86758367800832,81.86758367800832,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNic96_CTD_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNic96_CTD_nucl_in.pdb_0",48.995965858884794,48.995965858884794,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNic96_CTD_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNic96_CTD_nucl_in.pdb_0",47.25686337805506,47.25686337805506,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_NTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_NTD_nucl_out.pdb_0",0.8420796086015667,0.8420796086015667,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_NTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_NTD_nucl_out.pdb_0",0.8479163085546073,0.8479163085546073,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_NTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_NTD_nucl_out.pdb_0",1.628801292064006,1.628801292064006,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_CTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_CTD_nucl_out.pdb_0",0.6143490551412423,0.6143490551412423,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_CTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_CTD_nucl_out.pdb_0",43.11244897773344,43.11244897773344,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_CTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_CTD_nucl_out.pdb_0",3.500169024525156,3.500169024525156,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_NTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_NTD_nucl_in.pdb_0",11.83323849273757,11.83323849273757,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_NTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_NTD_nucl_in.pdb_0",8.237948191262339,8.237948191262339,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_NTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_NTD_nucl_in.pdb_0",4.092670129248167,4.092670129248167,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_CTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_CTD_nucl_in.pdb_0",17.53999536562626,17.53999536562626,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_CTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_CTD_nucl_in.pdb_0",17.091948125062526,17.091948125062526,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_CTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_CTD_nucl_in.pdb_0",27.87645614747219,27.87645614747219,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_NTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_NTD_nucl_out.pdb_0",7.31688063681787,7.31688063681787,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_NTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_NTD_nucl_out.pdb_0",37.014472800937796,37.014472800937796,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_NTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_NTD_nucl_out.pdb_0",4.656410403095686,4.656410403095686,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_CTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_CTD_nucl_out.pdb_0",5.193675095241089,5.193675095241089,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_CTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_CTD_nucl_out.pdb_0",9.534786537816116,9.534786537816116,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_CTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_CTD_nucl_out.pdb_0",4.496521526097883,4.496521526097883,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_NTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_NTD_nucl_in.pdb_0",103.78012601304857,103.78012601304857,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_NTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_NTD_nucl_in.pdb_0",8.71666659955072,8.71666659955072,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_NTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_NTD_nucl_in.pdb_0",153.44167306366492,153.44167306366492,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_CTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_CTD_nucl_in.pdb_0",7.722382706915395,7.722382706915395,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_CTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_CTD_nucl_in.pdb_0",5.643674457644885,5.643674457644885,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_CTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_CTD_nucl_in.pdb_0",6.8701757421318925,6.8701757421318925,1.0
