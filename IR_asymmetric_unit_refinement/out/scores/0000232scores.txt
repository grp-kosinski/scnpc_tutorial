score 465000.76943068585
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_56-Residue_205 of Nic96",0.0,0.0,1.0
"conn restraint Residue_892-Residue_900 of Nup157",0.0,0.0,1.0
"conn restraint Residue_892-Residue_900 of Nup157",0.0,0.0,1.0
"conn restraint Residue_992-Residue_1000 of Nup170",0.0,0.0,1.0
"conn restraint Residue_992-Residue_1000 of Nup170",0.0,0.0,1.0
"conn restraint Residue_973-Residue_974 of Nup192",110.50479946503023,110.50479946503023,1.0
"conn restraint Residue_973-Residue_974 of Nup192",50.30232408070245,50.30232408070245,1.0
"conn restraint Residue_993-Residue_994 of Nup188",0.1637726382891572,0.1637726382891572,1.0
"conn restraint Residue_993-Residue_994 of Nup188",48.374081294944375,48.374081294944375,1.0
"ev_restraint_resol_10",971.2884673855286,971.2884673855286,10.0
"FitRestraint_IR_relative_no_env",463000.2975463867,463000.2975463867,10000.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNsp1_complex_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNsp1_complex_nucl_out.pdb_0",4.439266283189493,4.439266283189493,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNsp1_complex_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNsp1_complex_nucl_out.pdb_0",10.65207328188804,10.65207328188804,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNsp1_complex_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNsp1_complex_nucl_out.pdb_0",6.150716806126447,6.150716806126447,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNic96_CTD_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNic96_CTD_nucl_out.pdb_0",75.82555019363727,75.82555019363727,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNic96_CTD_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNic96_CTD_nucl_out.pdb_0",39.85561223089291,39.85561223089291,1.0
"Symmetry restraint: System.State_0.Nic96.0,rb_ScNic96_CTD_cyt_out.pdb_0 - System.State_0.Nic96.3,rb_ScNic96_CTD_nucl_out.pdb_0",137.7425847076847,137.7425847076847,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNsp1_complex_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNsp1_complex_nucl_in.pdb_0",19.46400103670558,19.46400103670558,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNsp1_complex_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNsp1_complex_nucl_in.pdb_0",11.379695686182389,11.379695686182389,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNsp1_complex_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNsp1_complex_nucl_in.pdb_0",20.431720612881474,20.431720612881474,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNic96_CTD_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNic96_CTD_nucl_in.pdb_0",65.77082669769287,65.77082669769287,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNic96_CTD_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNic96_CTD_nucl_in.pdb_0",89.51240529564,89.51240529564,1.0
"Symmetry restraint: System.State_0.Nic96.1,rb_ScNic96_CTD_cyt_in.pdb_0 - System.State_0.Nic96.2,rb_ScNic96_CTD_nucl_in.pdb_0",66.16673690784857,66.16673690784857,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_NTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_NTD_nucl_out.pdb_0",5.444578530740491,5.444578530740491,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_NTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_NTD_nucl_out.pdb_0",11.772200299582646,11.772200299582646,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_NTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_NTD_nucl_out.pdb_0",5.726126256544393,5.726126256544393,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_CTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_CTD_nucl_out.pdb_0",0.13930644682947677,0.13930644682947677,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_CTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_CTD_nucl_out.pdb_0",0.11864451370535213,0.11864451370535213,1.0
"Symmetry restraint: System.State_0.Nup188.0,rb_ScNup188_CTD_cyt_out.pdb_0 - System.State_0.Nup188.1,rb_ScNup188_CTD_nucl_out.pdb_0",0.22575565698137476,0.22575565698137476,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_NTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_NTD_nucl_in.pdb_0",23.73865951461718,23.73865951461718,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_NTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_NTD_nucl_in.pdb_0",5.644376033121107,5.644376033121107,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_NTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_NTD_nucl_in.pdb_0",4.508106165662416,4.508106165662416,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_CTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_CTD_nucl_in.pdb_0",0.6234371432773764,0.6234371432773764,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_CTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_CTD_nucl_in.pdb_0",6.629019315996698,6.629019315996698,1.0
"Symmetry restraint: System.State_0.Nup192.0,rb_ScNup192_CTD_cyt_in.pdb_0 - System.State_0.Nup192.1,rb_ScNup192_CTD_nucl_in.pdb_0",0.6434623533313502,0.6434623533313502,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_NTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_NTD_nucl_out.pdb_0",9.264792413877629,9.264792413877629,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_NTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_NTD_nucl_out.pdb_0",30.777408949194548,30.777408949194548,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_NTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_NTD_nucl_out.pdb_0",10.967285575882352,10.967285575882352,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_CTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_CTD_nucl_out.pdb_0",3.1556203872810147,3.1556203872810147,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_CTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_CTD_nucl_out.pdb_0",14.73056205230971,14.73056205230971,1.0
"Symmetry restraint: System.State_0.Nup157.0,rb_ScNup157_CTD_cyt_out.pdb_0 - System.State_0.Nup157.1,rb_ScNup157_CTD_nucl_out.pdb_0",2.274176771131931,2.274176771131931,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_NTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_NTD_nucl_in.pdb_0",43.21651581248104,43.21651581248104,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_NTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_NTD_nucl_in.pdb_0",66.36688042721195,66.36688042721195,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_NTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_NTD_nucl_in.pdb_0",22.23396400105789,22.23396400105789,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_CTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_CTD_nucl_in.pdb_0",1.0655764655237707,1.0655764655237707,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_CTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_CTD_nucl_in.pdb_0",3.139545669766041,3.139545669766041,1.0
"Symmetry restraint: System.State_0.Nup170.0,rb_ScNup170_CTD_cyt_in.pdb_0 - System.State_0.Nup170.1,rb_ScNup170_CTD_nucl_in.pdb_0",0.0412489382000403,0.0412489382000403,1.0
